package edu.ustb.sei.mde.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSmartContractSpecificationLexer extends Lexer {
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_SLD=4;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int RULE_PLAINTEXT=7;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=8;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators

    public InternalSmartContractSpecificationLexer() {;} 
    public InternalSmartContractSpecificationLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalSmartContractSpecificationLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalSmartContractSpecification.g"; }

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:11:7: ( 'or' )
            // InternalSmartContractSpecification.g:11:9: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:12:7: ( '||' )
            // InternalSmartContractSpecification.g:12:9: '||'
            {
            match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:13:7: ( 'and' )
            // InternalSmartContractSpecification.g:13:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:14:7: ( '&&' )
            // InternalSmartContractSpecification.g:14:9: '&&'
            {
            match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:15:7: ( 'he' )
            // InternalSmartContractSpecification.g:15:9: 'he'
            {
            match("he"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:16:7: ( 'she' )
            // InternalSmartContractSpecification.g:16:9: 'she'
            {
            match("she"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:17:7: ( 'his' )
            // InternalSmartContractSpecification.g:17:9: 'his'
            {
            match("his"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:18:7: ( 'her' )
            // InternalSmartContractSpecification.g:18:9: 'her'
            {
            match("her"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:19:7: ( 'himself' )
            // InternalSmartContractSpecification.g:19:9: 'himself'
            {
            match("himself"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:20:7: ( 'herself' )
            // InternalSmartContractSpecification.g:20:9: 'herself'
            {
            match("herself"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:21:7: ( 'true' )
            // InternalSmartContractSpecification.g:21:9: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:22:7: ( 'false' )
            // InternalSmartContractSpecification.g:22:9: 'false'
            {
            match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:23:7: ( 'valid' )
            // InternalSmartContractSpecification.g:23:9: 'valid'
            {
            match("valid"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:24:7: ( 'fulfilled' )
            // InternalSmartContractSpecification.g:24:9: 'fulfilled'
            {
            match("fulfilled"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:25:7: ( 'executed' )
            // InternalSmartContractSpecification.g:25:9: 'executed'
            {
            match("executed"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:26:7: ( 'all' )
            // InternalSmartContractSpecification.g:26:9: 'all'
            {
            match("all"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:27:7: ( 'forAll' )
            // InternalSmartContractSpecification.g:27:9: 'forAll'
            {
            match("forAll"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:28:7: ( 'some' )
            // InternalSmartContractSpecification.g:28:9: 'some'
            {
            match("some"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:29:7: ( 'exists' )
            // InternalSmartContractSpecification.g:29:9: 'exists'
            {
            match("exists"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:30:7: ( '=' )
            // InternalSmartContractSpecification.g:30:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:31:7: ( 'is equal to' )
            // InternalSmartContractSpecification.g:31:9: 'is equal to'
            {
            match("is equal to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:32:7: ( '<' )
            // InternalSmartContractSpecification.g:32:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:33:7: ( '<=' )
            // InternalSmartContractSpecification.g:33:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:34:7: ( '>' )
            // InternalSmartContractSpecification.g:34:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:35:7: ( '>=' )
            // InternalSmartContractSpecification.g:35:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:36:7: ( '!=' )
            // InternalSmartContractSpecification.g:36:9: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:37:7: ( '<>' )
            // InternalSmartContractSpecification.g:37:9: '<>'
            {
            match("<>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:38:7: ( 'is not equal to' )
            // InternalSmartContractSpecification.g:38:9: 'is not equal to'
            {
            match("is not equal to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:39:7: ( 'belongs to' )
            // InternalSmartContractSpecification.g:39:9: 'belongs to'
            {
            match("belongs to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:40:7: ( 'is a subset of' )
            // InternalSmartContractSpecification.g:40:9: 'is a subset of'
            {
            match("is a subset of"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:41:7: ( '+' )
            // InternalSmartContractSpecification.g:41:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:42:7: ( '-' )
            // InternalSmartContractSpecification.g:42:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:43:7: ( '*' )
            // InternalSmartContractSpecification.g:43:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:44:7: ( '/' )
            // InternalSmartContractSpecification.g:44:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:45:7: ( '^' )
            // InternalSmartContractSpecification.g:45:9: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:46:7: ( 'before' )
            // InternalSmartContractSpecification.g:46:9: 'before'
            {
            match("before"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:47:7: ( 'after' )
            // InternalSmartContractSpecification.g:47:9: 'after'
            {
            match("after"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:48:7: ( 'any' )
            // InternalSmartContractSpecification.g:48:9: 'any'
            {
            match("any"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:49:7: ( 'one' )
            // InternalSmartContractSpecification.g:49:9: 'one'
            {
            match("one"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:50:7: ( 'this' )
            // InternalSmartContractSpecification.g:50:9: 'this'
            {
            match("this"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:51:7: ( 'the' )
            // InternalSmartContractSpecification.g:51:9: 'the'
            {
            match("the"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:52:7: ( 'now' )
            // InternalSmartContractSpecification.g:52:9: 'now'
            {
            match("now"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:53:7: ( 'start' )
            // InternalSmartContractSpecification.g:53:9: 'start'
            {
            match("start"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:54:7: ( 'end' )
            // InternalSmartContractSpecification.g:54:9: 'end'
            {
            match("end"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:55:7: ( 'contract' )
            // InternalSmartContractSpecification.g:55:9: 'contract'
            {
            match("contract"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:56:7: ( '{' )
            // InternalSmartContractSpecification.g:56:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:57:7: ( '}' )
            // InternalSmartContractSpecification.g:57:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "T__60"
    public final void mT__60() throws RecognitionException {
        try {
            int _type = T__60;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:58:7: ( '.' )
            // InternalSmartContractSpecification.g:58:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__60"

    // $ANTLR start "T__61"
    public final void mT__61() throws RecognitionException {
        try {
            int _type = T__61;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:59:7: ( 'type' )
            // InternalSmartContractSpecification.g:59:9: 'type'
            {
            match("type"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__61"

    // $ANTLR start "T__62"
    public final void mT__62() throws RecognitionException {
        try {
            int _type = T__62;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:60:7: ( ':' )
            // InternalSmartContractSpecification.g:60:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__62"

    // $ANTLR start "T__63"
    public final void mT__63() throws RecognitionException {
        try {
            int _type = T__63;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:61:7: ( 'party' )
            // InternalSmartContractSpecification.g:61:9: 'party'
            {
            match("party"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__63"

    // $ANTLR start "T__64"
    public final void mT__64() throws RecognitionException {
        try {
            int _type = T__64;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:62:7: ( 'group' )
            // InternalSmartContractSpecification.g:62:9: 'group'
            {
            match("group"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__64"

    // $ANTLR start "T__65"
    public final void mT__65() throws RecognitionException {
        try {
            int _type = T__65;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:63:7: ( 'external' )
            // InternalSmartContractSpecification.g:63:9: 'external'
            {
            match("external"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__65"

    // $ANTLR start "T__66"
    public final void mT__66() throws RecognitionException {
        try {
            int _type = T__66;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:64:7: ( ',' )
            // InternalSmartContractSpecification.g:64:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__66"

    // $ANTLR start "T__67"
    public final void mT__67() throws RecognitionException {
        try {
            int _type = T__67;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:65:7: ( '(' )
            // InternalSmartContractSpecification.g:65:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__67"

    // $ANTLR start "T__68"
    public final void mT__68() throws RecognitionException {
        try {
            int _type = T__68;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:66:7: ( ')' )
            // InternalSmartContractSpecification.g:66:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__68"

    // $ANTLR start "T__69"
    public final void mT__69() throws RecognitionException {
        try {
            int _type = T__69;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:67:7: ( 'term' )
            // InternalSmartContractSpecification.g:67:9: 'term'
            {
            match("term"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__69"

    // $ANTLR start "T__70"
    public final void mT__70() throws RecognitionException {
        try {
            int _type = T__70;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:68:7: ( 'can' )
            // InternalSmartContractSpecification.g:68:9: 'can'
            {
            match("can"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__70"

    // $ANTLR start "T__71"
    public final void mT__71() throws RecognitionException {
        try {
            int _type = T__71;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:69:7: ( 'when' )
            // InternalSmartContractSpecification.g:69:9: 'when'
            {
            match("when"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__71"

    // $ANTLR start "T__72"
    public final void mT__72() throws RecognitionException {
        try {
            int _type = T__72;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:70:7: ( 'while' )
            // InternalSmartContractSpecification.g:70:9: 'while'
            {
            match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__72"

    // $ANTLR start "T__73"
    public final void mT__73() throws RecognitionException {
        try {
            int _type = T__73;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:71:7: ( 'where' )
            // InternalSmartContractSpecification.g:71:9: 'where'
            {
            match("where"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__73"

    // $ANTLR start "T__74"
    public final void mT__74() throws RecognitionException {
        try {
            int _type = T__74;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:72:7: ( 'must' )
            // InternalSmartContractSpecification.g:72:9: 'must'
            {
            match("must"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__74"

    // $ANTLR start "T__75"
    public final void mT__75() throws RecognitionException {
        try {
            int _type = T__75;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:73:7: ( 'deposit' )
            // InternalSmartContractSpecification.g:73:9: 'deposit'
            {
            match("deposit"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__75"

    // $ANTLR start "T__76"
    public final void mT__76() throws RecognitionException {
        try {
            int _type = T__76;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:74:7: ( '$' )
            // InternalSmartContractSpecification.g:74:9: '$'
            {
            match('$'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__76"

    // $ANTLR start "T__77"
    public final void mT__77() throws RecognitionException {
        try {
            int _type = T__77;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:75:7: ( 'withdraw' )
            // InternalSmartContractSpecification.g:75:9: 'withdraw'
            {
            match("withdraw"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__77"

    // $ANTLR start "T__78"
    public final void mT__78() throws RecognitionException {
        try {
            int _type = T__78;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:76:7: ( 'transfer' )
            // InternalSmartContractSpecification.g:76:9: 'transfer'
            {
            match("transfer"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__78"

    // $ANTLR start "T__79"
    public final void mT__79() throws RecognitionException {
        try {
            int _type = T__79;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:77:7: ( 'to' )
            // InternalSmartContractSpecification.g:77:9: 'to'
            {
            match("to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__79"

    // $ANTLR start "T__80"
    public final void mT__80() throws RecognitionException {
        try {
            int _type = T__80;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:78:7: ( 'in' )
            // InternalSmartContractSpecification.g:78:9: 'in'
            {
            match("in"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__80"

    // $ANTLR start "T__81"
    public final void mT__81() throws RecognitionException {
        try {
            int _type = T__81;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:79:7: ( 'such' )
            // InternalSmartContractSpecification.g:79:9: 'such'
            {
            match("such"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__81"

    // $ANTLR start "T__82"
    public final void mT__82() throws RecognitionException {
        try {
            int _type = T__82;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:80:7: ( 'that' )
            // InternalSmartContractSpecification.g:80:9: 'that'
            {
            match("that"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__82"

    // $ANTLR start "T__83"
    public final void mT__83() throws RecognitionException {
        try {
            int _type = T__83;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:81:7: ( 'if' )
            // InternalSmartContractSpecification.g:81:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__83"

    // $ANTLR start "T__84"
    public final void mT__84() throws RecognitionException {
        try {
            int _type = T__84;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:82:7: ( 'then' )
            // InternalSmartContractSpecification.g:82:9: 'then'
            {
            match("then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__84"

    // $ANTLR start "T__85"
    public final void mT__85() throws RecognitionException {
        try {
            int _type = T__85;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:83:7: ( 'else' )
            // InternalSmartContractSpecification.g:83:9: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__85"

    // $ANTLR start "T__86"
    public final void mT__86() throws RecognitionException {
        try {
            int _type = T__86;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:84:7: ( 'implies' )
            // InternalSmartContractSpecification.g:84:9: 'implies'
            {
            match("implies"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__86"

    // $ANTLR start "T__87"
    public final void mT__87() throws RecognitionException {
        try {
            int _type = T__87;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:85:7: ( 'not' )
            // InternalSmartContractSpecification.g:85:9: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__87"

    // $ANTLR start "T__88"
    public final void mT__88() throws RecognitionException {
        try {
            int _type = T__88;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:86:7: ( '::' )
            // InternalSmartContractSpecification.g:86:9: '::'
            {
            match("::"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__88"

    // $ANTLR start "T__89"
    public final void mT__89() throws RecognitionException {
        try {
            int _type = T__89;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:87:7: ( 'did' )
            // InternalSmartContractSpecification.g:87:9: 'did'
            {
            match("did"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__89"

    // $ANTLR start "T__90"
    public final void mT__90() throws RecognitionException {
        try {
            int _type = T__90;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:88:7: ( 'is' )
            // InternalSmartContractSpecification.g:88:9: 'is'
            {
            match("is"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__90"

    // $ANTLR start "T__91"
    public final void mT__91() throws RecognitionException {
        try {
            int _type = T__91;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:89:7: ( 'year' )
            // InternalSmartContractSpecification.g:89:9: 'year'
            {
            match("year"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__91"

    // $ANTLR start "T__92"
    public final void mT__92() throws RecognitionException {
        try {
            int _type = T__92;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:90:7: ( 'month' )
            // InternalSmartContractSpecification.g:90:9: 'month'
            {
            match("month"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__92"

    // $ANTLR start "T__93"
    public final void mT__93() throws RecognitionException {
        try {
            int _type = T__93;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:91:7: ( 'day' )
            // InternalSmartContractSpecification.g:91:9: 'day'
            {
            match("day"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__93"

    // $ANTLR start "T__94"
    public final void mT__94() throws RecognitionException {
        try {
            int _type = T__94;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:92:7: ( 'hour' )
            // InternalSmartContractSpecification.g:92:9: 'hour'
            {
            match("hour"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__94"

    // $ANTLR start "T__95"
    public final void mT__95() throws RecognitionException {
        try {
            int _type = T__95;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:93:7: ( 'min' )
            // InternalSmartContractSpecification.g:93:9: 'min'
            {
            match("min"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__95"

    // $ANTLR start "T__96"
    public final void mT__96() throws RecognitionException {
        try {
            int _type = T__96;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:94:7: ( 'sec' )
            // InternalSmartContractSpecification.g:94:9: 'sec'
            {
            match("sec"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__96"

    // $ANTLR start "T__97"
    public final void mT__97() throws RecognitionException {
        try {
            int _type = T__97;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:95:7: ( '@' )
            // InternalSmartContractSpecification.g:95:9: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__97"

    // $ANTLR start "T__98"
    public final void mT__98() throws RecognitionException {
        try {
            int _type = T__98;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:96:7: ( 'set' )
            // InternalSmartContractSpecification.g:96:9: 'set'
            {
            match("set"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__98"

    // $ANTLR start "T__99"
    public final void mT__99() throws RecognitionException {
        try {
            int _type = T__99;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:97:7: ( 'within' )
            // InternalSmartContractSpecification.g:97:9: 'within'
            {
            match("within"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__99"

    // $ANTLR start "RULE_SLD"
    public final void mRULE_SLD() throws RecognitionException {
        try {
            int _type = RULE_SLD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9513:10: ( 'i.e.,' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalSmartContractSpecification.g:9513:12: 'i.e.,' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("i.e.,"); 

            // InternalSmartContractSpecification.g:9513:20: (~ ( ( '\\n' | '\\r' ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='\u0000' && LA1_0<='\t')||(LA1_0>='\u000B' && LA1_0<='\f')||(LA1_0>='\u000E' && LA1_0<='\uFFFF')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:9513:20: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalSmartContractSpecification.g:9513:36: ( ( '\\r' )? '\\n' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='\n'||LA3_0=='\r') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalSmartContractSpecification.g:9513:37: ( '\\r' )? '\\n'
                    {
                    // InternalSmartContractSpecification.g:9513:37: ( '\\r' )?
                    int alt2=2;
                    int LA2_0 = input.LA(1);

                    if ( (LA2_0=='\r') ) {
                        alt2=1;
                    }
                    switch (alt2) {
                        case 1 :
                            // InternalSmartContractSpecification.g:9513:37: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SLD"

    // $ANTLR start "RULE_PLAINTEXT"
    public final void mRULE_PLAINTEXT() throws RecognitionException {
        try {
            int _type = RULE_PLAINTEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9515:16: ( '#' ( '\\\\' . | ~ ( ( '\\\\' | '#' ) ) )* '#' )
            // InternalSmartContractSpecification.g:9515:18: '#' ( '\\\\' . | ~ ( ( '\\\\' | '#' ) ) )* '#'
            {
            match('#'); 
            // InternalSmartContractSpecification.g:9515:22: ( '\\\\' . | ~ ( ( '\\\\' | '#' ) ) )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='\\') ) {
                    alt4=1;
                }
                else if ( ((LA4_0>='\u0000' && LA4_0<='\"')||(LA4_0>='$' && LA4_0<='[')||(LA4_0>=']' && LA4_0<='\uFFFF')) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:9515:23: '\\\\' .
            	    {
            	    match('\\'); 
            	    matchAny(); 

            	    }
            	    break;
            	case 2 :
            	    // InternalSmartContractSpecification.g:9515:30: ~ ( ( '\\\\' | '#' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\"')||(input.LA(1)>='$' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            match('#'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PLAINTEXT"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9517:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalSmartContractSpecification.g:9517:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalSmartContractSpecification.g:9517:11: ( '^' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='^') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalSmartContractSpecification.g:9517:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalSmartContractSpecification.g:9517:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')||(LA6_0>='A' && LA6_0<='Z')||LA6_0=='_'||(LA6_0>='a' && LA6_0<='z')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9519:10: ( ( '0' .. '9' )+ )
            // InternalSmartContractSpecification.g:9519:12: ( '0' .. '9' )+
            {
            // InternalSmartContractSpecification.g:9519:12: ( '0' .. '9' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:9519:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9521:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalSmartContractSpecification.g:9521:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalSmartContractSpecification.g:9521:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\"') ) {
                alt10=1;
            }
            else if ( (LA10_0=='\'') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalSmartContractSpecification.g:9521:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalSmartContractSpecification.g:9521:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop8:
                    do {
                        int alt8=3;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\\') ) {
                            alt8=1;
                        }
                        else if ( ((LA8_0>='\u0000' && LA8_0<='!')||(LA8_0>='#' && LA8_0<='[')||(LA8_0>=']' && LA8_0<='\uFFFF')) ) {
                            alt8=2;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:9521:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalSmartContractSpecification.g:9521:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:9521:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalSmartContractSpecification.g:9521:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop9:
                    do {
                        int alt9=3;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0=='\\') ) {
                            alt9=1;
                        }
                        else if ( ((LA9_0>='\u0000' && LA9_0<='&')||(LA9_0>='(' && LA9_0<='[')||(LA9_0>=']' && LA9_0<='\uFFFF')) ) {
                            alt9=2;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:9521:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalSmartContractSpecification.g:9521:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9523:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalSmartContractSpecification.g:9523:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalSmartContractSpecification.g:9523:24: ( options {greedy=false; } : . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='*') ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1=='/') ) {
                        alt11=2;
                    }
                    else if ( ((LA11_1>='\u0000' && LA11_1<='.')||(LA11_1>='0' && LA11_1<='\uFFFF')) ) {
                        alt11=1;
                    }


                }
                else if ( ((LA11_0>='\u0000' && LA11_0<=')')||(LA11_0>='+' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:9523:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9525:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalSmartContractSpecification.g:9525:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalSmartContractSpecification.g:9525:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\u0000' && LA12_0<='\t')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\uFFFF')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:9525:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            // InternalSmartContractSpecification.g:9525:40: ( ( '\\r' )? '\\n' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='\n'||LA14_0=='\r') ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalSmartContractSpecification.g:9525:41: ( '\\r' )? '\\n'
                    {
                    // InternalSmartContractSpecification.g:9525:41: ( '\\r' )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0=='\r') ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalSmartContractSpecification.g:9525:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9527:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalSmartContractSpecification.g:9527:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalSmartContractSpecification.g:9527:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\t' && LA15_0<='\n')||LA15_0=='\r'||LA15_0==' ') ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSmartContractSpecification.g:9529:16: ( . )
            // InternalSmartContractSpecification.g:9529:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalSmartContractSpecification.g:1:8: ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | RULE_SLD | RULE_PLAINTEXT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt16=96;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // InternalSmartContractSpecification.g:1:10: T__13
                {
                mT__13(); 

                }
                break;
            case 2 :
                // InternalSmartContractSpecification.g:1:16: T__14
                {
                mT__14(); 

                }
                break;
            case 3 :
                // InternalSmartContractSpecification.g:1:22: T__15
                {
                mT__15(); 

                }
                break;
            case 4 :
                // InternalSmartContractSpecification.g:1:28: T__16
                {
                mT__16(); 

                }
                break;
            case 5 :
                // InternalSmartContractSpecification.g:1:34: T__17
                {
                mT__17(); 

                }
                break;
            case 6 :
                // InternalSmartContractSpecification.g:1:40: T__18
                {
                mT__18(); 

                }
                break;
            case 7 :
                // InternalSmartContractSpecification.g:1:46: T__19
                {
                mT__19(); 

                }
                break;
            case 8 :
                // InternalSmartContractSpecification.g:1:52: T__20
                {
                mT__20(); 

                }
                break;
            case 9 :
                // InternalSmartContractSpecification.g:1:58: T__21
                {
                mT__21(); 

                }
                break;
            case 10 :
                // InternalSmartContractSpecification.g:1:64: T__22
                {
                mT__22(); 

                }
                break;
            case 11 :
                // InternalSmartContractSpecification.g:1:70: T__23
                {
                mT__23(); 

                }
                break;
            case 12 :
                // InternalSmartContractSpecification.g:1:76: T__24
                {
                mT__24(); 

                }
                break;
            case 13 :
                // InternalSmartContractSpecification.g:1:82: T__25
                {
                mT__25(); 

                }
                break;
            case 14 :
                // InternalSmartContractSpecification.g:1:88: T__26
                {
                mT__26(); 

                }
                break;
            case 15 :
                // InternalSmartContractSpecification.g:1:94: T__27
                {
                mT__27(); 

                }
                break;
            case 16 :
                // InternalSmartContractSpecification.g:1:100: T__28
                {
                mT__28(); 

                }
                break;
            case 17 :
                // InternalSmartContractSpecification.g:1:106: T__29
                {
                mT__29(); 

                }
                break;
            case 18 :
                // InternalSmartContractSpecification.g:1:112: T__30
                {
                mT__30(); 

                }
                break;
            case 19 :
                // InternalSmartContractSpecification.g:1:118: T__31
                {
                mT__31(); 

                }
                break;
            case 20 :
                // InternalSmartContractSpecification.g:1:124: T__32
                {
                mT__32(); 

                }
                break;
            case 21 :
                // InternalSmartContractSpecification.g:1:130: T__33
                {
                mT__33(); 

                }
                break;
            case 22 :
                // InternalSmartContractSpecification.g:1:136: T__34
                {
                mT__34(); 

                }
                break;
            case 23 :
                // InternalSmartContractSpecification.g:1:142: T__35
                {
                mT__35(); 

                }
                break;
            case 24 :
                // InternalSmartContractSpecification.g:1:148: T__36
                {
                mT__36(); 

                }
                break;
            case 25 :
                // InternalSmartContractSpecification.g:1:154: T__37
                {
                mT__37(); 

                }
                break;
            case 26 :
                // InternalSmartContractSpecification.g:1:160: T__38
                {
                mT__38(); 

                }
                break;
            case 27 :
                // InternalSmartContractSpecification.g:1:166: T__39
                {
                mT__39(); 

                }
                break;
            case 28 :
                // InternalSmartContractSpecification.g:1:172: T__40
                {
                mT__40(); 

                }
                break;
            case 29 :
                // InternalSmartContractSpecification.g:1:178: T__41
                {
                mT__41(); 

                }
                break;
            case 30 :
                // InternalSmartContractSpecification.g:1:184: T__42
                {
                mT__42(); 

                }
                break;
            case 31 :
                // InternalSmartContractSpecification.g:1:190: T__43
                {
                mT__43(); 

                }
                break;
            case 32 :
                // InternalSmartContractSpecification.g:1:196: T__44
                {
                mT__44(); 

                }
                break;
            case 33 :
                // InternalSmartContractSpecification.g:1:202: T__45
                {
                mT__45(); 

                }
                break;
            case 34 :
                // InternalSmartContractSpecification.g:1:208: T__46
                {
                mT__46(); 

                }
                break;
            case 35 :
                // InternalSmartContractSpecification.g:1:214: T__47
                {
                mT__47(); 

                }
                break;
            case 36 :
                // InternalSmartContractSpecification.g:1:220: T__48
                {
                mT__48(); 

                }
                break;
            case 37 :
                // InternalSmartContractSpecification.g:1:226: T__49
                {
                mT__49(); 

                }
                break;
            case 38 :
                // InternalSmartContractSpecification.g:1:232: T__50
                {
                mT__50(); 

                }
                break;
            case 39 :
                // InternalSmartContractSpecification.g:1:238: T__51
                {
                mT__51(); 

                }
                break;
            case 40 :
                // InternalSmartContractSpecification.g:1:244: T__52
                {
                mT__52(); 

                }
                break;
            case 41 :
                // InternalSmartContractSpecification.g:1:250: T__53
                {
                mT__53(); 

                }
                break;
            case 42 :
                // InternalSmartContractSpecification.g:1:256: T__54
                {
                mT__54(); 

                }
                break;
            case 43 :
                // InternalSmartContractSpecification.g:1:262: T__55
                {
                mT__55(); 

                }
                break;
            case 44 :
                // InternalSmartContractSpecification.g:1:268: T__56
                {
                mT__56(); 

                }
                break;
            case 45 :
                // InternalSmartContractSpecification.g:1:274: T__57
                {
                mT__57(); 

                }
                break;
            case 46 :
                // InternalSmartContractSpecification.g:1:280: T__58
                {
                mT__58(); 

                }
                break;
            case 47 :
                // InternalSmartContractSpecification.g:1:286: T__59
                {
                mT__59(); 

                }
                break;
            case 48 :
                // InternalSmartContractSpecification.g:1:292: T__60
                {
                mT__60(); 

                }
                break;
            case 49 :
                // InternalSmartContractSpecification.g:1:298: T__61
                {
                mT__61(); 

                }
                break;
            case 50 :
                // InternalSmartContractSpecification.g:1:304: T__62
                {
                mT__62(); 

                }
                break;
            case 51 :
                // InternalSmartContractSpecification.g:1:310: T__63
                {
                mT__63(); 

                }
                break;
            case 52 :
                // InternalSmartContractSpecification.g:1:316: T__64
                {
                mT__64(); 

                }
                break;
            case 53 :
                // InternalSmartContractSpecification.g:1:322: T__65
                {
                mT__65(); 

                }
                break;
            case 54 :
                // InternalSmartContractSpecification.g:1:328: T__66
                {
                mT__66(); 

                }
                break;
            case 55 :
                // InternalSmartContractSpecification.g:1:334: T__67
                {
                mT__67(); 

                }
                break;
            case 56 :
                // InternalSmartContractSpecification.g:1:340: T__68
                {
                mT__68(); 

                }
                break;
            case 57 :
                // InternalSmartContractSpecification.g:1:346: T__69
                {
                mT__69(); 

                }
                break;
            case 58 :
                // InternalSmartContractSpecification.g:1:352: T__70
                {
                mT__70(); 

                }
                break;
            case 59 :
                // InternalSmartContractSpecification.g:1:358: T__71
                {
                mT__71(); 

                }
                break;
            case 60 :
                // InternalSmartContractSpecification.g:1:364: T__72
                {
                mT__72(); 

                }
                break;
            case 61 :
                // InternalSmartContractSpecification.g:1:370: T__73
                {
                mT__73(); 

                }
                break;
            case 62 :
                // InternalSmartContractSpecification.g:1:376: T__74
                {
                mT__74(); 

                }
                break;
            case 63 :
                // InternalSmartContractSpecification.g:1:382: T__75
                {
                mT__75(); 

                }
                break;
            case 64 :
                // InternalSmartContractSpecification.g:1:388: T__76
                {
                mT__76(); 

                }
                break;
            case 65 :
                // InternalSmartContractSpecification.g:1:394: T__77
                {
                mT__77(); 

                }
                break;
            case 66 :
                // InternalSmartContractSpecification.g:1:400: T__78
                {
                mT__78(); 

                }
                break;
            case 67 :
                // InternalSmartContractSpecification.g:1:406: T__79
                {
                mT__79(); 

                }
                break;
            case 68 :
                // InternalSmartContractSpecification.g:1:412: T__80
                {
                mT__80(); 

                }
                break;
            case 69 :
                // InternalSmartContractSpecification.g:1:418: T__81
                {
                mT__81(); 

                }
                break;
            case 70 :
                // InternalSmartContractSpecification.g:1:424: T__82
                {
                mT__82(); 

                }
                break;
            case 71 :
                // InternalSmartContractSpecification.g:1:430: T__83
                {
                mT__83(); 

                }
                break;
            case 72 :
                // InternalSmartContractSpecification.g:1:436: T__84
                {
                mT__84(); 

                }
                break;
            case 73 :
                // InternalSmartContractSpecification.g:1:442: T__85
                {
                mT__85(); 

                }
                break;
            case 74 :
                // InternalSmartContractSpecification.g:1:448: T__86
                {
                mT__86(); 

                }
                break;
            case 75 :
                // InternalSmartContractSpecification.g:1:454: T__87
                {
                mT__87(); 

                }
                break;
            case 76 :
                // InternalSmartContractSpecification.g:1:460: T__88
                {
                mT__88(); 

                }
                break;
            case 77 :
                // InternalSmartContractSpecification.g:1:466: T__89
                {
                mT__89(); 

                }
                break;
            case 78 :
                // InternalSmartContractSpecification.g:1:472: T__90
                {
                mT__90(); 

                }
                break;
            case 79 :
                // InternalSmartContractSpecification.g:1:478: T__91
                {
                mT__91(); 

                }
                break;
            case 80 :
                // InternalSmartContractSpecification.g:1:484: T__92
                {
                mT__92(); 

                }
                break;
            case 81 :
                // InternalSmartContractSpecification.g:1:490: T__93
                {
                mT__93(); 

                }
                break;
            case 82 :
                // InternalSmartContractSpecification.g:1:496: T__94
                {
                mT__94(); 

                }
                break;
            case 83 :
                // InternalSmartContractSpecification.g:1:502: T__95
                {
                mT__95(); 

                }
                break;
            case 84 :
                // InternalSmartContractSpecification.g:1:508: T__96
                {
                mT__96(); 

                }
                break;
            case 85 :
                // InternalSmartContractSpecification.g:1:514: T__97
                {
                mT__97(); 

                }
                break;
            case 86 :
                // InternalSmartContractSpecification.g:1:520: T__98
                {
                mT__98(); 

                }
                break;
            case 87 :
                // InternalSmartContractSpecification.g:1:526: T__99
                {
                mT__99(); 

                }
                break;
            case 88 :
                // InternalSmartContractSpecification.g:1:532: RULE_SLD
                {
                mRULE_SLD(); 

                }
                break;
            case 89 :
                // InternalSmartContractSpecification.g:1:541: RULE_PLAINTEXT
                {
                mRULE_PLAINTEXT(); 

                }
                break;
            case 90 :
                // InternalSmartContractSpecification.g:1:556: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 91 :
                // InternalSmartContractSpecification.g:1:564: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 92 :
                // InternalSmartContractSpecification.g:1:573: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 93 :
                // InternalSmartContractSpecification.g:1:585: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 94 :
                // InternalSmartContractSpecification.g:1:601: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 95 :
                // InternalSmartContractSpecification.g:1:617: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 96 :
                // InternalSmartContractSpecification.g:1:625: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA16_eotS =
        "\1\uffff\1\60\1\55\1\60\1\55\6\60\1\uffff\1\60\1\122\1\124\1\55\1\60\3\uffff\1\134\1\135\2\60\3\uffff\1\145\2\60\3\uffff\3\60\1\uffff\1\60\1\uffff\1\55\2\uffff\2\55\2\uffff\1\172\1\60\2\uffff\3\60\1\uffff\1\u0081\13\60\1\u0092\7\60\1\uffff\1\u009d\1\u009e\1\u009f\1\60\7\uffff\1\60\7\uffff\3\60\5\uffff\2\60\3\uffff\10\60\1\uffff\1\60\6\uffff\1\u00b3\1\u00b4\1\u00b5\1\u00b6\1\60\1\u00b9\1\uffff\1\u00ba\2\60\1\u00bd\3\60\1\u00c1\1\u00c2\3\60\1\u00c7\3\60\1\uffff\7\60\1\u00d2\1\60\4\uffff\3\60\1\u00da\1\u00db\1\60\1\u00dd\7\60\1\u00e6\1\60\1\u00e8\1\u00e9\1\60\4\uffff\2\60\2\uffff\1\60\1\u00ee\1\uffff\1\u00ef\1\60\1\u00f1\2\uffff\1\u00f2\1\60\1\u00f4\1\u00f5\1\uffff\1\u00f6\1\u00f7\1\u00f8\7\60\1\uffff\1\u0100\3\uffff\3\60\2\uffff\1\60\1\uffff\2\60\1\u0107\3\60\1\u010c\1\60\1\uffff\1\60\2\uffff\1\u010f\1\u0110\2\60\2\uffff\1\u0113\2\uffff\1\60\5\uffff\1\u0115\2\60\1\u0118\3\60\1\uffff\4\60\1\u0120\1\u0121\1\uffff\1\u0122\1\u0123\2\60\1\uffff\1\u0126\1\60\2\uffff\2\60\1\uffff\1\60\1\uffff\1\60\1\u012c\1\uffff\1\60\1\u012e\3\60\1\u0132\1\60\4\uffff\1\60\1\u0135\1\uffff\1\60\1\u0137\1\u0138\2\60\1\uffff\1\60\1\uffff\1\60\1\u013d\1\60\1\uffff\2\60\1\uffff\1\u0141\2\uffff\1\u0142\1\60\1\u0144\1\u0145\2\uffff\1\u0146\1\u0147\2\uffff\1\u0148\5\uffff";
    static final String DFA16_eofS =
        "\u0149\uffff";
    static final String DFA16_minS =
        "\1\0\1\156\1\174\1\146\1\46\3\145\2\141\1\154\1\uffff\1\56\3\75\1\145\3\uffff\1\52\1\101\1\157\1\141\3\uffff\1\72\1\141\1\162\3\uffff\1\150\1\151\1\141\1\uffff\1\145\1\uffff\1\0\2\uffff\2\0\2\uffff\1\60\1\145\2\uffff\1\144\1\154\1\164\1\uffff\1\60\1\155\1\165\1\145\1\155\1\141\2\143\2\141\1\160\1\162\1\60\2\154\1\162\1\154\1\145\1\144\1\163\1\uffff\1\40\2\60\1\160\7\uffff\1\146\7\uffff\1\164\2\156\5\uffff\1\162\1\157\3\uffff\1\145\1\164\1\163\2\156\1\160\1\144\1\171\1\uffff\1\141\6\uffff\4\60\1\145\1\60\1\uffff\1\60\1\163\1\162\1\60\1\145\1\162\1\150\2\60\1\145\1\156\1\163\1\60\1\164\1\145\1\155\1\uffff\1\163\1\146\1\101\1\151\1\143\1\163\1\145\1\60\1\145\1\141\3\uffff\1\154\2\157\2\60\1\164\1\60\1\164\1\165\1\156\1\154\1\150\2\164\1\60\1\157\2\60\1\162\4\uffff\1\162\1\145\2\uffff\1\145\1\60\1\uffff\1\60\1\164\1\60\2\uffff\1\60\1\163\2\60\1\uffff\3\60\1\145\1\151\1\154\1\144\1\165\1\164\1\162\1\uffff\1\60\3\uffff\1\151\1\156\1\162\2\uffff\1\162\1\uffff\1\171\1\160\1\60\2\145\1\144\1\60\1\150\1\uffff\1\163\2\uffff\2\60\2\154\2\uffff\1\60\2\uffff\1\146\5\uffff\1\60\2\154\1\60\1\164\1\163\1\156\1\uffff\1\145\1\147\1\145\1\141\2\60\1\uffff\2\60\1\162\1\156\1\uffff\1\60\1\151\2\uffff\2\146\1\uffff\1\145\1\uffff\1\154\1\60\1\uffff\1\145\1\60\1\141\2\163\1\60\1\143\4\uffff\1\141\1\60\1\uffff\1\164\2\60\1\162\1\145\1\uffff\1\144\1\uffff\1\154\1\60\1\40\1\uffff\1\164\1\167\1\uffff\1\60\2\uffff\1\60\1\144\2\60\2\uffff\2\60\2\uffff\1\60\5\uffff";
    static final String DFA16_maxS =
        "\1\uffff\1\162\1\174\1\156\1\46\1\157\1\165\1\171\1\165\1\141\1\170\1\uffff\1\163\1\76\2\75\1\145\3\uffff\1\57\1\172\2\157\3\uffff\1\72\1\141\1\162\3\uffff\1\151\1\165\1\151\1\uffff\1\145\1\uffff\1\uffff\2\uffff\2\uffff\2\uffff\1\172\1\145\2\uffff\1\171\1\154\1\164\1\uffff\1\172\1\163\1\165\1\145\1\155\1\141\1\143\1\164\1\165\1\151\1\160\1\162\1\172\2\154\1\162\1\154\1\164\1\144\1\163\1\uffff\3\172\1\160\7\uffff\1\154\7\uffff\1\167\2\156\5\uffff\1\162\1\157\3\uffff\1\151\1\164\1\163\2\156\1\160\1\144\1\171\1\uffff\1\141\6\uffff\4\172\1\145\1\172\1\uffff\1\172\1\163\1\162\1\172\1\145\1\162\1\150\2\172\1\145\1\156\1\163\1\172\1\164\1\145\1\155\1\uffff\1\163\1\146\1\101\1\151\1\143\1\163\1\145\1\172\1\145\1\156\3\uffff\1\154\2\157\2\172\1\164\1\172\1\164\1\165\1\162\1\154\1\150\2\164\1\172\1\157\2\172\1\162\4\uffff\1\162\1\145\2\uffff\1\145\1\172\1\uffff\1\172\1\164\1\172\2\uffff\1\172\1\163\2\172\1\uffff\3\172\1\145\1\151\1\154\1\144\1\165\1\164\1\162\1\uffff\1\172\3\uffff\1\151\1\156\1\162\2\uffff\1\162\1\uffff\1\171\1\160\1\172\2\145\1\151\1\172\1\150\1\uffff\1\163\2\uffff\2\172\2\154\2\uffff\1\172\2\uffff\1\146\5\uffff\1\172\2\154\1\172\1\164\1\163\1\156\1\uffff\1\145\1\147\1\145\1\141\2\172\1\uffff\2\172\1\162\1\156\1\uffff\1\172\1\151\2\uffff\2\146\1\uffff\1\145\1\uffff\1\154\1\172\1\uffff\1\145\1\172\1\141\2\163\1\172\1\143\4\uffff\1\141\1\172\1\uffff\1\164\2\172\1\162\1\145\1\uffff\1\144\1\uffff\1\154\1\172\1\40\1\uffff\1\164\1\167\1\uffff\1\172\2\uffff\1\172\1\144\2\172\2\uffff\2\172\2\uffff\1\172\5\uffff";
    static final String DFA16_acceptS =
        "\13\uffff\1\24\5\uffff\1\37\1\40\1\41\4\uffff\1\56\1\57\1\60\3\uffff\1\66\1\67\1\70\3\uffff\1\100\1\uffff\1\125\1\uffff\1\132\1\133\2\uffff\1\137\1\140\2\uffff\1\132\1\2\3\uffff\1\4\24\uffff\1\24\4\uffff\1\130\1\27\1\33\1\26\1\31\1\30\1\32\1\uffff\1\37\1\40\1\41\1\135\1\136\1\42\1\43\3\uffff\1\56\1\57\1\60\1\114\1\62\2\uffff\1\66\1\67\1\70\10\uffff\1\100\1\uffff\1\125\1\131\1\133\1\134\1\137\1\1\6\uffff\1\5\20\uffff\1\103\12\uffff\1\116\1\104\1\107\23\uffff\1\47\1\3\1\46\1\20\2\uffff\1\10\1\7\2\uffff\1\6\3\uffff\1\124\1\126\4\uffff\1\51\12\uffff\1\54\1\uffff\1\25\1\34\1\36\3\uffff\1\52\1\113\1\uffff\1\72\10\uffff\1\123\1\uffff\1\115\1\121\4\uffff\1\122\1\22\1\uffff\1\105\1\13\1\uffff\1\50\1\110\1\106\1\61\1\71\7\uffff\1\111\6\uffff\1\73\4\uffff\1\76\2\uffff\1\117\1\45\2\uffff\1\53\1\uffff\1\14\2\uffff\1\15\7\uffff\1\63\1\64\1\75\1\74\2\uffff\1\120\5\uffff\1\21\1\uffff\1\23\3\uffff\1\44\2\uffff\1\127\1\uffff\1\12\1\11\4\uffff\1\112\1\35\2\uffff\1\77\1\102\1\uffff\1\17\1\65\1\55\1\101\1\16";
    static final String DFA16_specialS =
        "\1\3\46\uffff\1\1\2\uffff\1\0\1\2\u011d\uffff}>";
    static final String[] DFA16_transitionS = {
            "\11\55\2\54\2\55\1\54\22\55\1\54\1\17\1\52\1\47\1\44\1\55\1\4\1\53\1\37\1\40\1\23\1\21\1\36\1\22\1\32\1\24\12\51\1\33\1\55\1\15\1\13\1\16\1\55\1\46\32\50\3\55\1\25\1\50\1\55\1\3\1\20\1\27\1\43\1\12\1\10\1\35\1\5\1\14\3\50\1\42\1\26\1\1\1\34\2\50\1\6\1\7\1\50\1\11\1\41\1\50\1\45\1\50\1\30\1\2\1\31\uff82\55",
            "\1\57\3\uffff\1\56",
            "\1\61",
            "\1\64\5\uffff\1\63\1\uffff\1\62",
            "\1\65",
            "\1\66\3\uffff\1\67\5\uffff\1\70",
            "\1\75\2\uffff\1\71\6\uffff\1\72\4\uffff\1\73\1\74",
            "\1\101\2\uffff\1\77\6\uffff\1\102\2\uffff\1\76\6\uffff\1\100",
            "\1\103\15\uffff\1\105\5\uffff\1\104",
            "\1\106",
            "\1\111\1\uffff\1\110\11\uffff\1\107",
            "",
            "\1\117\67\uffff\1\115\6\uffff\1\116\1\114\4\uffff\1\113",
            "\1\120\1\121",
            "\1\123",
            "\1\125",
            "\1\126",
            "",
            "",
            "",
            "\1\132\4\uffff\1\133",
            "\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\136",
            "\1\140\15\uffff\1\137",
            "",
            "",
            "",
            "\1\144",
            "\1\146",
            "\1\147",
            "",
            "",
            "",
            "\1\153\1\154",
            "\1\157\5\uffff\1\156\5\uffff\1\155",
            "\1\162\3\uffff\1\160\3\uffff\1\161",
            "",
            "\1\164",
            "",
            "\0\166",
            "",
            "",
            "\0\170",
            "\0\170",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\173",
            "",
            "",
            "\1\174\24\uffff\1\175",
            "\1\176",
            "\1\177",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\21\60\1\u0080\10\60",
            "\1\u0083\5\uffff\1\u0082",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089\20\uffff\1\u008a",
            "\1\u008c\23\uffff\1\u008b",
            "\1\u008f\3\uffff\1\u008e\3\uffff\1\u008d",
            "\1\u0090",
            "\1\u0091",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\1\u0096",
            "\1\u0097\3\uffff\1\u0098\12\uffff\1\u0099",
            "\1\u009a",
            "\1\u009b",
            "",
            "\1\u009c\17\uffff\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00a0",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00a2\5\uffff\1\u00a1",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00a4\2\uffff\1\u00a3",
            "\1\u00a5",
            "\1\u00a6",
            "",
            "",
            "",
            "",
            "",
            "\1\u00a7",
            "\1\u00a8",
            "",
            "",
            "",
            "\1\u00a9\3\uffff\1\u00aa",
            "\1\u00ab",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "\1\u00af",
            "\1\u00b0",
            "\1\u00b1",
            "",
            "\1\u00b2",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00b7",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\22\60\1\u00b8\7\60",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00bb",
            "\1\u00bc",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00c3",
            "\1\u00c4",
            "\1\u00c5",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\15\60\1\u00c6\14\60",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "",
            "\1\u00cb",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce",
            "\1\u00cf",
            "\1\u00d0",
            "\1\u00d1",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00d3",
            "\1\u00d6\3\uffff\1\u00d4\10\uffff\1\u00d5",
            "",
            "",
            "",
            "\1\u00d7",
            "\1\u00d8",
            "\1\u00d9",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00dc",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00de",
            "\1\u00df",
            "\1\u00e0\3\uffff\1\u00e1",
            "\1\u00e2",
            "\1\u00e3",
            "\1\u00e4",
            "\1\u00e5",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00e7",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00ea",
            "",
            "",
            "",
            "",
            "\1\u00eb",
            "\1\u00ec",
            "",
            "",
            "\1\u00ed",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00f0",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00f3",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00f9",
            "\1\u00fa",
            "\1\u00fb",
            "\1\u00fc",
            "\1\u00fd",
            "\1\u00fe",
            "\1\u00ff",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "",
            "\1\u0101",
            "\1\u0102",
            "\1\u0103",
            "",
            "",
            "\1\u0104",
            "",
            "\1\u0105",
            "\1\u0106",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0108",
            "\1\u0109",
            "\1\u010a\4\uffff\1\u010b",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u010d",
            "",
            "\1\u010e",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0111",
            "\1\u0112",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "\1\u0114",
            "",
            "",
            "",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0116",
            "\1\u0117",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0119",
            "\1\u011a",
            "\1\u011b",
            "",
            "\1\u011c",
            "\1\u011d",
            "\1\u011e",
            "\1\u011f",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0124",
            "\1\u0125",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0127",
            "",
            "",
            "\1\u0128",
            "\1\u0129",
            "",
            "\1\u012a",
            "",
            "\1\u012b",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\1\u012d",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u012f",
            "\1\u0130",
            "\1\u0131",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0133",
            "",
            "",
            "",
            "",
            "\1\u0134",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\1\u0136",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0139",
            "\1\u013a",
            "",
            "\1\u013b",
            "",
            "\1\u013c",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u013e",
            "",
            "\1\u013f",
            "\1\u0140",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0143",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | RULE_SLD | RULE_PLAINTEXT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA16_42 = input.LA(1);

                        s = -1;
                        if ( ((LA16_42>='\u0000' && LA16_42<='\uFFFF')) ) {s = 120;}

                        else s = 45;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA16_39 = input.LA(1);

                        s = -1;
                        if ( ((LA16_39>='\u0000' && LA16_39<='\uFFFF')) ) {s = 118;}

                        else s = 45;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA16_43 = input.LA(1);

                        s = -1;
                        if ( ((LA16_43>='\u0000' && LA16_43<='\uFFFF')) ) {s = 120;}

                        else s = 45;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA16_0 = input.LA(1);

                        s = -1;
                        if ( (LA16_0=='o') ) {s = 1;}

                        else if ( (LA16_0=='|') ) {s = 2;}

                        else if ( (LA16_0=='a') ) {s = 3;}

                        else if ( (LA16_0=='&') ) {s = 4;}

                        else if ( (LA16_0=='h') ) {s = 5;}

                        else if ( (LA16_0=='s') ) {s = 6;}

                        else if ( (LA16_0=='t') ) {s = 7;}

                        else if ( (LA16_0=='f') ) {s = 8;}

                        else if ( (LA16_0=='v') ) {s = 9;}

                        else if ( (LA16_0=='e') ) {s = 10;}

                        else if ( (LA16_0=='=') ) {s = 11;}

                        else if ( (LA16_0=='i') ) {s = 12;}

                        else if ( (LA16_0=='<') ) {s = 13;}

                        else if ( (LA16_0=='>') ) {s = 14;}

                        else if ( (LA16_0=='!') ) {s = 15;}

                        else if ( (LA16_0=='b') ) {s = 16;}

                        else if ( (LA16_0=='+') ) {s = 17;}

                        else if ( (LA16_0=='-') ) {s = 18;}

                        else if ( (LA16_0=='*') ) {s = 19;}

                        else if ( (LA16_0=='/') ) {s = 20;}

                        else if ( (LA16_0=='^') ) {s = 21;}

                        else if ( (LA16_0=='n') ) {s = 22;}

                        else if ( (LA16_0=='c') ) {s = 23;}

                        else if ( (LA16_0=='{') ) {s = 24;}

                        else if ( (LA16_0=='}') ) {s = 25;}

                        else if ( (LA16_0=='.') ) {s = 26;}

                        else if ( (LA16_0==':') ) {s = 27;}

                        else if ( (LA16_0=='p') ) {s = 28;}

                        else if ( (LA16_0=='g') ) {s = 29;}

                        else if ( (LA16_0==',') ) {s = 30;}

                        else if ( (LA16_0=='(') ) {s = 31;}

                        else if ( (LA16_0==')') ) {s = 32;}

                        else if ( (LA16_0=='w') ) {s = 33;}

                        else if ( (LA16_0=='m') ) {s = 34;}

                        else if ( (LA16_0=='d') ) {s = 35;}

                        else if ( (LA16_0=='$') ) {s = 36;}

                        else if ( (LA16_0=='y') ) {s = 37;}

                        else if ( (LA16_0=='@') ) {s = 38;}

                        else if ( (LA16_0=='#') ) {s = 39;}

                        else if ( ((LA16_0>='A' && LA16_0<='Z')||LA16_0=='_'||(LA16_0>='j' && LA16_0<='l')||(LA16_0>='q' && LA16_0<='r')||LA16_0=='u'||LA16_0=='x'||LA16_0=='z') ) {s = 40;}

                        else if ( ((LA16_0>='0' && LA16_0<='9')) ) {s = 41;}

                        else if ( (LA16_0=='\"') ) {s = 42;}

                        else if ( (LA16_0=='\'') ) {s = 43;}

                        else if ( ((LA16_0>='\t' && LA16_0<='\n')||LA16_0=='\r'||LA16_0==' ') ) {s = 44;}

                        else if ( ((LA16_0>='\u0000' && LA16_0<='\b')||(LA16_0>='\u000B' && LA16_0<='\f')||(LA16_0>='\u000E' && LA16_0<='\u001F')||LA16_0=='%'||LA16_0==';'||LA16_0=='?'||(LA16_0>='[' && LA16_0<=']')||LA16_0=='`'||(LA16_0>='~' && LA16_0<='\uFFFF')) ) {s = 45;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 16, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}