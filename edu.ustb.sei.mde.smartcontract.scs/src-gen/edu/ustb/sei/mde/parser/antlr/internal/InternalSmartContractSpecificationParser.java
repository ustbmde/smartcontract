package edu.ustb.sei.mde.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import edu.ustb.sei.mde.services.SmartContractSpecificationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalSmartContractSpecificationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_SLD", "RULE_PLAINTEXT", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'contract'", "'{'", "'.'", "'}'", "'type'", "':'", "'set'", "'party'", "'group'", "'external'", "','", "'('", "')'", "'term'", "'can'", "'when'", "'while'", "'where'", "'must'", "'deposit'", "'$'", "'withdraw'", "'transfer'", "'to'", "'in'", "'such'", "'that'", "'if'", "'then'", "'else'", "'implies'", "'or'", "'||'", "'and'", "'&&'", "'not'", "'this'", "'he'", "'she'", "'his'", "'her'", "'himself'", "'herself'", "'::'", "'-'", "'true'", "'false'", "'within'", "'did'", "'is'", "'valid'", "'fulfilled'", "'executed'", "'year'", "'month'", "'day'", "'hour'", "'min'", "'sec'", "'@'", "'all'", "'forAll'", "'some'", "'exists'", "'='", "'is equal to'", "'<'", "'<='", "'>'", "'>='", "'!='", "'<>'", "'is not equal to'", "'belongs to'", "'is a subset of'", "'+'", "'*'", "'/'", "'^'", "'before'", "'after'", "'any'", "'one'", "'the'", "'now'", "'start'", "'end'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=7;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_SLD=5;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int RULE_PLAINTEXT=6;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=8;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalSmartContractSpecificationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSmartContractSpecificationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSmartContractSpecificationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSmartContractSpecification.g"; }



     	private SmartContractSpecificationGrammarAccess grammarAccess;

        public InternalSmartContractSpecificationParser(TokenStream input, SmartContractSpecificationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Contract";
       	}

       	@Override
       	protected SmartContractSpecificationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleContract"
    // InternalSmartContractSpecification.g:65:1: entryRuleContract returns [EObject current=null] : iv_ruleContract= ruleContract EOF ;
    public final EObject entryRuleContract() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContract = null;


        try {
            // InternalSmartContractSpecification.g:65:49: (iv_ruleContract= ruleContract EOF )
            // InternalSmartContractSpecification.g:66:2: iv_ruleContract= ruleContract EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getContractRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleContract=ruleContract();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleContract; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContract"


    // $ANTLR start "ruleContract"
    // InternalSmartContractSpecification.g:72:1: ruleContract returns [EObject current=null] : (otherlv_0= 'contract' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_parties_3_0= ruleParty ) )+ ( (lv_fields_4_0= ruleField ) )* ( ( (lv_terms_5_0= ruleTerm ) ) otherlv_6= '.' )* ( (lv_types_7_0= ruleComplexType ) )* otherlv_8= '}' ) ;
    public final EObject ruleContract() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_parties_3_0 = null;

        EObject lv_fields_4_0 = null;

        EObject lv_terms_5_0 = null;

        EObject lv_types_7_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:78:2: ( (otherlv_0= 'contract' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_parties_3_0= ruleParty ) )+ ( (lv_fields_4_0= ruleField ) )* ( ( (lv_terms_5_0= ruleTerm ) ) otherlv_6= '.' )* ( (lv_types_7_0= ruleComplexType ) )* otherlv_8= '}' ) )
            // InternalSmartContractSpecification.g:79:2: (otherlv_0= 'contract' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_parties_3_0= ruleParty ) )+ ( (lv_fields_4_0= ruleField ) )* ( ( (lv_terms_5_0= ruleTerm ) ) otherlv_6= '.' )* ( (lv_types_7_0= ruleComplexType ) )* otherlv_8= '}' )
            {
            // InternalSmartContractSpecification.g:79:2: (otherlv_0= 'contract' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_parties_3_0= ruleParty ) )+ ( (lv_fields_4_0= ruleField ) )* ( ( (lv_terms_5_0= ruleTerm ) ) otherlv_6= '.' )* ( (lv_types_7_0= ruleComplexType ) )* otherlv_8= '}' )
            // InternalSmartContractSpecification.g:80:3: otherlv_0= 'contract' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_parties_3_0= ruleParty ) )+ ( (lv_fields_4_0= ruleField ) )* ( ( (lv_terms_5_0= ruleTerm ) ) otherlv_6= '.' )* ( (lv_types_7_0= ruleComplexType ) )* otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getContractAccess().getContractKeyword_0());
              		
            }
            // InternalSmartContractSpecification.g:84:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSmartContractSpecification.g:85:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSmartContractSpecification.g:85:4: (lv_name_1_0= RULE_ID )
            // InternalSmartContractSpecification.g:86:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getContractAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getContractRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getContractAccess().getLeftCurlyBracketKeyword_2());
              		
            }
            // InternalSmartContractSpecification.g:106:3: ( (lv_parties_3_0= ruleParty ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==20||LA1_0==22) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:107:4: (lv_parties_3_0= ruleParty )
            	    {
            	    // InternalSmartContractSpecification.g:107:4: (lv_parties_3_0= ruleParty )
            	    // InternalSmartContractSpecification.g:108:5: lv_parties_3_0= ruleParty
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getContractAccess().getPartiesPartyParserRuleCall_3_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_6);
            	    lv_parties_3_0=ruleParty();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getContractRule());
            	      					}
            	      					add(
            	      						current,
            	      						"parties",
            	      						lv_parties_3_0,
            	      						"edu.ustb.sei.mde.SmartContractSpecification.Party");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            // InternalSmartContractSpecification.g:125:3: ( (lv_fields_4_0= ruleField ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:126:4: (lv_fields_4_0= ruleField )
            	    {
            	    // InternalSmartContractSpecification.g:126:4: (lv_fields_4_0= ruleField )
            	    // InternalSmartContractSpecification.g:127:5: lv_fields_4_0= ruleField
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getContractAccess().getFieldsFieldParserRuleCall_4_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_7);
            	    lv_fields_4_0=ruleField();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getContractRule());
            	      					}
            	      					add(
            	      						current,
            	      						"fields",
            	      						lv_fields_4_0,
            	      						"edu.ustb.sei.mde.SmartContractSpecification.Field");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalSmartContractSpecification.g:144:3: ( ( (lv_terms_5_0= ruleTerm ) ) otherlv_6= '.' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==26) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:145:4: ( (lv_terms_5_0= ruleTerm ) ) otherlv_6= '.'
            	    {
            	    // InternalSmartContractSpecification.g:145:4: ( (lv_terms_5_0= ruleTerm ) )
            	    // InternalSmartContractSpecification.g:146:5: (lv_terms_5_0= ruleTerm )
            	    {
            	    // InternalSmartContractSpecification.g:146:5: (lv_terms_5_0= ruleTerm )
            	    // InternalSmartContractSpecification.g:147:6: lv_terms_5_0= ruleTerm
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getContractAccess().getTermsTermParserRuleCall_5_0_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_8);
            	    lv_terms_5_0=ruleTerm();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getContractRule());
            	      						}
            	      						add(
            	      							current,
            	      							"terms",
            	      							lv_terms_5_0,
            	      							"edu.ustb.sei.mde.SmartContractSpecification.Term");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    otherlv_6=(Token)match(input,15,FOLLOW_9); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_6, grammarAccess.getContractAccess().getFullStopKeyword_5_1());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalSmartContractSpecification.g:169:3: ( (lv_types_7_0= ruleComplexType ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:170:4: (lv_types_7_0= ruleComplexType )
            	    {
            	    // InternalSmartContractSpecification.g:170:4: (lv_types_7_0= ruleComplexType )
            	    // InternalSmartContractSpecification.g:171:5: lv_types_7_0= ruleComplexType
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getContractAccess().getTypesComplexTypeParserRuleCall_6_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_10);
            	    lv_types_7_0=ruleComplexType();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getContractRule());
            	      					}
            	      					add(
            	      						current,
            	      						"types",
            	      						lv_types_7_0,
            	      						"edu.ustb.sei.mde.SmartContractSpecification.ComplexType");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_8=(Token)match(input,16,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getContractAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContract"


    // $ANTLR start "entryRuleComplexType"
    // InternalSmartContractSpecification.g:196:1: entryRuleComplexType returns [EObject current=null] : iv_ruleComplexType= ruleComplexType EOF ;
    public final EObject entryRuleComplexType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplexType = null;


        try {
            // InternalSmartContractSpecification.g:196:52: (iv_ruleComplexType= ruleComplexType EOF )
            // InternalSmartContractSpecification.g:197:2: iv_ruleComplexType= ruleComplexType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComplexTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleComplexType=ruleComplexType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComplexType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplexType"


    // $ANTLR start "ruleComplexType"
    // InternalSmartContractSpecification.g:203:1: ruleComplexType returns [EObject current=null] : (otherlv_0= 'type' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleField ) )* otherlv_4= '}' ) ;
    public final EObject ruleComplexType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_fields_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:209:2: ( (otherlv_0= 'type' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleField ) )* otherlv_4= '}' ) )
            // InternalSmartContractSpecification.g:210:2: (otherlv_0= 'type' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleField ) )* otherlv_4= '}' )
            {
            // InternalSmartContractSpecification.g:210:2: (otherlv_0= 'type' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleField ) )* otherlv_4= '}' )
            // InternalSmartContractSpecification.g:211:3: otherlv_0= 'type' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleField ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getComplexTypeAccess().getTypeKeyword_0());
              		
            }
            // InternalSmartContractSpecification.g:215:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSmartContractSpecification.g:216:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSmartContractSpecification.g:216:4: (lv_name_1_0= RULE_ID )
            // InternalSmartContractSpecification.g:217:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getComplexTypeAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getComplexTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getComplexTypeAccess().getLeftCurlyBracketKeyword_2());
              		
            }
            // InternalSmartContractSpecification.g:237:3: ( (lv_fields_3_0= ruleField ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalSmartContractSpecification.g:238:4: (lv_fields_3_0= ruleField )
            	    {
            	    // InternalSmartContractSpecification.g:238:4: (lv_fields_3_0= ruleField )
            	    // InternalSmartContractSpecification.g:239:5: lv_fields_3_0= ruleField
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getComplexTypeAccess().getFieldsFieldParserRuleCall_3_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_11);
            	    lv_fields_3_0=ruleField();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getComplexTypeRule());
            	      					}
            	      					add(
            	      						current,
            	      						"fields",
            	      						lv_fields_3_0,
            	      						"edu.ustb.sei.mde.SmartContractSpecification.Field");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_4=(Token)match(input,16,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getComplexTypeAccess().getRightCurlyBracketKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplexType"


    // $ANTLR start "entryRuleField"
    // InternalSmartContractSpecification.g:264:1: entryRuleField returns [EObject current=null] : iv_ruleField= ruleField EOF ;
    public final EObject entryRuleField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleField = null;


        try {
            // InternalSmartContractSpecification.g:264:46: (iv_ruleField= ruleField EOF )
            // InternalSmartContractSpecification.g:265:2: iv_ruleField= ruleField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleField=ruleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleField; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleField"


    // $ANTLR start "ruleField"
    // InternalSmartContractSpecification.g:271:1: ruleField returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_many_2_0= 'set' ) )? ( (otherlv_3= RULE_ID ) ) (this_SLD_4= RULE_SLD )? ) ;
    public final EObject ruleField() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_many_2_0=null;
        Token otherlv_3=null;
        Token this_SLD_4=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:277:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_many_2_0= 'set' ) )? ( (otherlv_3= RULE_ID ) ) (this_SLD_4= RULE_SLD )? ) )
            // InternalSmartContractSpecification.g:278:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_many_2_0= 'set' ) )? ( (otherlv_3= RULE_ID ) ) (this_SLD_4= RULE_SLD )? )
            {
            // InternalSmartContractSpecification.g:278:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_many_2_0= 'set' ) )? ( (otherlv_3= RULE_ID ) ) (this_SLD_4= RULE_SLD )? )
            // InternalSmartContractSpecification.g:279:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_many_2_0= 'set' ) )? ( (otherlv_3= RULE_ID ) ) (this_SLD_4= RULE_SLD )?
            {
            // InternalSmartContractSpecification.g:279:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalSmartContractSpecification.g:280:4: (lv_name_0_0= RULE_ID )
            {
            // InternalSmartContractSpecification.g:280:4: (lv_name_0_0= RULE_ID )
            // InternalSmartContractSpecification.g:281:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_0_0, grammarAccess.getFieldAccess().getNameIDTerminalRuleCall_0_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFieldRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_0_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_1=(Token)match(input,18,FOLLOW_13); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getFieldAccess().getColonKeyword_1());
              		
            }
            // InternalSmartContractSpecification.g:301:3: ( (lv_many_2_0= 'set' ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalSmartContractSpecification.g:302:4: (lv_many_2_0= 'set' )
                    {
                    // InternalSmartContractSpecification.g:302:4: (lv_many_2_0= 'set' )
                    // InternalSmartContractSpecification.g:303:5: lv_many_2_0= 'set'
                    {
                    lv_many_2_0=(Token)match(input,19,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_many_2_0, grammarAccess.getFieldAccess().getManySetKeyword_2_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getFieldRule());
                      					}
                      					setWithLastConsumed(current, "many", true, "set");
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalSmartContractSpecification.g:315:3: ( (otherlv_3= RULE_ID ) )
            // InternalSmartContractSpecification.g:316:4: (otherlv_3= RULE_ID )
            {
            // InternalSmartContractSpecification.g:316:4: (otherlv_3= RULE_ID )
            // InternalSmartContractSpecification.g:317:5: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFieldRule());
              					}
              				
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_3, grammarAccess.getFieldAccess().getTypeTypeCrossReference_3_0());
              				
            }

            }


            }

            // InternalSmartContractSpecification.g:328:3: (this_SLD_4= RULE_SLD )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_SLD) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalSmartContractSpecification.g:329:4: this_SLD_4= RULE_SLD
                    {
                    this_SLD_4=(Token)match(input,RULE_SLD,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(this_SLD_4, grammarAccess.getFieldAccess().getSLDTerminalRuleCall_4());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleField"


    // $ANTLR start "entryRuleParty"
    // InternalSmartContractSpecification.g:338:1: entryRuleParty returns [EObject current=null] : iv_ruleParty= ruleParty EOF ;
    public final EObject entryRuleParty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParty = null;


        try {
            // InternalSmartContractSpecification.g:338:46: (iv_ruleParty= ruleParty EOF )
            // InternalSmartContractSpecification.g:339:2: iv_ruleParty= ruleParty EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPartyRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParty=ruleParty();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParty; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParty"


    // $ANTLR start "ruleParty"
    // InternalSmartContractSpecification.g:345:1: ruleParty returns [EObject current=null] : ( ( () otherlv_1= 'party' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_fields_4_0= ruleField ) )* ( (lv_actions_5_0= ruleAction ) )* otherlv_6= '}' ) | ( () otherlv_8= 'party' otherlv_9= 'group' ( (lv_name_10_0= RULE_ID ) ) otherlv_11= '{' ( (lv_fields_12_0= ruleField ) )* ( (lv_actions_13_0= ruleAction ) )* otherlv_14= '}' ) | ( () otherlv_16= 'external' otherlv_17= 'party' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= '{' ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )? otherlv_23= '}' ) ) ;
    public final EObject ruleParty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token lv_name_10_0=null;
        Token otherlv_11=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token lv_name_18_0=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        EObject lv_fields_4_0 = null;

        EObject lv_actions_5_0 = null;

        EObject lv_fields_12_0 = null;

        EObject lv_actions_13_0 = null;

        EObject lv_actions_20_0 = null;

        EObject lv_actions_22_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:351:2: ( ( ( () otherlv_1= 'party' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_fields_4_0= ruleField ) )* ( (lv_actions_5_0= ruleAction ) )* otherlv_6= '}' ) | ( () otherlv_8= 'party' otherlv_9= 'group' ( (lv_name_10_0= RULE_ID ) ) otherlv_11= '{' ( (lv_fields_12_0= ruleField ) )* ( (lv_actions_13_0= ruleAction ) )* otherlv_14= '}' ) | ( () otherlv_16= 'external' otherlv_17= 'party' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= '{' ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )? otherlv_23= '}' ) ) )
            // InternalSmartContractSpecification.g:352:2: ( ( () otherlv_1= 'party' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_fields_4_0= ruleField ) )* ( (lv_actions_5_0= ruleAction ) )* otherlv_6= '}' ) | ( () otherlv_8= 'party' otherlv_9= 'group' ( (lv_name_10_0= RULE_ID ) ) otherlv_11= '{' ( (lv_fields_12_0= ruleField ) )* ( (lv_actions_13_0= ruleAction ) )* otherlv_14= '}' ) | ( () otherlv_16= 'external' otherlv_17= 'party' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= '{' ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )? otherlv_23= '}' ) )
            {
            // InternalSmartContractSpecification.g:352:2: ( ( () otherlv_1= 'party' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_fields_4_0= ruleField ) )* ( (lv_actions_5_0= ruleAction ) )* otherlv_6= '}' ) | ( () otherlv_8= 'party' otherlv_9= 'group' ( (lv_name_10_0= RULE_ID ) ) otherlv_11= '{' ( (lv_fields_12_0= ruleField ) )* ( (lv_actions_13_0= ruleAction ) )* otherlv_14= '}' ) | ( () otherlv_16= 'external' otherlv_17= 'party' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= '{' ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )? otherlv_23= '}' ) )
            int alt14=3;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==20) ) {
                int LA14_1 = input.LA(2);

                if ( (LA14_1==RULE_ID) ) {
                    alt14=1;
                }
                else if ( (LA14_1==21) ) {
                    alt14=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA14_0==22) ) {
                alt14=3;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalSmartContractSpecification.g:353:3: ( () otherlv_1= 'party' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_fields_4_0= ruleField ) )* ( (lv_actions_5_0= ruleAction ) )* otherlv_6= '}' )
                    {
                    // InternalSmartContractSpecification.g:353:3: ( () otherlv_1= 'party' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_fields_4_0= ruleField ) )* ( (lv_actions_5_0= ruleAction ) )* otherlv_6= '}' )
                    // InternalSmartContractSpecification.g:354:4: () otherlv_1= 'party' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_fields_4_0= ruleField ) )* ( (lv_actions_5_0= ruleAction ) )* otherlv_6= '}'
                    {
                    // InternalSmartContractSpecification.g:354:4: ()
                    // InternalSmartContractSpecification.g:355:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getPartyAccess().getSinglePartyAction_0_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_1=(Token)match(input,20,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getPartyAccess().getPartyKeyword_0_1());
                      			
                    }
                    // InternalSmartContractSpecification.g:365:4: ( (lv_name_2_0= RULE_ID ) )
                    // InternalSmartContractSpecification.g:366:5: (lv_name_2_0= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:366:5: (lv_name_2_0= RULE_ID )
                    // InternalSmartContractSpecification.g:367:6: lv_name_2_0= RULE_ID
                    {
                    lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_2_0, grammarAccess.getPartyAccess().getNameIDTerminalRuleCall_0_2_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getPartyRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_2_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    otherlv_3=(Token)match(input,14,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getPartyAccess().getLeftCurlyBracketKeyword_0_3());
                      			
                    }
                    // InternalSmartContractSpecification.g:387:4: ( (lv_fields_4_0= ruleField ) )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==RULE_ID) ) {
                            int LA8_1 = input.LA(2);

                            if ( (LA8_1==18) ) {
                                alt8=1;
                            }


                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:388:5: (lv_fields_4_0= ruleField )
                    	    {
                    	    // InternalSmartContractSpecification.g:388:5: (lv_fields_4_0= ruleField )
                    	    // InternalSmartContractSpecification.g:389:6: lv_fields_4_0= ruleField
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getPartyAccess().getFieldsFieldParserRuleCall_0_4_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_11);
                    	    lv_fields_4_0=ruleField();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getPartyRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"fields",
                    	      							lv_fields_4_0,
                    	      							"edu.ustb.sei.mde.SmartContractSpecification.Field");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    // InternalSmartContractSpecification.g:406:4: ( (lv_actions_5_0= ruleAction ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==RULE_ID) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:407:5: (lv_actions_5_0= ruleAction )
                    	    {
                    	    // InternalSmartContractSpecification.g:407:5: (lv_actions_5_0= ruleAction )
                    	    // InternalSmartContractSpecification.g:408:6: lv_actions_5_0= ruleAction
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getPartyAccess().getActionsActionParserRuleCall_0_5_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_11);
                    	    lv_actions_5_0=ruleAction();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getPartyRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"actions",
                    	      							lv_actions_5_0,
                    	      							"edu.ustb.sei.mde.SmartContractSpecification.Action");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,16,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getPartyAccess().getRightCurlyBracketKeyword_0_6());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:431:3: ( () otherlv_8= 'party' otherlv_9= 'group' ( (lv_name_10_0= RULE_ID ) ) otherlv_11= '{' ( (lv_fields_12_0= ruleField ) )* ( (lv_actions_13_0= ruleAction ) )* otherlv_14= '}' )
                    {
                    // InternalSmartContractSpecification.g:431:3: ( () otherlv_8= 'party' otherlv_9= 'group' ( (lv_name_10_0= RULE_ID ) ) otherlv_11= '{' ( (lv_fields_12_0= ruleField ) )* ( (lv_actions_13_0= ruleAction ) )* otherlv_14= '}' )
                    // InternalSmartContractSpecification.g:432:4: () otherlv_8= 'party' otherlv_9= 'group' ( (lv_name_10_0= RULE_ID ) ) otherlv_11= '{' ( (lv_fields_12_0= ruleField ) )* ( (lv_actions_13_0= ruleAction ) )* otherlv_14= '}'
                    {
                    // InternalSmartContractSpecification.g:432:4: ()
                    // InternalSmartContractSpecification.g:433:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getPartyAccess().getGroupPartyAction_1_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_8=(Token)match(input,20,FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getPartyAccess().getPartyKeyword_1_1());
                      			
                    }
                    otherlv_9=(Token)match(input,21,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getPartyAccess().getGroupKeyword_1_2());
                      			
                    }
                    // InternalSmartContractSpecification.g:447:4: ( (lv_name_10_0= RULE_ID ) )
                    // InternalSmartContractSpecification.g:448:5: (lv_name_10_0= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:448:5: (lv_name_10_0= RULE_ID )
                    // InternalSmartContractSpecification.g:449:6: lv_name_10_0= RULE_ID
                    {
                    lv_name_10_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_10_0, grammarAccess.getPartyAccess().getNameIDTerminalRuleCall_1_3_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getPartyRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_10_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    otherlv_11=(Token)match(input,14,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_11, grammarAccess.getPartyAccess().getLeftCurlyBracketKeyword_1_4());
                      			
                    }
                    // InternalSmartContractSpecification.g:469:4: ( (lv_fields_12_0= ruleField ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==RULE_ID) ) {
                            int LA10_1 = input.LA(2);

                            if ( (LA10_1==18) ) {
                                alt10=1;
                            }


                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:470:5: (lv_fields_12_0= ruleField )
                    	    {
                    	    // InternalSmartContractSpecification.g:470:5: (lv_fields_12_0= ruleField )
                    	    // InternalSmartContractSpecification.g:471:6: lv_fields_12_0= ruleField
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getPartyAccess().getFieldsFieldParserRuleCall_1_5_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_11);
                    	    lv_fields_12_0=ruleField();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getPartyRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"fields",
                    	      							lv_fields_12_0,
                    	      							"edu.ustb.sei.mde.SmartContractSpecification.Field");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    // InternalSmartContractSpecification.g:488:4: ( (lv_actions_13_0= ruleAction ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==RULE_ID) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:489:5: (lv_actions_13_0= ruleAction )
                    	    {
                    	    // InternalSmartContractSpecification.g:489:5: (lv_actions_13_0= ruleAction )
                    	    // InternalSmartContractSpecification.g:490:6: lv_actions_13_0= ruleAction
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getPartyAccess().getActionsActionParserRuleCall_1_6_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_11);
                    	    lv_actions_13_0=ruleAction();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getPartyRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"actions",
                    	      							lv_actions_13_0,
                    	      							"edu.ustb.sei.mde.SmartContractSpecification.Action");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,16,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_14, grammarAccess.getPartyAccess().getRightCurlyBracketKeyword_1_7());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:513:3: ( () otherlv_16= 'external' otherlv_17= 'party' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= '{' ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )? otherlv_23= '}' )
                    {
                    // InternalSmartContractSpecification.g:513:3: ( () otherlv_16= 'external' otherlv_17= 'party' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= '{' ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )? otherlv_23= '}' )
                    // InternalSmartContractSpecification.g:514:4: () otherlv_16= 'external' otherlv_17= 'party' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= '{' ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )? otherlv_23= '}'
                    {
                    // InternalSmartContractSpecification.g:514:4: ()
                    // InternalSmartContractSpecification.g:515:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getPartyAccess().getExternalPartyAction_2_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_16=(Token)match(input,22,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_16, grammarAccess.getPartyAccess().getExternalKeyword_2_1());
                      			
                    }
                    otherlv_17=(Token)match(input,20,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_17, grammarAccess.getPartyAccess().getPartyKeyword_2_2());
                      			
                    }
                    // InternalSmartContractSpecification.g:529:4: ( (lv_name_18_0= RULE_ID ) )
                    // InternalSmartContractSpecification.g:530:5: (lv_name_18_0= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:530:5: (lv_name_18_0= RULE_ID )
                    // InternalSmartContractSpecification.g:531:6: lv_name_18_0= RULE_ID
                    {
                    lv_name_18_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_18_0, grammarAccess.getPartyAccess().getNameIDTerminalRuleCall_2_3_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getPartyRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_18_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    otherlv_19=(Token)match(input,14,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_19, grammarAccess.getPartyAccess().getLeftCurlyBracketKeyword_2_4());
                      			
                    }
                    // InternalSmartContractSpecification.g:551:4: ( ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )* )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==RULE_ID) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalSmartContractSpecification.g:552:5: ( (lv_actions_20_0= ruleAction ) ) (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )*
                            {
                            // InternalSmartContractSpecification.g:552:5: ( (lv_actions_20_0= ruleAction ) )
                            // InternalSmartContractSpecification.g:553:6: (lv_actions_20_0= ruleAction )
                            {
                            // InternalSmartContractSpecification.g:553:6: (lv_actions_20_0= ruleAction )
                            // InternalSmartContractSpecification.g:554:7: lv_actions_20_0= ruleAction
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getPartyAccess().getActionsActionParserRuleCall_2_5_0_0());
                              						
                            }
                            pushFollow(FOLLOW_17);
                            lv_actions_20_0=ruleAction();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getPartyRule());
                              							}
                              							add(
                              								current,
                              								"actions",
                              								lv_actions_20_0,
                              								"edu.ustb.sei.mde.SmartContractSpecification.Action");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }

                            // InternalSmartContractSpecification.g:571:5: (otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) ) )*
                            loop12:
                            do {
                                int alt12=2;
                                int LA12_0 = input.LA(1);

                                if ( (LA12_0==23) ) {
                                    alt12=1;
                                }


                                switch (alt12) {
                            	case 1 :
                            	    // InternalSmartContractSpecification.g:572:6: otherlv_21= ',' ( (lv_actions_22_0= ruleAction ) )
                            	    {
                            	    otherlv_21=(Token)match(input,23,FOLLOW_3); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      						newLeafNode(otherlv_21, grammarAccess.getPartyAccess().getCommaKeyword_2_5_1_0());
                            	      					
                            	    }
                            	    // InternalSmartContractSpecification.g:576:6: ( (lv_actions_22_0= ruleAction ) )
                            	    // InternalSmartContractSpecification.g:577:7: (lv_actions_22_0= ruleAction )
                            	    {
                            	    // InternalSmartContractSpecification.g:577:7: (lv_actions_22_0= ruleAction )
                            	    // InternalSmartContractSpecification.g:578:8: lv_actions_22_0= ruleAction
                            	    {
                            	    if ( state.backtracking==0 ) {

                            	      								newCompositeNode(grammarAccess.getPartyAccess().getActionsActionParserRuleCall_2_5_1_1_0());
                            	      							
                            	    }
                            	    pushFollow(FOLLOW_17);
                            	    lv_actions_22_0=ruleAction();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      								if (current==null) {
                            	      									current = createModelElementForParent(grammarAccess.getPartyRule());
                            	      								}
                            	      								add(
                            	      									current,
                            	      									"actions",
                            	      									lv_actions_22_0,
                            	      									"edu.ustb.sei.mde.SmartContractSpecification.Action");
                            	      								afterParserOrEnumRuleCall();
                            	      							
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop12;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_23=(Token)match(input,16,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_23, grammarAccess.getPartyAccess().getRightCurlyBracketKeyword_2_6());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParty"


    // $ANTLR start "entryRuleAction"
    // InternalSmartContractSpecification.g:606:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalSmartContractSpecification.g:606:47: (iv_ruleAction= ruleAction EOF )
            // InternalSmartContractSpecification.g:607:2: iv_ruleAction= ruleAction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalSmartContractSpecification.g:613:1: ruleAction returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleField ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) )? (this_SLD_8= RULE_SLD )? ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token this_SLD_8=null;
        EObject lv_parameters_2_0 = null;

        EObject lv_parameters_4_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:619:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleField ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) )? (this_SLD_8= RULE_SLD )? ) )
            // InternalSmartContractSpecification.g:620:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleField ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) )? (this_SLD_8= RULE_SLD )? )
            {
            // InternalSmartContractSpecification.g:620:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleField ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) )? (this_SLD_8= RULE_SLD )? )
            // InternalSmartContractSpecification.g:621:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleField ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) )? (this_SLD_8= RULE_SLD )?
            {
            // InternalSmartContractSpecification.g:621:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalSmartContractSpecification.g:622:4: (lv_name_0_0= RULE_ID )
            {
            // InternalSmartContractSpecification.g:622:4: (lv_name_0_0= RULE_ID )
            // InternalSmartContractSpecification.g:623:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_0_0, grammarAccess.getActionAccess().getNameIDTerminalRuleCall_0_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getActionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_0_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_1=(Token)match(input,24,FOLLOW_19); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getActionAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalSmartContractSpecification.g:643:3: ( ( (lv_parameters_2_0= ruleField ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )* )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalSmartContractSpecification.g:644:4: ( (lv_parameters_2_0= ruleField ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )*
                    {
                    // InternalSmartContractSpecification.g:644:4: ( (lv_parameters_2_0= ruleField ) )
                    // InternalSmartContractSpecification.g:645:5: (lv_parameters_2_0= ruleField )
                    {
                    // InternalSmartContractSpecification.g:645:5: (lv_parameters_2_0= ruleField )
                    // InternalSmartContractSpecification.g:646:6: lv_parameters_2_0= ruleField
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getActionAccess().getParametersFieldParserRuleCall_2_0_0());
                      					
                    }
                    pushFollow(FOLLOW_20);
                    lv_parameters_2_0=ruleField();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getActionRule());
                      						}
                      						add(
                      							current,
                      							"parameters",
                      							lv_parameters_2_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.Field");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalSmartContractSpecification.g:663:4: (otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==23) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:664:5: otherlv_3= ',' ( (lv_parameters_4_0= ruleField ) )
                    	    {
                    	    otherlv_3=(Token)match(input,23,FOLLOW_3); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_3, grammarAccess.getActionAccess().getCommaKeyword_2_1_0());
                    	      				
                    	    }
                    	    // InternalSmartContractSpecification.g:668:5: ( (lv_parameters_4_0= ruleField ) )
                    	    // InternalSmartContractSpecification.g:669:6: (lv_parameters_4_0= ruleField )
                    	    {
                    	    // InternalSmartContractSpecification.g:669:6: (lv_parameters_4_0= ruleField )
                    	    // InternalSmartContractSpecification.g:670:7: lv_parameters_4_0= ruleField
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getActionAccess().getParametersFieldParserRuleCall_2_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_20);
                    	    lv_parameters_4_0=ruleField();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getActionRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"parameters",
                    	      								lv_parameters_4_0,
                    	      								"edu.ustb.sei.mde.SmartContractSpecification.Field");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,25,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getActionAccess().getRightParenthesisKeyword_3());
              		
            }
            // InternalSmartContractSpecification.g:693:3: (otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==18) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalSmartContractSpecification.g:694:4: otherlv_6= ':' ( (otherlv_7= RULE_ID ) )
                    {
                    otherlv_6=(Token)match(input,18,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getActionAccess().getColonKeyword_4_0());
                      			
                    }
                    // InternalSmartContractSpecification.g:698:4: ( (otherlv_7= RULE_ID ) )
                    // InternalSmartContractSpecification.g:699:5: (otherlv_7= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:699:5: (otherlv_7= RULE_ID )
                    // InternalSmartContractSpecification.g:700:6: otherlv_7= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getActionRule());
                      						}
                      					
                    }
                    otherlv_7=(Token)match(input,RULE_ID,FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_7, grammarAccess.getActionAccess().getTypeTypeCrossReference_4_1_0());
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalSmartContractSpecification.g:712:3: (this_SLD_8= RULE_SLD )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_SLD) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalSmartContractSpecification.g:713:4: this_SLD_8= RULE_SLD
                    {
                    this_SLD_8=(Token)match(input,RULE_SLD,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(this_SLD_8, grammarAccess.getActionAccess().getSLDTerminalRuleCall_5());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleTerm"
    // InternalSmartContractSpecification.g:722:1: entryRuleTerm returns [EObject current=null] : iv_ruleTerm= ruleTerm EOF ;
    public final EObject entryRuleTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerm = null;


        try {
            // InternalSmartContractSpecification.g:722:45: (iv_ruleTerm= ruleTerm EOF )
            // InternalSmartContractSpecification.g:723:2: iv_ruleTerm= ruleTerm EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTermRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTerm=ruleTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTerm; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerm"


    // $ANTLR start "ruleTerm"
    // InternalSmartContractSpecification.g:729:1: ruleTerm returns [EObject current=null] : ( ( () otherlv_1= 'term' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'can' ( (otherlv_6= RULE_ID ) ) ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )? ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )? ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )? ) | ( () otherlv_17= 'term' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= ':' ( (otherlv_20= RULE_ID ) ) otherlv_21= 'must' ( (otherlv_22= RULE_ID ) ) ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )? ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )? ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )? ) ) ;
    public final EObject ruleTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_17=null;
        Token lv_name_18_0=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_30=null;
        EObject lv_condition_9_0 = null;

        EObject lv_transferOperations_12_0 = null;

        EObject lv_postCondition_15_0 = null;

        EObject lv_condition_25_0 = null;

        EObject lv_transferOperations_28_0 = null;

        EObject lv_postCondition_31_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:735:2: ( ( ( () otherlv_1= 'term' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'can' ( (otherlv_6= RULE_ID ) ) ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )? ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )? ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )? ) | ( () otherlv_17= 'term' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= ':' ( (otherlv_20= RULE_ID ) ) otherlv_21= 'must' ( (otherlv_22= RULE_ID ) ) ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )? ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )? ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )? ) ) )
            // InternalSmartContractSpecification.g:736:2: ( ( () otherlv_1= 'term' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'can' ( (otherlv_6= RULE_ID ) ) ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )? ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )? ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )? ) | ( () otherlv_17= 'term' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= ':' ( (otherlv_20= RULE_ID ) ) otherlv_21= 'must' ( (otherlv_22= RULE_ID ) ) ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )? ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )? ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )? ) )
            {
            // InternalSmartContractSpecification.g:736:2: ( ( () otherlv_1= 'term' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'can' ( (otherlv_6= RULE_ID ) ) ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )? ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )? ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )? ) | ( () otherlv_17= 'term' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= ':' ( (otherlv_20= RULE_ID ) ) otherlv_21= 'must' ( (otherlv_22= RULE_ID ) ) ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )? ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )? ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )? ) )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==26) ) {
                int LA33_1 = input.LA(2);

                if ( (LA33_1==RULE_ID) ) {
                    int LA33_2 = input.LA(3);

                    if ( (LA33_2==18) ) {
                        int LA33_3 = input.LA(4);

                        if ( (LA33_3==RULE_ID) ) {
                            int LA33_4 = input.LA(5);

                            if ( (LA33_4==31) ) {
                                alt33=2;
                            }
                            else if ( (LA33_4==27) ) {
                                alt33=1;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return current;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 33, 4, input);

                                throw nvae;
                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return current;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 33, 3, input);

                            throw nvae;
                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 33, 2, input);

                        throw nvae;
                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalSmartContractSpecification.g:737:3: ( () otherlv_1= 'term' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'can' ( (otherlv_6= RULE_ID ) ) ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )? ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )? ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )? )
                    {
                    // InternalSmartContractSpecification.g:737:3: ( () otherlv_1= 'term' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'can' ( (otherlv_6= RULE_ID ) ) ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )? ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )? ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )? )
                    // InternalSmartContractSpecification.g:738:4: () otherlv_1= 'term' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'can' ( (otherlv_6= RULE_ID ) ) ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )? ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )? ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )?
                    {
                    // InternalSmartContractSpecification.g:738:4: ()
                    // InternalSmartContractSpecification.g:739:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getTermAccess().getRightTermAction_0_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_1=(Token)match(input,26,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getTermAccess().getTermKeyword_0_1());
                      			
                    }
                    // InternalSmartContractSpecification.g:749:4: ( (lv_name_2_0= RULE_ID ) )
                    // InternalSmartContractSpecification.g:750:5: (lv_name_2_0= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:750:5: (lv_name_2_0= RULE_ID )
                    // InternalSmartContractSpecification.g:751:6: lv_name_2_0= RULE_ID
                    {
                    lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_2_0, grammarAccess.getTermAccess().getNameIDTerminalRuleCall_0_2_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTermRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_2_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    otherlv_3=(Token)match(input,18,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getTermAccess().getColonKeyword_0_3());
                      			
                    }
                    // InternalSmartContractSpecification.g:771:4: ( (otherlv_4= RULE_ID ) )
                    // InternalSmartContractSpecification.g:772:5: (otherlv_4= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:772:5: (otherlv_4= RULE_ID )
                    // InternalSmartContractSpecification.g:773:6: otherlv_4= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTermRule());
                      						}
                      					
                    }
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_22); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_4, grammarAccess.getTermAccess().getPartyInternalPartyCrossReference_0_4_0());
                      					
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,27,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getTermAccess().getCanKeyword_0_5());
                      			
                    }
                    // InternalSmartContractSpecification.g:788:4: ( (otherlv_6= RULE_ID ) )
                    // InternalSmartContractSpecification.g:789:5: (otherlv_6= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:789:5: (otherlv_6= RULE_ID )
                    // InternalSmartContractSpecification.g:790:6: otherlv_6= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTermRule());
                      						}
                      					
                    }
                    otherlv_6=(Token)match(input,RULE_ID,FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_6, grammarAccess.getTermAccess().getActionActionCrossReference_0_6_0());
                      					
                    }

                    }


                    }

                    // InternalSmartContractSpecification.g:801:4: ( (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) ) )?
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==23) ) {
                        int LA20_1 = input.LA(2);

                        if ( (LA20_1==28) ) {
                            alt20=1;
                        }
                    }
                    else if ( (LA20_0==28) ) {
                        alt20=1;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalSmartContractSpecification.g:802:5: (otherlv_7= ',' )? otherlv_8= 'when' ( (lv_condition_9_0= ruleExpression ) )
                            {
                            // InternalSmartContractSpecification.g:802:5: (otherlv_7= ',' )?
                            int alt19=2;
                            int LA19_0 = input.LA(1);

                            if ( (LA19_0==23) ) {
                                alt19=1;
                            }
                            switch (alt19) {
                                case 1 :
                                    // InternalSmartContractSpecification.g:803:6: otherlv_7= ','
                                    {
                                    otherlv_7=(Token)match(input,23,FOLLOW_24); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_7, grammarAccess.getTermAccess().getCommaKeyword_0_7_0());
                                      					
                                    }

                                    }
                                    break;

                            }

                            otherlv_8=(Token)match(input,28,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_8, grammarAccess.getTermAccess().getWhenKeyword_0_7_1());
                              				
                            }
                            // InternalSmartContractSpecification.g:812:5: ( (lv_condition_9_0= ruleExpression ) )
                            // InternalSmartContractSpecification.g:813:6: (lv_condition_9_0= ruleExpression )
                            {
                            // InternalSmartContractSpecification.g:813:6: (lv_condition_9_0= ruleExpression )
                            // InternalSmartContractSpecification.g:814:7: lv_condition_9_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getTermAccess().getConditionExpressionParserRuleCall_0_7_2_0());
                              						
                            }
                            pushFollow(FOLLOW_26);
                            lv_condition_9_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getTermRule());
                              							}
                              							set(
                              								current,
                              								"condition",
                              								lv_condition_9_0,
                              								"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }

                    // InternalSmartContractSpecification.g:832:4: ( (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+ )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==23) ) {
                        int LA23_1 = input.LA(2);

                        if ( (LA23_1==29) ) {
                            alt23=1;
                        }
                    }
                    else if ( (LA23_0==29) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // InternalSmartContractSpecification.g:833:5: (otherlv_10= ',' )? otherlv_11= 'while' ( (lv_transferOperations_12_0= ruleTransferOperation ) )+
                            {
                            // InternalSmartContractSpecification.g:833:5: (otherlv_10= ',' )?
                            int alt21=2;
                            int LA21_0 = input.LA(1);

                            if ( (LA21_0==23) ) {
                                alt21=1;
                            }
                            switch (alt21) {
                                case 1 :
                                    // InternalSmartContractSpecification.g:834:6: otherlv_10= ','
                                    {
                                    otherlv_10=(Token)match(input,23,FOLLOW_27); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_10, grammarAccess.getTermAccess().getCommaKeyword_0_8_0());
                                      					
                                    }

                                    }
                                    break;

                            }

                            otherlv_11=(Token)match(input,29,FOLLOW_28); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_11, grammarAccess.getTermAccess().getWhileKeyword_0_8_1());
                              				
                            }
                            // InternalSmartContractSpecification.g:843:5: ( (lv_transferOperations_12_0= ruleTransferOperation ) )+
                            int cnt22=0;
                            loop22:
                            do {
                                int alt22=2;
                                int LA22_0 = input.LA(1);

                                if ( (LA22_0==32||(LA22_0>=34 && LA22_0<=35)) ) {
                                    alt22=1;
                                }


                                switch (alt22) {
                            	case 1 :
                            	    // InternalSmartContractSpecification.g:844:6: (lv_transferOperations_12_0= ruleTransferOperation )
                            	    {
                            	    // InternalSmartContractSpecification.g:844:6: (lv_transferOperations_12_0= ruleTransferOperation )
                            	    // InternalSmartContractSpecification.g:845:7: lv_transferOperations_12_0= ruleTransferOperation
                            	    {
                            	    if ( state.backtracking==0 ) {

                            	      							newCompositeNode(grammarAccess.getTermAccess().getTransferOperationsTransferOperationParserRuleCall_0_8_2_0());
                            	      						
                            	    }
                            	    pushFollow(FOLLOW_29);
                            	    lv_transferOperations_12_0=ruleTransferOperation();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      							if (current==null) {
                            	      								current = createModelElementForParent(grammarAccess.getTermRule());
                            	      							}
                            	      							add(
                            	      								current,
                            	      								"transferOperations",
                            	      								lv_transferOperations_12_0,
                            	      								"edu.ustb.sei.mde.SmartContractSpecification.TransferOperation");
                            	      							afterParserOrEnumRuleCall();
                            	      						
                            	    }

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt22 >= 1 ) break loop22;
                            	    if (state.backtracking>0) {state.failed=true; return current;}
                                        EarlyExitException eee =
                                            new EarlyExitException(22, input);
                                        throw eee;
                                }
                                cnt22++;
                            } while (true);


                            }
                            break;

                    }

                    // InternalSmartContractSpecification.g:863:4: ( (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) ) )?
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0==23||LA25_0==30) ) {
                        alt25=1;
                    }
                    switch (alt25) {
                        case 1 :
                            // InternalSmartContractSpecification.g:864:5: (otherlv_13= ',' )? otherlv_14= 'where' ( (lv_postCondition_15_0= ruleExpression ) )
                            {
                            // InternalSmartContractSpecification.g:864:5: (otherlv_13= ',' )?
                            int alt24=2;
                            int LA24_0 = input.LA(1);

                            if ( (LA24_0==23) ) {
                                alt24=1;
                            }
                            switch (alt24) {
                                case 1 :
                                    // InternalSmartContractSpecification.g:865:6: otherlv_13= ','
                                    {
                                    otherlv_13=(Token)match(input,23,FOLLOW_30); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_13, grammarAccess.getTermAccess().getCommaKeyword_0_9_0());
                                      					
                                    }

                                    }
                                    break;

                            }

                            otherlv_14=(Token)match(input,30,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_14, grammarAccess.getTermAccess().getWhereKeyword_0_9_1());
                              				
                            }
                            // InternalSmartContractSpecification.g:874:5: ( (lv_postCondition_15_0= ruleExpression ) )
                            // InternalSmartContractSpecification.g:875:6: (lv_postCondition_15_0= ruleExpression )
                            {
                            // InternalSmartContractSpecification.g:875:6: (lv_postCondition_15_0= ruleExpression )
                            // InternalSmartContractSpecification.g:876:7: lv_postCondition_15_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getTermAccess().getPostConditionExpressionParserRuleCall_0_9_2_0());
                              						
                            }
                            pushFollow(FOLLOW_2);
                            lv_postCondition_15_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getTermRule());
                              							}
                              							set(
                              								current,
                              								"postCondition",
                              								lv_postCondition_15_0,
                              								"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:896:3: ( () otherlv_17= 'term' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= ':' ( (otherlv_20= RULE_ID ) ) otherlv_21= 'must' ( (otherlv_22= RULE_ID ) ) ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )? ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )? ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )? )
                    {
                    // InternalSmartContractSpecification.g:896:3: ( () otherlv_17= 'term' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= ':' ( (otherlv_20= RULE_ID ) ) otherlv_21= 'must' ( (otherlv_22= RULE_ID ) ) ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )? ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )? ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )? )
                    // InternalSmartContractSpecification.g:897:4: () otherlv_17= 'term' ( (lv_name_18_0= RULE_ID ) ) otherlv_19= ':' ( (otherlv_20= RULE_ID ) ) otherlv_21= 'must' ( (otherlv_22= RULE_ID ) ) ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )? ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )? ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )?
                    {
                    // InternalSmartContractSpecification.g:897:4: ()
                    // InternalSmartContractSpecification.g:898:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getTermAccess().getDutyTermAction_1_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_17=(Token)match(input,26,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_17, grammarAccess.getTermAccess().getTermKeyword_1_1());
                      			
                    }
                    // InternalSmartContractSpecification.g:908:4: ( (lv_name_18_0= RULE_ID ) )
                    // InternalSmartContractSpecification.g:909:5: (lv_name_18_0= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:909:5: (lv_name_18_0= RULE_ID )
                    // InternalSmartContractSpecification.g:910:6: lv_name_18_0= RULE_ID
                    {
                    lv_name_18_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_18_0, grammarAccess.getTermAccess().getNameIDTerminalRuleCall_1_2_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTermRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_18_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    otherlv_19=(Token)match(input,18,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_19, grammarAccess.getTermAccess().getColonKeyword_1_3());
                      			
                    }
                    // InternalSmartContractSpecification.g:930:4: ( (otherlv_20= RULE_ID ) )
                    // InternalSmartContractSpecification.g:931:5: (otherlv_20= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:931:5: (otherlv_20= RULE_ID )
                    // InternalSmartContractSpecification.g:932:6: otherlv_20= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTermRule());
                      						}
                      					
                    }
                    otherlv_20=(Token)match(input,RULE_ID,FOLLOW_31); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_20, grammarAccess.getTermAccess().getPartyInternalPartyCrossReference_1_4_0());
                      					
                    }

                    }


                    }

                    otherlv_21=(Token)match(input,31,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_21, grammarAccess.getTermAccess().getMustKeyword_1_5());
                      			
                    }
                    // InternalSmartContractSpecification.g:947:4: ( (otherlv_22= RULE_ID ) )
                    // InternalSmartContractSpecification.g:948:5: (otherlv_22= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:948:5: (otherlv_22= RULE_ID )
                    // InternalSmartContractSpecification.g:949:6: otherlv_22= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTermRule());
                      						}
                      					
                    }
                    otherlv_22=(Token)match(input,RULE_ID,FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_22, grammarAccess.getTermAccess().getActionActionCrossReference_1_6_0());
                      					
                    }

                    }


                    }

                    // InternalSmartContractSpecification.g:960:4: ( (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) ) )?
                    int alt27=2;
                    int LA27_0 = input.LA(1);

                    if ( (LA27_0==23) ) {
                        int LA27_1 = input.LA(2);

                        if ( (LA27_1==28) ) {
                            alt27=1;
                        }
                    }
                    else if ( (LA27_0==28) ) {
                        alt27=1;
                    }
                    switch (alt27) {
                        case 1 :
                            // InternalSmartContractSpecification.g:961:5: (otherlv_23= ',' )? otherlv_24= 'when' ( (lv_condition_25_0= ruleExpression ) )
                            {
                            // InternalSmartContractSpecification.g:961:5: (otherlv_23= ',' )?
                            int alt26=2;
                            int LA26_0 = input.LA(1);

                            if ( (LA26_0==23) ) {
                                alt26=1;
                            }
                            switch (alt26) {
                                case 1 :
                                    // InternalSmartContractSpecification.g:962:6: otherlv_23= ','
                                    {
                                    otherlv_23=(Token)match(input,23,FOLLOW_24); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_23, grammarAccess.getTermAccess().getCommaKeyword_1_7_0());
                                      					
                                    }

                                    }
                                    break;

                            }

                            otherlv_24=(Token)match(input,28,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_24, grammarAccess.getTermAccess().getWhenKeyword_1_7_1());
                              				
                            }
                            // InternalSmartContractSpecification.g:971:5: ( (lv_condition_25_0= ruleExpression ) )
                            // InternalSmartContractSpecification.g:972:6: (lv_condition_25_0= ruleExpression )
                            {
                            // InternalSmartContractSpecification.g:972:6: (lv_condition_25_0= ruleExpression )
                            // InternalSmartContractSpecification.g:973:7: lv_condition_25_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getTermAccess().getConditionExpressionParserRuleCall_1_7_2_0());
                              						
                            }
                            pushFollow(FOLLOW_26);
                            lv_condition_25_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getTermRule());
                              							}
                              							set(
                              								current,
                              								"condition",
                              								lv_condition_25_0,
                              								"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }

                    // InternalSmartContractSpecification.g:991:4: ( (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+ )?
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0==23) ) {
                        int LA30_1 = input.LA(2);

                        if ( (LA30_1==29) ) {
                            alt30=1;
                        }
                    }
                    else if ( (LA30_0==29) ) {
                        alt30=1;
                    }
                    switch (alt30) {
                        case 1 :
                            // InternalSmartContractSpecification.g:992:5: (otherlv_26= ',' )? otherlv_27= 'while' ( (lv_transferOperations_28_0= ruleTransferOperation ) )+
                            {
                            // InternalSmartContractSpecification.g:992:5: (otherlv_26= ',' )?
                            int alt28=2;
                            int LA28_0 = input.LA(1);

                            if ( (LA28_0==23) ) {
                                alt28=1;
                            }
                            switch (alt28) {
                                case 1 :
                                    // InternalSmartContractSpecification.g:993:6: otherlv_26= ','
                                    {
                                    otherlv_26=(Token)match(input,23,FOLLOW_27); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_26, grammarAccess.getTermAccess().getCommaKeyword_1_8_0());
                                      					
                                    }

                                    }
                                    break;

                            }

                            otherlv_27=(Token)match(input,29,FOLLOW_28); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_27, grammarAccess.getTermAccess().getWhileKeyword_1_8_1());
                              				
                            }
                            // InternalSmartContractSpecification.g:1002:5: ( (lv_transferOperations_28_0= ruleTransferOperation ) )+
                            int cnt29=0;
                            loop29:
                            do {
                                int alt29=2;
                                int LA29_0 = input.LA(1);

                                if ( (LA29_0==32||(LA29_0>=34 && LA29_0<=35)) ) {
                                    alt29=1;
                                }


                                switch (alt29) {
                            	case 1 :
                            	    // InternalSmartContractSpecification.g:1003:6: (lv_transferOperations_28_0= ruleTransferOperation )
                            	    {
                            	    // InternalSmartContractSpecification.g:1003:6: (lv_transferOperations_28_0= ruleTransferOperation )
                            	    // InternalSmartContractSpecification.g:1004:7: lv_transferOperations_28_0= ruleTransferOperation
                            	    {
                            	    if ( state.backtracking==0 ) {

                            	      							newCompositeNode(grammarAccess.getTermAccess().getTransferOperationsTransferOperationParserRuleCall_1_8_2_0());
                            	      						
                            	    }
                            	    pushFollow(FOLLOW_29);
                            	    lv_transferOperations_28_0=ruleTransferOperation();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      							if (current==null) {
                            	      								current = createModelElementForParent(grammarAccess.getTermRule());
                            	      							}
                            	      							add(
                            	      								current,
                            	      								"transferOperations",
                            	      								lv_transferOperations_28_0,
                            	      								"edu.ustb.sei.mde.SmartContractSpecification.TransferOperation");
                            	      							afterParserOrEnumRuleCall();
                            	      						
                            	    }

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt29 >= 1 ) break loop29;
                            	    if (state.backtracking>0) {state.failed=true; return current;}
                                        EarlyExitException eee =
                                            new EarlyExitException(29, input);
                                        throw eee;
                                }
                                cnt29++;
                            } while (true);


                            }
                            break;

                    }

                    // InternalSmartContractSpecification.g:1022:4: ( (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) ) )?
                    int alt32=2;
                    int LA32_0 = input.LA(1);

                    if ( (LA32_0==23||LA32_0==30) ) {
                        alt32=1;
                    }
                    switch (alt32) {
                        case 1 :
                            // InternalSmartContractSpecification.g:1023:5: (otherlv_29= ',' )? otherlv_30= 'where' ( (lv_postCondition_31_0= ruleExpression ) )
                            {
                            // InternalSmartContractSpecification.g:1023:5: (otherlv_29= ',' )?
                            int alt31=2;
                            int LA31_0 = input.LA(1);

                            if ( (LA31_0==23) ) {
                                alt31=1;
                            }
                            switch (alt31) {
                                case 1 :
                                    // InternalSmartContractSpecification.g:1024:6: otherlv_29= ','
                                    {
                                    otherlv_29=(Token)match(input,23,FOLLOW_30); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_29, grammarAccess.getTermAccess().getCommaKeyword_1_9_0());
                                      					
                                    }

                                    }
                                    break;

                            }

                            otherlv_30=(Token)match(input,30,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_30, grammarAccess.getTermAccess().getWhereKeyword_1_9_1());
                              				
                            }
                            // InternalSmartContractSpecification.g:1033:5: ( (lv_postCondition_31_0= ruleExpression ) )
                            // InternalSmartContractSpecification.g:1034:6: (lv_postCondition_31_0= ruleExpression )
                            {
                            // InternalSmartContractSpecification.g:1034:6: (lv_postCondition_31_0= ruleExpression )
                            // InternalSmartContractSpecification.g:1035:7: lv_postCondition_31_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getTermAccess().getPostConditionExpressionParserRuleCall_1_9_2_0());
                              						
                            }
                            pushFollow(FOLLOW_2);
                            lv_postCondition_31_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getTermRule());
                              							}
                              							set(
                              								current,
                              								"postCondition",
                              								lv_postCondition_31_0,
                              								"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerm"


    // $ANTLR start "entryRuleTransferOperation"
    // InternalSmartContractSpecification.g:1058:1: entryRuleTransferOperation returns [EObject current=null] : iv_ruleTransferOperation= ruleTransferOperation EOF ;
    public final EObject entryRuleTransferOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransferOperation = null;


        try {
            // InternalSmartContractSpecification.g:1058:58: (iv_ruleTransferOperation= ruleTransferOperation EOF )
            // InternalSmartContractSpecification.g:1059:2: iv_ruleTransferOperation= ruleTransferOperation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTransferOperationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTransferOperation=ruleTransferOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTransferOperation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransferOperation"


    // $ANTLR start "ruleTransferOperation"
    // InternalSmartContractSpecification.g:1065:1: ruleTransferOperation returns [EObject current=null] : ( ( () otherlv_1= 'deposit' otherlv_2= '$' ( (lv_money_3_0= ruleExpression ) ) ) | ( () otherlv_5= 'withdraw' otherlv_6= '$' ( (lv_money_7_0= ruleExpression ) ) ) | ( () otherlv_9= 'transfer' ( (lv_targets_10_0= ruleTarget ) )+ ) ) ;
    public final EObject ruleTransferOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        EObject lv_money_3_0 = null;

        EObject lv_money_7_0 = null;

        EObject lv_targets_10_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1071:2: ( ( ( () otherlv_1= 'deposit' otherlv_2= '$' ( (lv_money_3_0= ruleExpression ) ) ) | ( () otherlv_5= 'withdraw' otherlv_6= '$' ( (lv_money_7_0= ruleExpression ) ) ) | ( () otherlv_9= 'transfer' ( (lv_targets_10_0= ruleTarget ) )+ ) ) )
            // InternalSmartContractSpecification.g:1072:2: ( ( () otherlv_1= 'deposit' otherlv_2= '$' ( (lv_money_3_0= ruleExpression ) ) ) | ( () otherlv_5= 'withdraw' otherlv_6= '$' ( (lv_money_7_0= ruleExpression ) ) ) | ( () otherlv_9= 'transfer' ( (lv_targets_10_0= ruleTarget ) )+ ) )
            {
            // InternalSmartContractSpecification.g:1072:2: ( ( () otherlv_1= 'deposit' otherlv_2= '$' ( (lv_money_3_0= ruleExpression ) ) ) | ( () otherlv_5= 'withdraw' otherlv_6= '$' ( (lv_money_7_0= ruleExpression ) ) ) | ( () otherlv_9= 'transfer' ( (lv_targets_10_0= ruleTarget ) )+ ) )
            int alt35=3;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt35=1;
                }
                break;
            case 34:
                {
                alt35=2;
                }
                break;
            case 35:
                {
                alt35=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }

            switch (alt35) {
                case 1 :
                    // InternalSmartContractSpecification.g:1073:3: ( () otherlv_1= 'deposit' otherlv_2= '$' ( (lv_money_3_0= ruleExpression ) ) )
                    {
                    // InternalSmartContractSpecification.g:1073:3: ( () otherlv_1= 'deposit' otherlv_2= '$' ( (lv_money_3_0= ruleExpression ) ) )
                    // InternalSmartContractSpecification.g:1074:4: () otherlv_1= 'deposit' otherlv_2= '$' ( (lv_money_3_0= ruleExpression ) )
                    {
                    // InternalSmartContractSpecification.g:1074:4: ()
                    // InternalSmartContractSpecification.g:1075:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getTransferOperationAccess().getDepositAction_0_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_1=(Token)match(input,32,FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getTransferOperationAccess().getDepositKeyword_0_1());
                      			
                    }
                    otherlv_2=(Token)match(input,33,FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getTransferOperationAccess().getDollarSignKeyword_0_2());
                      			
                    }
                    // InternalSmartContractSpecification.g:1089:4: ( (lv_money_3_0= ruleExpression ) )
                    // InternalSmartContractSpecification.g:1090:5: (lv_money_3_0= ruleExpression )
                    {
                    // InternalSmartContractSpecification.g:1090:5: (lv_money_3_0= ruleExpression )
                    // InternalSmartContractSpecification.g:1091:6: lv_money_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTransferOperationAccess().getMoneyExpressionParserRuleCall_0_3_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_money_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTransferOperationRule());
                      						}
                      						set(
                      							current,
                      							"money",
                      							lv_money_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:1110:3: ( () otherlv_5= 'withdraw' otherlv_6= '$' ( (lv_money_7_0= ruleExpression ) ) )
                    {
                    // InternalSmartContractSpecification.g:1110:3: ( () otherlv_5= 'withdraw' otherlv_6= '$' ( (lv_money_7_0= ruleExpression ) ) )
                    // InternalSmartContractSpecification.g:1111:4: () otherlv_5= 'withdraw' otherlv_6= '$' ( (lv_money_7_0= ruleExpression ) )
                    {
                    // InternalSmartContractSpecification.g:1111:4: ()
                    // InternalSmartContractSpecification.g:1112:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getTransferOperationAccess().getWithdrawAction_1_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_5=(Token)match(input,34,FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getTransferOperationAccess().getWithdrawKeyword_1_1());
                      			
                    }
                    otherlv_6=(Token)match(input,33,FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getTransferOperationAccess().getDollarSignKeyword_1_2());
                      			
                    }
                    // InternalSmartContractSpecification.g:1126:4: ( (lv_money_7_0= ruleExpression ) )
                    // InternalSmartContractSpecification.g:1127:5: (lv_money_7_0= ruleExpression )
                    {
                    // InternalSmartContractSpecification.g:1127:5: (lv_money_7_0= ruleExpression )
                    // InternalSmartContractSpecification.g:1128:6: lv_money_7_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTransferOperationAccess().getMoneyExpressionParserRuleCall_1_3_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_money_7_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTransferOperationRule());
                      						}
                      						set(
                      							current,
                      							"money",
                      							lv_money_7_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:1147:3: ( () otherlv_9= 'transfer' ( (lv_targets_10_0= ruleTarget ) )+ )
                    {
                    // InternalSmartContractSpecification.g:1147:3: ( () otherlv_9= 'transfer' ( (lv_targets_10_0= ruleTarget ) )+ )
                    // InternalSmartContractSpecification.g:1148:4: () otherlv_9= 'transfer' ( (lv_targets_10_0= ruleTarget ) )+
                    {
                    // InternalSmartContractSpecification.g:1148:4: ()
                    // InternalSmartContractSpecification.g:1149:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getTransferOperationAccess().getTransferAction_2_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_9=(Token)match(input,35,FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getTransferOperationAccess().getTransferKeyword_2_1());
                      			
                    }
                    // InternalSmartContractSpecification.g:1159:4: ( (lv_targets_10_0= ruleTarget ) )+
                    int cnt34=0;
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==33) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalSmartContractSpecification.g:1160:5: (lv_targets_10_0= ruleTarget )
                    	    {
                    	    // InternalSmartContractSpecification.g:1160:5: (lv_targets_10_0= ruleTarget )
                    	    // InternalSmartContractSpecification.g:1161:6: lv_targets_10_0= ruleTarget
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getTransferOperationAccess().getTargetsTargetParserRuleCall_2_2_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_33);
                    	    lv_targets_10_0=ruleTarget();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getTransferOperationRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"targets",
                    	      							lv_targets_10_0,
                    	      							"edu.ustb.sei.mde.SmartContractSpecification.Target");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt34 >= 1 ) break loop34;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(34, input);
                                throw eee;
                        }
                        cnt34++;
                    } while (true);


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransferOperation"


    // $ANTLR start "entryRuleTarget"
    // InternalSmartContractSpecification.g:1183:1: entryRuleTarget returns [EObject current=null] : iv_ruleTarget= ruleTarget EOF ;
    public final EObject entryRuleTarget() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTarget = null;


        try {
            // InternalSmartContractSpecification.g:1183:47: (iv_ruleTarget= ruleTarget EOF )
            // InternalSmartContractSpecification.g:1184:2: iv_ruleTarget= ruleTarget EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTargetRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTarget=ruleTarget();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTarget; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTarget"


    // $ANTLR start "ruleTarget"
    // InternalSmartContractSpecification.g:1190:1: ruleTarget returns [EObject current=null] : (otherlv_0= '$' ( (lv_money_1_0= ruleExpression ) ) otherlv_2= 'to' ( (lv_account_3_0= ruleSlotRef ) ) ) ;
    public final EObject ruleTarget() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_money_1_0 = null;

        EObject lv_account_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1196:2: ( (otherlv_0= '$' ( (lv_money_1_0= ruleExpression ) ) otherlv_2= 'to' ( (lv_account_3_0= ruleSlotRef ) ) ) )
            // InternalSmartContractSpecification.g:1197:2: (otherlv_0= '$' ( (lv_money_1_0= ruleExpression ) ) otherlv_2= 'to' ( (lv_account_3_0= ruleSlotRef ) ) )
            {
            // InternalSmartContractSpecification.g:1197:2: (otherlv_0= '$' ( (lv_money_1_0= ruleExpression ) ) otherlv_2= 'to' ( (lv_account_3_0= ruleSlotRef ) ) )
            // InternalSmartContractSpecification.g:1198:3: otherlv_0= '$' ( (lv_money_1_0= ruleExpression ) ) otherlv_2= 'to' ( (lv_account_3_0= ruleSlotRef ) )
            {
            otherlv_0=(Token)match(input,33,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getTargetAccess().getDollarSignKeyword_0());
              		
            }
            // InternalSmartContractSpecification.g:1202:3: ( (lv_money_1_0= ruleExpression ) )
            // InternalSmartContractSpecification.g:1203:4: (lv_money_1_0= ruleExpression )
            {
            // InternalSmartContractSpecification.g:1203:4: (lv_money_1_0= ruleExpression )
            // InternalSmartContractSpecification.g:1204:5: lv_money_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTargetAccess().getMoneyExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_34);
            lv_money_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getTargetRule());
              					}
              					set(
              						current,
              						"money",
              						lv_money_1_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,36,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getTargetAccess().getToKeyword_2());
              		
            }
            // InternalSmartContractSpecification.g:1225:3: ( (lv_account_3_0= ruleSlotRef ) )
            // InternalSmartContractSpecification.g:1226:4: (lv_account_3_0= ruleSlotRef )
            {
            // InternalSmartContractSpecification.g:1226:4: (lv_account_3_0= ruleSlotRef )
            // InternalSmartContractSpecification.g:1227:5: lv_account_3_0= ruleSlotRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTargetAccess().getAccountSlotRefParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_account_3_0=ruleSlotRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getTargetRule());
              					}
              					set(
              						current,
              						"account",
              						lv_account_3_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.SlotRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTarget"


    // $ANTLR start "entryRuleExpression"
    // InternalSmartContractSpecification.g:1248:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalSmartContractSpecification.g:1248:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalSmartContractSpecification.g:1249:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalSmartContractSpecification.g:1255:1: ruleExpression returns [EObject current=null] : (this_ConditionalExpression_0= ruleConditionalExpression | ( ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression ) ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ConditionalExpression_0 = null;

        EObject this_ImplyExpression_1 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1261:2: ( (this_ConditionalExpression_0= ruleConditionalExpression | ( ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression ) ) )
            // InternalSmartContractSpecification.g:1262:2: (this_ConditionalExpression_0= ruleConditionalExpression | ( ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression ) )
            {
            // InternalSmartContractSpecification.g:1262:2: (this_ConditionalExpression_0= ruleConditionalExpression | ( ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression ) )
            int alt36=2;
            alt36 = dfa36.predict(input);
            switch (alt36) {
                case 1 :
                    // InternalSmartContractSpecification.g:1263:3: this_ConditionalExpression_0= ruleConditionalExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpressionAccess().getConditionalExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ConditionalExpression_0=ruleConditionalExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ConditionalExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:1272:3: ( ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression )
                    {
                    // InternalSmartContractSpecification.g:1272:3: ( ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression )
                    // InternalSmartContractSpecification.g:1273:4: ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getExpressionAccess().getImplyExpressionParserRuleCall_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ImplyExpression_1=ruleImplyExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_ImplyExpression_1;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleDescriptiveExpression"
    // InternalSmartContractSpecification.g:1287:1: entryRuleDescriptiveExpression returns [EObject current=null] : iv_ruleDescriptiveExpression= ruleDescriptiveExpression EOF ;
    public final EObject entryRuleDescriptiveExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescriptiveExpression = null;


        try {
            // InternalSmartContractSpecification.g:1287:62: (iv_ruleDescriptiveExpression= ruleDescriptiveExpression EOF )
            // InternalSmartContractSpecification.g:1288:2: iv_ruleDescriptiveExpression= ruleDescriptiveExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDescriptiveExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDescriptiveExpression=ruleDescriptiveExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDescriptiveExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescriptiveExpression"


    // $ANTLR start "ruleDescriptiveExpression"
    // InternalSmartContractSpecification.g:1294:1: ruleDescriptiveExpression returns [EObject current=null] : ( (lv_plainText_0_0= RULE_PLAINTEXT ) ) ;
    public final EObject ruleDescriptiveExpression() throws RecognitionException {
        EObject current = null;

        Token lv_plainText_0_0=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1300:2: ( ( (lv_plainText_0_0= RULE_PLAINTEXT ) ) )
            // InternalSmartContractSpecification.g:1301:2: ( (lv_plainText_0_0= RULE_PLAINTEXT ) )
            {
            // InternalSmartContractSpecification.g:1301:2: ( (lv_plainText_0_0= RULE_PLAINTEXT ) )
            // InternalSmartContractSpecification.g:1302:3: (lv_plainText_0_0= RULE_PLAINTEXT )
            {
            // InternalSmartContractSpecification.g:1302:3: (lv_plainText_0_0= RULE_PLAINTEXT )
            // InternalSmartContractSpecification.g:1303:4: lv_plainText_0_0= RULE_PLAINTEXT
            {
            lv_plainText_0_0=(Token)match(input,RULE_PLAINTEXT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(lv_plainText_0_0, grammarAccess.getDescriptiveExpressionAccess().getPlainTextPLAINTEXTTerminalRuleCall_0());
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getDescriptiveExpressionRule());
              				}
              				setWithLastConsumed(
              					current,
              					"plainText",
              					lv_plainText_0_0,
              					"edu.ustb.sei.mde.SmartContractSpecification.PLAINTEXT");
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescriptiveExpression"


    // $ANTLR start "entryRuleCollectionSourceExpression"
    // InternalSmartContractSpecification.g:1322:1: entryRuleCollectionSourceExpression returns [EObject current=null] : iv_ruleCollectionSourceExpression= ruleCollectionSourceExpression EOF ;
    public final EObject entryRuleCollectionSourceExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionSourceExpression = null;


        try {
            // InternalSmartContractSpecification.g:1322:67: (iv_ruleCollectionSourceExpression= ruleCollectionSourceExpression EOF )
            // InternalSmartContractSpecification.g:1323:2: iv_ruleCollectionSourceExpression= ruleCollectionSourceExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionSourceExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCollectionSourceExpression=ruleCollectionSourceExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionSourceExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionSourceExpression"


    // $ANTLR start "ruleCollectionSourceExpression"
    // InternalSmartContractSpecification.g:1329:1: ruleCollectionSourceExpression returns [EObject current=null] : (this_SlotRef_0= ruleSlotRef ( ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) ) ( (lv_condition_3_0= ruleExpression ) ) )? ) ;
    public final EObject ruleCollectionSourceExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_SlotRef_0 = null;

        EObject lv_condition_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1335:2: ( (this_SlotRef_0= ruleSlotRef ( ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) ) ( (lv_condition_3_0= ruleExpression ) ) )? ) )
            // InternalSmartContractSpecification.g:1336:2: (this_SlotRef_0= ruleSlotRef ( ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) ) ( (lv_condition_3_0= ruleExpression ) ) )? )
            {
            // InternalSmartContractSpecification.g:1336:2: (this_SlotRef_0= ruleSlotRef ( ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) ) ( (lv_condition_3_0= ruleExpression ) ) )? )
            // InternalSmartContractSpecification.g:1337:3: this_SlotRef_0= ruleSlotRef ( ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) ) ( (lv_condition_3_0= ruleExpression ) ) )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getCollectionSourceExpressionAccess().getSlotRefParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_35);
            this_SlotRef_0=ruleSlotRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_SlotRef_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalSmartContractSpecification.g:1345:3: ( ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) ) ( (lv_condition_3_0= ruleExpression ) ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==30) && (synpred2_InternalSmartContractSpecification())) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalSmartContractSpecification.g:1346:4: ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) ) ( (lv_condition_3_0= ruleExpression ) )
                    {
                    // InternalSmartContractSpecification.g:1346:4: ( ( ( () 'where' ) )=> ( () otherlv_2= 'where' ) )
                    // InternalSmartContractSpecification.g:1347:5: ( ( () 'where' ) )=> ( () otherlv_2= 'where' )
                    {
                    // InternalSmartContractSpecification.g:1353:5: ( () otherlv_2= 'where' )
                    // InternalSmartContractSpecification.g:1354:6: () otherlv_2= 'where'
                    {
                    // InternalSmartContractSpecification.g:1354:6: ()
                    // InternalSmartContractSpecification.g:1355:7: 
                    {
                    if ( state.backtracking==0 ) {

                      							current = forceCreateModelElementAndSet(
                      								grammarAccess.getCollectionSourceExpressionAccess().getSelectExpressionSourceAction_1_0_0_0(),
                      								current);
                      						
                    }

                    }

                    otherlv_2=(Token)match(input,30,FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_2, grammarAccess.getCollectionSourceExpressionAccess().getWhereKeyword_1_0_0_1());
                      					
                    }

                    }


                    }

                    // InternalSmartContractSpecification.g:1367:4: ( (lv_condition_3_0= ruleExpression ) )
                    // InternalSmartContractSpecification.g:1368:5: (lv_condition_3_0= ruleExpression )
                    {
                    // InternalSmartContractSpecification.g:1368:5: (lv_condition_3_0= ruleExpression )
                    // InternalSmartContractSpecification.g:1369:6: lv_condition_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCollectionSourceExpressionAccess().getConditionExpressionParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_condition_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCollectionSourceExpressionRule());
                      						}
                      						set(
                      							current,
                      							"condition",
                      							lv_condition_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionSourceExpression"


    // $ANTLR start "entryRuleIteratorDeclExpression"
    // InternalSmartContractSpecification.g:1391:1: entryRuleIteratorDeclExpression returns [EObject current=null] : iv_ruleIteratorDeclExpression= ruleIteratorDeclExpression EOF ;
    public final EObject entryRuleIteratorDeclExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIteratorDeclExpression = null;


        try {
            // InternalSmartContractSpecification.g:1391:63: (iv_ruleIteratorDeclExpression= ruleIteratorDeclExpression EOF )
            // InternalSmartContractSpecification.g:1392:2: iv_ruleIteratorDeclExpression= ruleIteratorDeclExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIteratorDeclExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIteratorDeclExpression=ruleIteratorDeclExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIteratorDeclExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIteratorDeclExpression"


    // $ANTLR start "ruleIteratorDeclExpression"
    // InternalSmartContractSpecification.g:1398:1: ruleIteratorDeclExpression returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleIteratorDeclExpression() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1404:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSmartContractSpecification.g:1405:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSmartContractSpecification.g:1405:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSmartContractSpecification.g:1406:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSmartContractSpecification.g:1406:3: (lv_name_0_0= RULE_ID )
            // InternalSmartContractSpecification.g:1407:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(lv_name_0_0, grammarAccess.getIteratorDeclExpressionAccess().getNameIDTerminalRuleCall_0());
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getIteratorDeclExpressionRule());
              				}
              				setWithLastConsumed(
              					current,
              					"name",
              					lv_name_0_0,
              					"org.eclipse.xtext.common.Terminals.ID");
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIteratorDeclExpression"


    // $ANTLR start "entryRuleQuantifierExpression"
    // InternalSmartContractSpecification.g:1426:1: entryRuleQuantifierExpression returns [EObject current=null] : iv_ruleQuantifierExpression= ruleQuantifierExpression EOF ;
    public final EObject entryRuleQuantifierExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuantifierExpression = null;


        try {
            // InternalSmartContractSpecification.g:1426:61: (iv_ruleQuantifierExpression= ruleQuantifierExpression EOF )
            // InternalSmartContractSpecification.g:1427:2: iv_ruleQuantifierExpression= ruleQuantifierExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQuantifierExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQuantifierExpression=ruleQuantifierExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQuantifierExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuantifierExpression"


    // $ANTLR start "ruleQuantifierExpression"
    // InternalSmartContractSpecification.g:1433:1: ruleQuantifierExpression returns [EObject current=null] : ( ( (lv_qunatifier_0_0= ruleQuantifier ) ) ( (lv_iterator_1_0= ruleIteratorDeclExpression ) ) otherlv_2= 'in' ( (lv_scope_3_0= ruleCollectionSourceExpression ) ) otherlv_4= 'such' otherlv_5= 'that' ( (lv_condition_6_0= ruleExpression ) ) ) ;
    public final EObject ruleQuantifierExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Enumerator lv_qunatifier_0_0 = null;

        EObject lv_iterator_1_0 = null;

        EObject lv_scope_3_0 = null;

        EObject lv_condition_6_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1439:2: ( ( ( (lv_qunatifier_0_0= ruleQuantifier ) ) ( (lv_iterator_1_0= ruleIteratorDeclExpression ) ) otherlv_2= 'in' ( (lv_scope_3_0= ruleCollectionSourceExpression ) ) otherlv_4= 'such' otherlv_5= 'that' ( (lv_condition_6_0= ruleExpression ) ) ) )
            // InternalSmartContractSpecification.g:1440:2: ( ( (lv_qunatifier_0_0= ruleQuantifier ) ) ( (lv_iterator_1_0= ruleIteratorDeclExpression ) ) otherlv_2= 'in' ( (lv_scope_3_0= ruleCollectionSourceExpression ) ) otherlv_4= 'such' otherlv_5= 'that' ( (lv_condition_6_0= ruleExpression ) ) )
            {
            // InternalSmartContractSpecification.g:1440:2: ( ( (lv_qunatifier_0_0= ruleQuantifier ) ) ( (lv_iterator_1_0= ruleIteratorDeclExpression ) ) otherlv_2= 'in' ( (lv_scope_3_0= ruleCollectionSourceExpression ) ) otherlv_4= 'such' otherlv_5= 'that' ( (lv_condition_6_0= ruleExpression ) ) )
            // InternalSmartContractSpecification.g:1441:3: ( (lv_qunatifier_0_0= ruleQuantifier ) ) ( (lv_iterator_1_0= ruleIteratorDeclExpression ) ) otherlv_2= 'in' ( (lv_scope_3_0= ruleCollectionSourceExpression ) ) otherlv_4= 'such' otherlv_5= 'that' ( (lv_condition_6_0= ruleExpression ) )
            {
            // InternalSmartContractSpecification.g:1441:3: ( (lv_qunatifier_0_0= ruleQuantifier ) )
            // InternalSmartContractSpecification.g:1442:4: (lv_qunatifier_0_0= ruleQuantifier )
            {
            // InternalSmartContractSpecification.g:1442:4: (lv_qunatifier_0_0= ruleQuantifier )
            // InternalSmartContractSpecification.g:1443:5: lv_qunatifier_0_0= ruleQuantifier
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getQuantifierExpressionAccess().getQunatifierQuantifierEnumRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_3);
            lv_qunatifier_0_0=ruleQuantifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getQuantifierExpressionRule());
              					}
              					set(
              						current,
              						"qunatifier",
              						lv_qunatifier_0_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.Quantifier");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalSmartContractSpecification.g:1460:3: ( (lv_iterator_1_0= ruleIteratorDeclExpression ) )
            // InternalSmartContractSpecification.g:1461:4: (lv_iterator_1_0= ruleIteratorDeclExpression )
            {
            // InternalSmartContractSpecification.g:1461:4: (lv_iterator_1_0= ruleIteratorDeclExpression )
            // InternalSmartContractSpecification.g:1462:5: lv_iterator_1_0= ruleIteratorDeclExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getQuantifierExpressionAccess().getIteratorIteratorDeclExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_36);
            lv_iterator_1_0=ruleIteratorDeclExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getQuantifierExpressionRule());
              					}
              					set(
              						current,
              						"iterator",
              						lv_iterator_1_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.IteratorDeclExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,37,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getQuantifierExpressionAccess().getInKeyword_2());
              		
            }
            // InternalSmartContractSpecification.g:1483:3: ( (lv_scope_3_0= ruleCollectionSourceExpression ) )
            // InternalSmartContractSpecification.g:1484:4: (lv_scope_3_0= ruleCollectionSourceExpression )
            {
            // InternalSmartContractSpecification.g:1484:4: (lv_scope_3_0= ruleCollectionSourceExpression )
            // InternalSmartContractSpecification.g:1485:5: lv_scope_3_0= ruleCollectionSourceExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getQuantifierExpressionAccess().getScopeCollectionSourceExpressionParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_37);
            lv_scope_3_0=ruleCollectionSourceExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getQuantifierExpressionRule());
              					}
              					set(
              						current,
              						"scope",
              						lv_scope_3_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.CollectionSourceExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,38,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getQuantifierExpressionAccess().getSuchKeyword_4());
              		
            }
            otherlv_5=(Token)match(input,39,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getQuantifierExpressionAccess().getThatKeyword_5());
              		
            }
            // InternalSmartContractSpecification.g:1510:3: ( (lv_condition_6_0= ruleExpression ) )
            // InternalSmartContractSpecification.g:1511:4: (lv_condition_6_0= ruleExpression )
            {
            // InternalSmartContractSpecification.g:1511:4: (lv_condition_6_0= ruleExpression )
            // InternalSmartContractSpecification.g:1512:5: lv_condition_6_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getQuantifierExpressionAccess().getConditionExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_condition_6_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getQuantifierExpressionRule());
              					}
              					set(
              						current,
              						"condition",
              						lv_condition_6_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifierExpression"


    // $ANTLR start "entryRuleConditionalExpression"
    // InternalSmartContractSpecification.g:1533:1: entryRuleConditionalExpression returns [EObject current=null] : iv_ruleConditionalExpression= ruleConditionalExpression EOF ;
    public final EObject entryRuleConditionalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionalExpression = null;


        try {
            // InternalSmartContractSpecification.g:1533:62: (iv_ruleConditionalExpression= ruleConditionalExpression EOF )
            // InternalSmartContractSpecification.g:1534:2: iv_ruleConditionalExpression= ruleConditionalExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionalExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleConditionalExpression=ruleConditionalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionalExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionalExpression"


    // $ANTLR start "ruleConditionalExpression"
    // InternalSmartContractSpecification.g:1540:1: ruleConditionalExpression returns [EObject current=null] : (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenBranch_3_0= ruleExpression ) ) otherlv_4= 'else' ( (lv_elseBranch_5_0= ruleExpression ) ) ) ;
    public final EObject ruleConditionalExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_condition_1_0 = null;

        EObject lv_thenBranch_3_0 = null;

        EObject lv_elseBranch_5_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1546:2: ( (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenBranch_3_0= ruleExpression ) ) otherlv_4= 'else' ( (lv_elseBranch_5_0= ruleExpression ) ) ) )
            // InternalSmartContractSpecification.g:1547:2: (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenBranch_3_0= ruleExpression ) ) otherlv_4= 'else' ( (lv_elseBranch_5_0= ruleExpression ) ) )
            {
            // InternalSmartContractSpecification.g:1547:2: (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenBranch_3_0= ruleExpression ) ) otherlv_4= 'else' ( (lv_elseBranch_5_0= ruleExpression ) ) )
            // InternalSmartContractSpecification.g:1548:3: otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenBranch_3_0= ruleExpression ) ) otherlv_4= 'else' ( (lv_elseBranch_5_0= ruleExpression ) )
            {
            otherlv_0=(Token)match(input,40,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getConditionalExpressionAccess().getIfKeyword_0());
              		
            }
            // InternalSmartContractSpecification.g:1552:3: ( (lv_condition_1_0= ruleExpression ) )
            // InternalSmartContractSpecification.g:1553:4: (lv_condition_1_0= ruleExpression )
            {
            // InternalSmartContractSpecification.g:1553:4: (lv_condition_1_0= ruleExpression )
            // InternalSmartContractSpecification.g:1554:5: lv_condition_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConditionalExpressionAccess().getConditionExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_39);
            lv_condition_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConditionalExpressionRule());
              					}
              					set(
              						current,
              						"condition",
              						lv_condition_1_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,41,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getConditionalExpressionAccess().getThenKeyword_2());
              		
            }
            // InternalSmartContractSpecification.g:1575:3: ( (lv_thenBranch_3_0= ruleExpression ) )
            // InternalSmartContractSpecification.g:1576:4: (lv_thenBranch_3_0= ruleExpression )
            {
            // InternalSmartContractSpecification.g:1576:4: (lv_thenBranch_3_0= ruleExpression )
            // InternalSmartContractSpecification.g:1577:5: lv_thenBranch_3_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConditionalExpressionAccess().getThenBranchExpressionParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_40);
            lv_thenBranch_3_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConditionalExpressionRule());
              					}
              					set(
              						current,
              						"thenBranch",
              						lv_thenBranch_3_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,42,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getConditionalExpressionAccess().getElseKeyword_4());
              		
            }
            // InternalSmartContractSpecification.g:1598:3: ( (lv_elseBranch_5_0= ruleExpression ) )
            // InternalSmartContractSpecification.g:1599:4: (lv_elseBranch_5_0= ruleExpression )
            {
            // InternalSmartContractSpecification.g:1599:4: (lv_elseBranch_5_0= ruleExpression )
            // InternalSmartContractSpecification.g:1600:5: lv_elseBranch_5_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConditionalExpressionAccess().getElseBranchExpressionParserRuleCall_5_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_elseBranch_5_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConditionalExpressionRule());
              					}
              					set(
              						current,
              						"elseBranch",
              						lv_elseBranch_5_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionalExpression"


    // $ANTLR start "entryRuleImplyExpression"
    // InternalSmartContractSpecification.g:1621:1: entryRuleImplyExpression returns [EObject current=null] : iv_ruleImplyExpression= ruleImplyExpression EOF ;
    public final EObject entryRuleImplyExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplyExpression = null;


        try {
            // InternalSmartContractSpecification.g:1621:56: (iv_ruleImplyExpression= ruleImplyExpression EOF )
            // InternalSmartContractSpecification.g:1622:2: iv_ruleImplyExpression= ruleImplyExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImplyExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleImplyExpression=ruleImplyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImplyExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplyExpression"


    // $ANTLR start "ruleImplyExpression"
    // InternalSmartContractSpecification.g:1628:1: ruleImplyExpression returns [EObject current=null] : (this_OrExpression_0= ruleOrExpression ( ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) ) ( (lv_implied_3_0= ruleExpression ) ) )? ) ;
    public final EObject ruleImplyExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_OrExpression_0 = null;

        EObject lv_implied_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1634:2: ( (this_OrExpression_0= ruleOrExpression ( ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) ) ( (lv_implied_3_0= ruleExpression ) ) )? ) )
            // InternalSmartContractSpecification.g:1635:2: (this_OrExpression_0= ruleOrExpression ( ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) ) ( (lv_implied_3_0= ruleExpression ) ) )? )
            {
            // InternalSmartContractSpecification.g:1635:2: (this_OrExpression_0= ruleOrExpression ( ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) ) ( (lv_implied_3_0= ruleExpression ) ) )? )
            // InternalSmartContractSpecification.g:1636:3: this_OrExpression_0= ruleOrExpression ( ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) ) ( (lv_implied_3_0= ruleExpression ) ) )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getImplyExpressionAccess().getOrExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_41);
            this_OrExpression_0=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_OrExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalSmartContractSpecification.g:1644:3: ( ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) ) ( (lv_implied_3_0= ruleExpression ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==43) ) {
                int LA38_1 = input.LA(2);

                if ( (synpred3_InternalSmartContractSpecification()) ) {
                    alt38=1;
                }
            }
            switch (alt38) {
                case 1 :
                    // InternalSmartContractSpecification.g:1645:4: ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) ) ( (lv_implied_3_0= ruleExpression ) )
                    {
                    // InternalSmartContractSpecification.g:1645:4: ( ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' ) )
                    // InternalSmartContractSpecification.g:1646:5: ( ( () 'implies' ) )=> ( () otherlv_2= 'implies' )
                    {
                    // InternalSmartContractSpecification.g:1652:5: ( () otherlv_2= 'implies' )
                    // InternalSmartContractSpecification.g:1653:6: () otherlv_2= 'implies'
                    {
                    // InternalSmartContractSpecification.g:1653:6: ()
                    // InternalSmartContractSpecification.g:1654:7: 
                    {
                    if ( state.backtracking==0 ) {

                      							current = forceCreateModelElementAndSet(
                      								grammarAccess.getImplyExpressionAccess().getImplyExpressionConditionAction_1_0_0_0(),
                      								current);
                      						
                    }

                    }

                    otherlv_2=(Token)match(input,43,FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_2, grammarAccess.getImplyExpressionAccess().getImpliesKeyword_1_0_0_1());
                      					
                    }

                    }


                    }

                    // InternalSmartContractSpecification.g:1666:4: ( (lv_implied_3_0= ruleExpression ) )
                    // InternalSmartContractSpecification.g:1667:5: (lv_implied_3_0= ruleExpression )
                    {
                    // InternalSmartContractSpecification.g:1667:5: (lv_implied_3_0= ruleExpression )
                    // InternalSmartContractSpecification.g:1668:6: lv_implied_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getImplyExpressionAccess().getImpliedExpressionParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_implied_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getImplyExpressionRule());
                      						}
                      						set(
                      							current,
                      							"implied",
                      							lv_implied_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplyExpression"


    // $ANTLR start "entryRuleOrExpression"
    // InternalSmartContractSpecification.g:1690:1: entryRuleOrExpression returns [EObject current=null] : iv_ruleOrExpression= ruleOrExpression EOF ;
    public final EObject entryRuleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpression = null;


        try {
            // InternalSmartContractSpecification.g:1690:53: (iv_ruleOrExpression= ruleOrExpression EOF )
            // InternalSmartContractSpecification.g:1691:2: iv_ruleOrExpression= ruleOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOrExpression=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOrExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpression"


    // $ANTLR start "ruleOrExpression"
    // InternalSmartContractSpecification.g:1697:1: ruleOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) ) ( (lv_right_4_0= ruleOrExpression ) ) )? ) ;
    public final EObject ruleOrExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_AndExpression_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1703:2: ( (this_AndExpression_0= ruleAndExpression ( ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) ) ( (lv_right_4_0= ruleOrExpression ) ) )? ) )
            // InternalSmartContractSpecification.g:1704:2: (this_AndExpression_0= ruleAndExpression ( ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) ) ( (lv_right_4_0= ruleOrExpression ) ) )? )
            {
            // InternalSmartContractSpecification.g:1704:2: (this_AndExpression_0= ruleAndExpression ( ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) ) ( (lv_right_4_0= ruleOrExpression ) ) )? )
            // InternalSmartContractSpecification.g:1705:3: this_AndExpression_0= ruleAndExpression ( ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) ) ( (lv_right_4_0= ruleOrExpression ) ) )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getOrExpressionAccess().getAndExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_42);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_AndExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalSmartContractSpecification.g:1713:3: ( ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) ) ( (lv_right_4_0= ruleOrExpression ) ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==44) ) {
                int LA40_1 = input.LA(2);

                if ( (synpred4_InternalSmartContractSpecification()) ) {
                    alt40=1;
                }
            }
            else if ( (LA40_0==45) ) {
                int LA40_2 = input.LA(2);

                if ( (synpred4_InternalSmartContractSpecification()) ) {
                    alt40=1;
                }
            }
            switch (alt40) {
                case 1 :
                    // InternalSmartContractSpecification.g:1714:4: ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) ) ( (lv_right_4_0= ruleOrExpression ) )
                    {
                    // InternalSmartContractSpecification.g:1714:4: ( ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) ) )
                    // InternalSmartContractSpecification.g:1715:5: ( ( () ( 'or' | '||' ) ) )=> ( () (otherlv_2= 'or' | otherlv_3= '||' ) )
                    {
                    // InternalSmartContractSpecification.g:1725:5: ( () (otherlv_2= 'or' | otherlv_3= '||' ) )
                    // InternalSmartContractSpecification.g:1726:6: () (otherlv_2= 'or' | otherlv_3= '||' )
                    {
                    // InternalSmartContractSpecification.g:1726:6: ()
                    // InternalSmartContractSpecification.g:1727:7: 
                    {
                    if ( state.backtracking==0 ) {

                      							current = forceCreateModelElementAndSet(
                      								grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0_0(),
                      								current);
                      						
                    }

                    }

                    // InternalSmartContractSpecification.g:1733:6: (otherlv_2= 'or' | otherlv_3= '||' )
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==44) ) {
                        alt39=1;
                    }
                    else if ( (LA39_0==45) ) {
                        alt39=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 39, 0, input);

                        throw nvae;
                    }
                    switch (alt39) {
                        case 1 :
                            // InternalSmartContractSpecification.g:1734:7: otherlv_2= 'or'
                            {
                            otherlv_2=(Token)match(input,44,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							newLeafNode(otherlv_2, grammarAccess.getOrExpressionAccess().getOrKeyword_1_0_0_1_0());
                              						
                            }

                            }
                            break;
                        case 2 :
                            // InternalSmartContractSpecification.g:1739:7: otherlv_3= '||'
                            {
                            otherlv_3=(Token)match(input,45,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							newLeafNode(otherlv_3, grammarAccess.getOrExpressionAccess().getVerticalLineVerticalLineKeyword_1_0_0_1_1());
                              						
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSmartContractSpecification.g:1746:4: ( (lv_right_4_0= ruleOrExpression ) )
                    // InternalSmartContractSpecification.g:1747:5: (lv_right_4_0= ruleOrExpression )
                    {
                    // InternalSmartContractSpecification.g:1747:5: (lv_right_4_0= ruleOrExpression )
                    // InternalSmartContractSpecification.g:1748:6: lv_right_4_0= ruleOrExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getOrExpressionAccess().getRightOrExpressionParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_right_4_0=ruleOrExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getOrExpressionRule());
                      						}
                      						set(
                      							current,
                      							"right",
                      							lv_right_4_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.OrExpression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // InternalSmartContractSpecification.g:1770:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // InternalSmartContractSpecification.g:1770:54: (iv_ruleAndExpression= ruleAndExpression EOF )
            // InternalSmartContractSpecification.g:1771:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // InternalSmartContractSpecification.g:1777:1: ruleAndExpression returns [EObject current=null] : (this_AndExpressionChild_0= ruleAndExpressionChild ( ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) ) ( (lv_right_4_0= ruleAndExpression ) ) )? ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_AndExpressionChild_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1783:2: ( (this_AndExpressionChild_0= ruleAndExpressionChild ( ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) ) ( (lv_right_4_0= ruleAndExpression ) ) )? ) )
            // InternalSmartContractSpecification.g:1784:2: (this_AndExpressionChild_0= ruleAndExpressionChild ( ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) ) ( (lv_right_4_0= ruleAndExpression ) ) )? )
            {
            // InternalSmartContractSpecification.g:1784:2: (this_AndExpressionChild_0= ruleAndExpressionChild ( ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) ) ( (lv_right_4_0= ruleAndExpression ) ) )? )
            // InternalSmartContractSpecification.g:1785:3: this_AndExpressionChild_0= ruleAndExpressionChild ( ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) ) ( (lv_right_4_0= ruleAndExpression ) ) )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getAndExpressionAccess().getAndExpressionChildParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_43);
            this_AndExpressionChild_0=ruleAndExpressionChild();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_AndExpressionChild_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalSmartContractSpecification.g:1793:3: ( ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) ) ( (lv_right_4_0= ruleAndExpression ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==46) ) {
                int LA42_1 = input.LA(2);

                if ( (synpred5_InternalSmartContractSpecification()) ) {
                    alt42=1;
                }
            }
            else if ( (LA42_0==47) ) {
                int LA42_2 = input.LA(2);

                if ( (synpred5_InternalSmartContractSpecification()) ) {
                    alt42=1;
                }
            }
            switch (alt42) {
                case 1 :
                    // InternalSmartContractSpecification.g:1794:4: ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) ) ( (lv_right_4_0= ruleAndExpression ) )
                    {
                    // InternalSmartContractSpecification.g:1794:4: ( ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) ) )
                    // InternalSmartContractSpecification.g:1795:5: ( ( () ( 'and' | '&&' ) ) )=> ( () (otherlv_2= 'and' | otherlv_3= '&&' ) )
                    {
                    // InternalSmartContractSpecification.g:1805:5: ( () (otherlv_2= 'and' | otherlv_3= '&&' ) )
                    // InternalSmartContractSpecification.g:1806:6: () (otherlv_2= 'and' | otherlv_3= '&&' )
                    {
                    // InternalSmartContractSpecification.g:1806:6: ()
                    // InternalSmartContractSpecification.g:1807:7: 
                    {
                    if ( state.backtracking==0 ) {

                      							current = forceCreateModelElementAndSet(
                      								grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0_0(),
                      								current);
                      						
                    }

                    }

                    // InternalSmartContractSpecification.g:1813:6: (otherlv_2= 'and' | otherlv_3= '&&' )
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==46) ) {
                        alt41=1;
                    }
                    else if ( (LA41_0==47) ) {
                        alt41=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 41, 0, input);

                        throw nvae;
                    }
                    switch (alt41) {
                        case 1 :
                            // InternalSmartContractSpecification.g:1814:7: otherlv_2= 'and'
                            {
                            otherlv_2=(Token)match(input,46,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							newLeafNode(otherlv_2, grammarAccess.getAndExpressionAccess().getAndKeyword_1_0_0_1_0());
                              						
                            }

                            }
                            break;
                        case 2 :
                            // InternalSmartContractSpecification.g:1819:7: otherlv_3= '&&'
                            {
                            otherlv_3=(Token)match(input,47,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							newLeafNode(otherlv_3, grammarAccess.getAndExpressionAccess().getAmpersandAmpersandKeyword_1_0_0_1_1());
                              						
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSmartContractSpecification.g:1826:4: ( (lv_right_4_0= ruleAndExpression ) )
                    // InternalSmartContractSpecification.g:1827:5: (lv_right_4_0= ruleAndExpression )
                    {
                    // InternalSmartContractSpecification.g:1827:5: (lv_right_4_0= ruleAndExpression )
                    // InternalSmartContractSpecification.g:1828:6: lv_right_4_0= ruleAndExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAndExpressionAccess().getRightAndExpressionParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_right_4_0=ruleAndExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAndExpressionRule());
                      						}
                      						set(
                      							current,
                      							"right",
                      							lv_right_4_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.AndExpression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleAndExpressionChild"
    // InternalSmartContractSpecification.g:1850:1: entryRuleAndExpressionChild returns [EObject current=null] : iv_ruleAndExpressionChild= ruleAndExpressionChild EOF ;
    public final EObject entryRuleAndExpressionChild() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpressionChild = null;


        try {
            // InternalSmartContractSpecification.g:1850:59: (iv_ruleAndExpressionChild= ruleAndExpressionChild EOF )
            // InternalSmartContractSpecification.g:1851:2: iv_ruleAndExpressionChild= ruleAndExpressionChild EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndExpressionChildRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAndExpressionChild=ruleAndExpressionChild();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndExpressionChild; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpressionChild"


    // $ANTLR start "ruleAndExpressionChild"
    // InternalSmartContractSpecification.g:1857:1: ruleAndExpressionChild returns [EObject current=null] : (this_NotExpression_0= ruleNotExpression | this_RelationalExpression_1= ruleRelationalExpression ) ;
    public final EObject ruleAndExpressionChild() throws RecognitionException {
        EObject current = null;

        EObject this_NotExpression_0 = null;

        EObject this_RelationalExpression_1 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1863:2: ( (this_NotExpression_0= ruleNotExpression | this_RelationalExpression_1= ruleRelationalExpression ) )
            // InternalSmartContractSpecification.g:1864:2: (this_NotExpression_0= ruleNotExpression | this_RelationalExpression_1= ruleRelationalExpression )
            {
            // InternalSmartContractSpecification.g:1864:2: (this_NotExpression_0= ruleNotExpression | this_RelationalExpression_1= ruleRelationalExpression )
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==48) ) {
                alt43=1;
            }
            else if ( (LA43_0==RULE_ID||(LA43_0>=RULE_PLAINTEXT && LA43_0<=RULE_INT)||LA43_0==24||(LA43_0>=49 && LA43_0<=55)||(LA43_0>=57 && LA43_0<=60)||(LA43_0>=72 && LA43_0<=76)||(LA43_0>=92 && LA43_0<=99)) ) {
                alt43=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }
            switch (alt43) {
                case 1 :
                    // InternalSmartContractSpecification.g:1865:3: this_NotExpression_0= ruleNotExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAndExpressionChildAccess().getNotExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_NotExpression_0=ruleNotExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_NotExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:1874:3: this_RelationalExpression_1= ruleRelationalExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAndExpressionChildAccess().getRelationalExpressionParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_RelationalExpression_1=ruleRelationalExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RelationalExpression_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpressionChild"


    // $ANTLR start "entryRuleNotExpression"
    // InternalSmartContractSpecification.g:1886:1: entryRuleNotExpression returns [EObject current=null] : iv_ruleNotExpression= ruleNotExpression EOF ;
    public final EObject entryRuleNotExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotExpression = null;


        try {
            // InternalSmartContractSpecification.g:1886:54: (iv_ruleNotExpression= ruleNotExpression EOF )
            // InternalSmartContractSpecification.g:1887:2: iv_ruleNotExpression= ruleNotExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNotExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNotExpression=ruleNotExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNotExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotExpression"


    // $ANTLR start "ruleNotExpression"
    // InternalSmartContractSpecification.g:1893:1: ruleNotExpression returns [EObject current=null] : (otherlv_0= 'not' ( (lv_inner_1_0= ruleRelationalExpression ) ) ) ;
    public final EObject ruleNotExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_inner_1_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1899:2: ( (otherlv_0= 'not' ( (lv_inner_1_0= ruleRelationalExpression ) ) ) )
            // InternalSmartContractSpecification.g:1900:2: (otherlv_0= 'not' ( (lv_inner_1_0= ruleRelationalExpression ) ) )
            {
            // InternalSmartContractSpecification.g:1900:2: (otherlv_0= 'not' ( (lv_inner_1_0= ruleRelationalExpression ) ) )
            // InternalSmartContractSpecification.g:1901:3: otherlv_0= 'not' ( (lv_inner_1_0= ruleRelationalExpression ) )
            {
            otherlv_0=(Token)match(input,48,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getNotExpressionAccess().getNotKeyword_0());
              		
            }
            // InternalSmartContractSpecification.g:1905:3: ( (lv_inner_1_0= ruleRelationalExpression ) )
            // InternalSmartContractSpecification.g:1906:4: (lv_inner_1_0= ruleRelationalExpression )
            {
            // InternalSmartContractSpecification.g:1906:4: (lv_inner_1_0= ruleRelationalExpression )
            // InternalSmartContractSpecification.g:1907:5: lv_inner_1_0= ruleRelationalExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getNotExpressionAccess().getInnerRelationalExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_inner_1_0=ruleRelationalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getNotExpressionRule());
              					}
              					set(
              						current,
              						"inner",
              						lv_inner_1_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.RelationalExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotExpression"


    // $ANTLR start "entryRuleRelationalExpression"
    // InternalSmartContractSpecification.g:1928:1: entryRuleRelationalExpression returns [EObject current=null] : iv_ruleRelationalExpression= ruleRelationalExpression EOF ;
    public final EObject entryRuleRelationalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationalExpression = null;


        try {
            // InternalSmartContractSpecification.g:1928:61: (iv_ruleRelationalExpression= ruleRelationalExpression EOF )
            // InternalSmartContractSpecification.g:1929:2: iv_ruleRelationalExpression= ruleRelationalExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationalExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRelationalExpression=ruleRelationalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationalExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationalExpression"


    // $ANTLR start "ruleRelationalExpression"
    // InternalSmartContractSpecification.g:1935:1: ruleRelationalExpression returns [EObject current=null] : (this_ArithmeticExpression_0= ruleArithmeticExpression ( ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) ) )? ) ;
    public final EObject ruleRelationalExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ArithmeticExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:1941:2: ( (this_ArithmeticExpression_0= ruleArithmeticExpression ( ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) ) )? ) )
            // InternalSmartContractSpecification.g:1942:2: (this_ArithmeticExpression_0= ruleArithmeticExpression ( ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) ) )? )
            {
            // InternalSmartContractSpecification.g:1942:2: (this_ArithmeticExpression_0= ruleArithmeticExpression ( ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) ) )? )
            // InternalSmartContractSpecification.g:1943:3: this_ArithmeticExpression_0= ruleArithmeticExpression ( ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) ) )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getRelationalExpressionAccess().getArithmeticExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_44);
            this_ArithmeticExpression_0=ruleArithmeticExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_ArithmeticExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalSmartContractSpecification.g:1951:3: ( ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) ) )?
            int alt44=2;
            alt44 = dfa44.predict(input);
            switch (alt44) {
                case 1 :
                    // InternalSmartContractSpecification.g:1952:4: ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) )
                    {
                    // InternalSmartContractSpecification.g:1952:4: ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) )
                    // InternalSmartContractSpecification.g:1953:5: ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) )
                    {
                    // InternalSmartContractSpecification.g:1963:5: ( () ( (lv_operator_2_0= ruleRelationOperator ) ) )
                    // InternalSmartContractSpecification.g:1964:6: () ( (lv_operator_2_0= ruleRelationOperator ) )
                    {
                    // InternalSmartContractSpecification.g:1964:6: ()
                    // InternalSmartContractSpecification.g:1965:7: 
                    {
                    if ( state.backtracking==0 ) {

                      							current = forceCreateModelElementAndSet(
                      								grammarAccess.getRelationalExpressionAccess().getRelationalExpressionLeftAction_1_0_0_0(),
                      								current);
                      						
                    }

                    }

                    // InternalSmartContractSpecification.g:1971:6: ( (lv_operator_2_0= ruleRelationOperator ) )
                    // InternalSmartContractSpecification.g:1972:7: (lv_operator_2_0= ruleRelationOperator )
                    {
                    // InternalSmartContractSpecification.g:1972:7: (lv_operator_2_0= ruleRelationOperator )
                    // InternalSmartContractSpecification.g:1973:8: lv_operator_2_0= ruleRelationOperator
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getRelationalExpressionAccess().getOperatorRelationOperatorEnumRuleCall_1_0_0_1_0());
                      							
                    }
                    pushFollow(FOLLOW_25);
                    lv_operator_2_0=ruleRelationOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getRelationalExpressionRule());
                      								}
                      								set(
                      									current,
                      									"operator",
                      									lv_operator_2_0,
                      									"edu.ustb.sei.mde.SmartContractSpecification.RelationOperator");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }


                    }


                    }

                    // InternalSmartContractSpecification.g:1992:4: ( (lv_right_3_0= ruleArithmeticExpression ) )
                    // InternalSmartContractSpecification.g:1993:5: (lv_right_3_0= ruleArithmeticExpression )
                    {
                    // InternalSmartContractSpecification.g:1993:5: (lv_right_3_0= ruleArithmeticExpression )
                    // InternalSmartContractSpecification.g:1994:6: lv_right_3_0= ruleArithmeticExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getRelationalExpressionAccess().getRightArithmeticExpressionParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_right_3_0=ruleArithmeticExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getRelationalExpressionRule());
                      						}
                      						set(
                      							current,
                      							"right",
                      							lv_right_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.ArithmeticExpression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationalExpression"


    // $ANTLR start "entryRuleArithmeticExpression"
    // InternalSmartContractSpecification.g:2016:1: entryRuleArithmeticExpression returns [EObject current=null] : iv_ruleArithmeticExpression= ruleArithmeticExpression EOF ;
    public final EObject entryRuleArithmeticExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArithmeticExpression = null;


        try {
            // InternalSmartContractSpecification.g:2016:61: (iv_ruleArithmeticExpression= ruleArithmeticExpression EOF )
            // InternalSmartContractSpecification.g:2017:2: iv_ruleArithmeticExpression= ruleArithmeticExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArithmeticExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleArithmeticExpression=ruleArithmeticExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArithmeticExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArithmeticExpression"


    // $ANTLR start "ruleArithmeticExpression"
    // InternalSmartContractSpecification.g:2023:1: ruleArithmeticExpression returns [EObject current=null] : this_AdditiveExpression_0= ruleAdditiveExpression ;
    public final EObject ruleArithmeticExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AdditiveExpression_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2029:2: (this_AdditiveExpression_0= ruleAdditiveExpression )
            // InternalSmartContractSpecification.g:2030:2: this_AdditiveExpression_0= ruleAdditiveExpression
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getArithmeticExpressionAccess().getAdditiveExpressionParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_AdditiveExpression_0=ruleAdditiveExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_AdditiveExpression_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArithmeticExpression"


    // $ANTLR start "entryRuleAdditiveExpression"
    // InternalSmartContractSpecification.g:2041:1: entryRuleAdditiveExpression returns [EObject current=null] : iv_ruleAdditiveExpression= ruleAdditiveExpression EOF ;
    public final EObject entryRuleAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditiveExpression = null;


        try {
            // InternalSmartContractSpecification.g:2041:59: (iv_ruleAdditiveExpression= ruleAdditiveExpression EOF )
            // InternalSmartContractSpecification.g:2042:2: iv_ruleAdditiveExpression= ruleAdditiveExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditiveExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAdditiveExpression=ruleAdditiveExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditiveExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditiveExpression"


    // $ANTLR start "ruleAdditiveExpression"
    // InternalSmartContractSpecification.g:2048:1: ruleAdditiveExpression returns [EObject current=null] : (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? ) ;
    public final EObject ruleAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject this_MultiplicativeExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2054:2: ( (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? ) )
            // InternalSmartContractSpecification.g:2055:2: (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? )
            {
            // InternalSmartContractSpecification.g:2055:2: (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )? )
            // InternalSmartContractSpecification.g:2056:3: this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getMultiplicativeExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_45);
            this_MultiplicativeExpression_0=ruleMultiplicativeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_MultiplicativeExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalSmartContractSpecification.g:2064:3: ( ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==88) ) {
                int LA45_1 = input.LA(2);

                if ( (synpred7_InternalSmartContractSpecification()) ) {
                    alt45=1;
                }
            }
            else if ( (LA45_0==57) ) {
                int LA45_2 = input.LA(2);

                if ( (synpred7_InternalSmartContractSpecification()) ) {
                    alt45=1;
                }
            }
            switch (alt45) {
                case 1 :
                    // InternalSmartContractSpecification.g:2065:4: ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) ) ( (lv_right_3_0= ruleMultiplicativeExpression ) )
                    {
                    // InternalSmartContractSpecification.g:2065:4: ( ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ) )
                    // InternalSmartContractSpecification.g:2066:5: ( ( () ( ( ruleAdditiveOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) )
                    {
                    // InternalSmartContractSpecification.g:2076:5: ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) )
                    // InternalSmartContractSpecification.g:2077:6: () ( (lv_operator_2_0= ruleAdditiveOperator ) )
                    {
                    // InternalSmartContractSpecification.g:2077:6: ()
                    // InternalSmartContractSpecification.g:2078:7: 
                    {
                    if ( state.backtracking==0 ) {

                      							current = forceCreateModelElementAndSet(
                      								grammarAccess.getAdditiveExpressionAccess().getAdditiveExpressionLeftAction_1_0_0_0(),
                      								current);
                      						
                    }

                    }

                    // InternalSmartContractSpecification.g:2084:6: ( (lv_operator_2_0= ruleAdditiveOperator ) )
                    // InternalSmartContractSpecification.g:2085:7: (lv_operator_2_0= ruleAdditiveOperator )
                    {
                    // InternalSmartContractSpecification.g:2085:7: (lv_operator_2_0= ruleAdditiveOperator )
                    // InternalSmartContractSpecification.g:2086:8: lv_operator_2_0= ruleAdditiveOperator
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getOperatorAdditiveOperatorEnumRuleCall_1_0_0_1_0());
                      							
                    }
                    pushFollow(FOLLOW_25);
                    lv_operator_2_0=ruleAdditiveOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getAdditiveExpressionRule());
                      								}
                      								set(
                      									current,
                      									"operator",
                      									lv_operator_2_0,
                      									"edu.ustb.sei.mde.SmartContractSpecification.AdditiveOperator");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }


                    }


                    }

                    // InternalSmartContractSpecification.g:2105:4: ( (lv_right_3_0= ruleMultiplicativeExpression ) )
                    // InternalSmartContractSpecification.g:2106:5: (lv_right_3_0= ruleMultiplicativeExpression )
                    {
                    // InternalSmartContractSpecification.g:2106:5: (lv_right_3_0= ruleMultiplicativeExpression )
                    // InternalSmartContractSpecification.g:2107:6: lv_right_3_0= ruleMultiplicativeExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getRightMultiplicativeExpressionParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_right_3_0=ruleMultiplicativeExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAdditiveExpressionRule());
                      						}
                      						set(
                      							current,
                      							"right",
                      							lv_right_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.MultiplicativeExpression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditiveExpression"


    // $ANTLR start "entryRuleMultiplicativeExpression"
    // InternalSmartContractSpecification.g:2129:1: entryRuleMultiplicativeExpression returns [EObject current=null] : iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF ;
    public final EObject entryRuleMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicativeExpression = null;


        try {
            // InternalSmartContractSpecification.g:2129:65: (iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF )
            // InternalSmartContractSpecification.g:2130:2: iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicativeExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMultiplicativeExpression=ruleMultiplicativeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicativeExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicativeExpression"


    // $ANTLR start "ruleMultiplicativeExpression"
    // InternalSmartContractSpecification.g:2136:1: ruleMultiplicativeExpression returns [EObject current=null] : (this_AtomExpression_0= ruleAtomExpression ( ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) ) ( (lv_right_3_0= ruleAtomExpression ) ) )? ) ;
    public final EObject ruleMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AtomExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2142:2: ( (this_AtomExpression_0= ruleAtomExpression ( ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) ) ( (lv_right_3_0= ruleAtomExpression ) ) )? ) )
            // InternalSmartContractSpecification.g:2143:2: (this_AtomExpression_0= ruleAtomExpression ( ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) ) ( (lv_right_3_0= ruleAtomExpression ) ) )? )
            {
            // InternalSmartContractSpecification.g:2143:2: (this_AtomExpression_0= ruleAtomExpression ( ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) ) ( (lv_right_3_0= ruleAtomExpression ) ) )? )
            // InternalSmartContractSpecification.g:2144:3: this_AtomExpression_0= ruleAtomExpression ( ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) ) ( (lv_right_3_0= ruleAtomExpression ) ) )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getAtomExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_46);
            this_AtomExpression_0=ruleAtomExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_AtomExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalSmartContractSpecification.g:2152:3: ( ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) ) ( (lv_right_3_0= ruleAtomExpression ) ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==89) ) {
                int LA46_1 = input.LA(2);

                if ( (synpred8_InternalSmartContractSpecification()) ) {
                    alt46=1;
                }
            }
            else if ( (LA46_0==90) ) {
                int LA46_2 = input.LA(2);

                if ( (synpred8_InternalSmartContractSpecification()) ) {
                    alt46=1;
                }
            }
            switch (alt46) {
                case 1 :
                    // InternalSmartContractSpecification.g:2153:4: ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) ) ( (lv_right_3_0= ruleAtomExpression ) )
                    {
                    // InternalSmartContractSpecification.g:2153:4: ( ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ) )
                    // InternalSmartContractSpecification.g:2154:5: ( ( () ( ( ruleMultiplicativeOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) )
                    {
                    // InternalSmartContractSpecification.g:2164:5: ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) )
                    // InternalSmartContractSpecification.g:2165:6: () ( (lv_operator_2_0= ruleMultiplicativeOperator ) )
                    {
                    // InternalSmartContractSpecification.g:2165:6: ()
                    // InternalSmartContractSpecification.g:2166:7: 
                    {
                    if ( state.backtracking==0 ) {

                      							current = forceCreateModelElementAndSet(
                      								grammarAccess.getMultiplicativeExpressionAccess().getMultiplicativeExpressionLeftAction_1_0_0_0(),
                      								current);
                      						
                    }

                    }

                    // InternalSmartContractSpecification.g:2172:6: ( (lv_operator_2_0= ruleMultiplicativeOperator ) )
                    // InternalSmartContractSpecification.g:2173:7: (lv_operator_2_0= ruleMultiplicativeOperator )
                    {
                    // InternalSmartContractSpecification.g:2173:7: (lv_operator_2_0= ruleMultiplicativeOperator )
                    // InternalSmartContractSpecification.g:2174:8: lv_operator_2_0= ruleMultiplicativeOperator
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getOperatorMultiplicativeOperatorEnumRuleCall_1_0_0_1_0());
                      							
                    }
                    pushFollow(FOLLOW_25);
                    lv_operator_2_0=ruleMultiplicativeOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getMultiplicativeExpressionRule());
                      								}
                      								set(
                      									current,
                      									"operator",
                      									lv_operator_2_0,
                      									"edu.ustb.sei.mde.SmartContractSpecification.MultiplicativeOperator");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }


                    }


                    }

                    // InternalSmartContractSpecification.g:2193:4: ( (lv_right_3_0= ruleAtomExpression ) )
                    // InternalSmartContractSpecification.g:2194:5: (lv_right_3_0= ruleAtomExpression )
                    {
                    // InternalSmartContractSpecification.g:2194:5: (lv_right_3_0= ruleAtomExpression )
                    // InternalSmartContractSpecification.g:2195:6: lv_right_3_0= ruleAtomExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getRightAtomExpressionParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_right_3_0=ruleAtomExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getMultiplicativeExpressionRule());
                      						}
                      						set(
                      							current,
                      							"right",
                      							lv_right_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.AtomExpression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicativeExpression"


    // $ANTLR start "entryRuleAtomExpression"
    // InternalSmartContractSpecification.g:2217:1: entryRuleAtomExpression returns [EObject current=null] : iv_ruleAtomExpression= ruleAtomExpression EOF ;
    public final EObject entryRuleAtomExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomExpression = null;


        try {
            // InternalSmartContractSpecification.g:2217:55: (iv_ruleAtomExpression= ruleAtomExpression EOF )
            // InternalSmartContractSpecification.g:2218:2: iv_ruleAtomExpression= ruleAtomExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAtomExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAtomExpression=ruleAtomExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAtomExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomExpression"


    // $ANTLR start "ruleAtomExpression"
    // InternalSmartContractSpecification.g:2224:1: ruleAtomExpression returns [EObject current=null] : ( ( ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression ) | this_TimePredicate_1= ruleTimePredicate | this_SlotRef_2= ruleSlotRef | this_ThisExpression_3= ruleThisExpression | this_QuantifierExpression_4= ruleQuantifierExpression | this_DescriptiveExpression_5= ruleDescriptiveExpression | (otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')' ) ) ;
    public final EObject ruleAtomExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject this_ConstantExpression_0 = null;

        EObject this_TimePredicate_1 = null;

        EObject this_SlotRef_2 = null;

        EObject this_ThisExpression_3 = null;

        EObject this_QuantifierExpression_4 = null;

        EObject this_DescriptiveExpression_5 = null;

        EObject this_Expression_7 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2230:2: ( ( ( ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression ) | this_TimePredicate_1= ruleTimePredicate | this_SlotRef_2= ruleSlotRef | this_ThisExpression_3= ruleThisExpression | this_QuantifierExpression_4= ruleQuantifierExpression | this_DescriptiveExpression_5= ruleDescriptiveExpression | (otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')' ) ) )
            // InternalSmartContractSpecification.g:2231:2: ( ( ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression ) | this_TimePredicate_1= ruleTimePredicate | this_SlotRef_2= ruleSlotRef | this_ThisExpression_3= ruleThisExpression | this_QuantifierExpression_4= ruleQuantifierExpression | this_DescriptiveExpression_5= ruleDescriptiveExpression | (otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')' ) )
            {
            // InternalSmartContractSpecification.g:2231:2: ( ( ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression ) | this_TimePredicate_1= ruleTimePredicate | this_SlotRef_2= ruleSlotRef | this_ThisExpression_3= ruleThisExpression | this_QuantifierExpression_4= ruleQuantifierExpression | this_DescriptiveExpression_5= ruleDescriptiveExpression | (otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')' ) )
            int alt47=7;
            alt47 = dfa47.predict(input);
            switch (alt47) {
                case 1 :
                    // InternalSmartContractSpecification.g:2232:3: ( ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression )
                    {
                    // InternalSmartContractSpecification.g:2232:3: ( ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression )
                    // InternalSmartContractSpecification.g:2233:4: ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getAtomExpressionAccess().getConstantExpressionParserRuleCall_0());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ConstantExpression_0=ruleConstantExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_ConstantExpression_0;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:2244:3: this_TimePredicate_1= ruleTimePredicate
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAtomExpressionAccess().getTimePredicateParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_TimePredicate_1=ruleTimePredicate();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_TimePredicate_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:2253:3: this_SlotRef_2= ruleSlotRef
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAtomExpressionAccess().getSlotRefParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SlotRef_2=ruleSlotRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SlotRef_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalSmartContractSpecification.g:2262:3: this_ThisExpression_3= ruleThisExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAtomExpressionAccess().getThisExpressionParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ThisExpression_3=ruleThisExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ThisExpression_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalSmartContractSpecification.g:2271:3: this_QuantifierExpression_4= ruleQuantifierExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAtomExpressionAccess().getQuantifierExpressionParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_QuantifierExpression_4=ruleQuantifierExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_QuantifierExpression_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalSmartContractSpecification.g:2280:3: this_DescriptiveExpression_5= ruleDescriptiveExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAtomExpressionAccess().getDescriptiveExpressionParserRuleCall_5());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_DescriptiveExpression_5=ruleDescriptiveExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DescriptiveExpression_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalSmartContractSpecification.g:2289:3: (otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')' )
                    {
                    // InternalSmartContractSpecification.g:2289:3: (otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')' )
                    // InternalSmartContractSpecification.g:2290:4: otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')'
                    {
                    otherlv_6=(Token)match(input,24,FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getAtomExpressionAccess().getLeftParenthesisKeyword_6_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getAtomExpressionAccess().getExpressionParserRuleCall_6_1());
                      			
                    }
                    pushFollow(FOLLOW_47);
                    this_Expression_7=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_Expression_7;
                      				afterParserOrEnumRuleCall();
                      			
                    }
                    otherlv_8=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getAtomExpressionAccess().getRightParenthesisKeyword_6_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomExpression"


    // $ANTLR start "entryRuleThisExpression"
    // InternalSmartContractSpecification.g:2311:1: entryRuleThisExpression returns [EObject current=null] : iv_ruleThisExpression= ruleThisExpression EOF ;
    public final EObject entryRuleThisExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleThisExpression = null;


        try {
            // InternalSmartContractSpecification.g:2311:55: (iv_ruleThisExpression= ruleThisExpression EOF )
            // InternalSmartContractSpecification.g:2312:2: iv_ruleThisExpression= ruleThisExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getThisExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleThisExpression=ruleThisExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleThisExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleThisExpression"


    // $ANTLR start "ruleThisExpression"
    // InternalSmartContractSpecification.g:2318:1: ruleThisExpression returns [EObject current=null] : ( () ( (otherlv_1= 'this' this_ID_2= RULE_ID ) | otherlv_3= 'he' | otherlv_4= 'she' | otherlv_5= 'his' | otherlv_6= 'her' | otherlv_7= 'himself' | otherlv_8= 'herself' ) (otherlv_9= '::' ( (lv_transitive_10_0= ruleTransitiveOperator ) )? ( (lv_next_11_0= ruleSlotRef ) ) )? ) ;
    public final EObject ruleThisExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token this_ID_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Enumerator lv_transitive_10_0 = null;

        EObject lv_next_11_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2324:2: ( ( () ( (otherlv_1= 'this' this_ID_2= RULE_ID ) | otherlv_3= 'he' | otherlv_4= 'she' | otherlv_5= 'his' | otherlv_6= 'her' | otherlv_7= 'himself' | otherlv_8= 'herself' ) (otherlv_9= '::' ( (lv_transitive_10_0= ruleTransitiveOperator ) )? ( (lv_next_11_0= ruleSlotRef ) ) )? ) )
            // InternalSmartContractSpecification.g:2325:2: ( () ( (otherlv_1= 'this' this_ID_2= RULE_ID ) | otherlv_3= 'he' | otherlv_4= 'she' | otherlv_5= 'his' | otherlv_6= 'her' | otherlv_7= 'himself' | otherlv_8= 'herself' ) (otherlv_9= '::' ( (lv_transitive_10_0= ruleTransitiveOperator ) )? ( (lv_next_11_0= ruleSlotRef ) ) )? )
            {
            // InternalSmartContractSpecification.g:2325:2: ( () ( (otherlv_1= 'this' this_ID_2= RULE_ID ) | otherlv_3= 'he' | otherlv_4= 'she' | otherlv_5= 'his' | otherlv_6= 'her' | otherlv_7= 'himself' | otherlv_8= 'herself' ) (otherlv_9= '::' ( (lv_transitive_10_0= ruleTransitiveOperator ) )? ( (lv_next_11_0= ruleSlotRef ) ) )? )
            // InternalSmartContractSpecification.g:2326:3: () ( (otherlv_1= 'this' this_ID_2= RULE_ID ) | otherlv_3= 'he' | otherlv_4= 'she' | otherlv_5= 'his' | otherlv_6= 'her' | otherlv_7= 'himself' | otherlv_8= 'herself' ) (otherlv_9= '::' ( (lv_transitive_10_0= ruleTransitiveOperator ) )? ( (lv_next_11_0= ruleSlotRef ) ) )?
            {
            // InternalSmartContractSpecification.g:2326:3: ()
            // InternalSmartContractSpecification.g:2327:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getThisExpressionAccess().getThisExpressionAction_0(),
              					current);
              			
            }

            }

            // InternalSmartContractSpecification.g:2333:3: ( (otherlv_1= 'this' this_ID_2= RULE_ID ) | otherlv_3= 'he' | otherlv_4= 'she' | otherlv_5= 'his' | otherlv_6= 'her' | otherlv_7= 'himself' | otherlv_8= 'herself' )
            int alt48=7;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt48=1;
                }
                break;
            case 50:
                {
                alt48=2;
                }
                break;
            case 51:
                {
                alt48=3;
                }
                break;
            case 52:
                {
                alt48=4;
                }
                break;
            case 53:
                {
                alt48=5;
                }
                break;
            case 54:
                {
                alt48=6;
                }
                break;
            case 55:
                {
                alt48=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;
            }

            switch (alt48) {
                case 1 :
                    // InternalSmartContractSpecification.g:2334:4: (otherlv_1= 'this' this_ID_2= RULE_ID )
                    {
                    // InternalSmartContractSpecification.g:2334:4: (otherlv_1= 'this' this_ID_2= RULE_ID )
                    // InternalSmartContractSpecification.g:2335:5: otherlv_1= 'this' this_ID_2= RULE_ID
                    {
                    otherlv_1=(Token)match(input,49,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_1, grammarAccess.getThisExpressionAccess().getThisKeyword_1_0_0());
                      				
                    }
                    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(this_ID_2, grammarAccess.getThisExpressionAccess().getIDTerminalRuleCall_1_0_1());
                      				
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:2345:4: otherlv_3= 'he'
                    {
                    otherlv_3=(Token)match(input,50,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getThisExpressionAccess().getHeKeyword_1_1());
                      			
                    }

                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:2350:4: otherlv_4= 'she'
                    {
                    otherlv_4=(Token)match(input,51,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getThisExpressionAccess().getSheKeyword_1_2());
                      			
                    }

                    }
                    break;
                case 4 :
                    // InternalSmartContractSpecification.g:2355:4: otherlv_5= 'his'
                    {
                    otherlv_5=(Token)match(input,52,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getThisExpressionAccess().getHisKeyword_1_3());
                      			
                    }

                    }
                    break;
                case 5 :
                    // InternalSmartContractSpecification.g:2360:4: otherlv_6= 'her'
                    {
                    otherlv_6=(Token)match(input,53,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getThisExpressionAccess().getHerKeyword_1_4());
                      			
                    }

                    }
                    break;
                case 6 :
                    // InternalSmartContractSpecification.g:2365:4: otherlv_7= 'himself'
                    {
                    otherlv_7=(Token)match(input,54,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getThisExpressionAccess().getHimselfKeyword_1_5());
                      			
                    }

                    }
                    break;
                case 7 :
                    // InternalSmartContractSpecification.g:2370:4: otherlv_8= 'herself'
                    {
                    otherlv_8=(Token)match(input,55,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getThisExpressionAccess().getHerselfKeyword_1_6());
                      			
                    }

                    }
                    break;

            }

            // InternalSmartContractSpecification.g:2375:3: (otherlv_9= '::' ( (lv_transitive_10_0= ruleTransitiveOperator ) )? ( (lv_next_11_0= ruleSlotRef ) ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==56) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalSmartContractSpecification.g:2376:4: otherlv_9= '::' ( (lv_transitive_10_0= ruleTransitiveOperator ) )? ( (lv_next_11_0= ruleSlotRef ) )
                    {
                    otherlv_9=(Token)match(input,56,FOLLOW_49); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getThisExpressionAccess().getColonColonKeyword_2_0());
                      			
                    }
                    // InternalSmartContractSpecification.g:2380:4: ( (lv_transitive_10_0= ruleTransitiveOperator ) )?
                    int alt49=2;
                    int LA49_0 = input.LA(1);

                    if ( (LA49_0==89||LA49_0==91) ) {
                        alt49=1;
                    }
                    switch (alt49) {
                        case 1 :
                            // InternalSmartContractSpecification.g:2381:5: (lv_transitive_10_0= ruleTransitiveOperator )
                            {
                            // InternalSmartContractSpecification.g:2381:5: (lv_transitive_10_0= ruleTransitiveOperator )
                            // InternalSmartContractSpecification.g:2382:6: lv_transitive_10_0= ruleTransitiveOperator
                            {
                            if ( state.backtracking==0 ) {

                              						newCompositeNode(grammarAccess.getThisExpressionAccess().getTransitiveTransitiveOperatorEnumRuleCall_2_1_0());
                              					
                            }
                            pushFollow(FOLLOW_3);
                            lv_transitive_10_0=ruleTransitiveOperator();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						if (current==null) {
                              							current = createModelElementForParent(grammarAccess.getThisExpressionRule());
                              						}
                              						set(
                              							current,
                              							"transitive",
                              							lv_transitive_10_0,
                              							"edu.ustb.sei.mde.SmartContractSpecification.TransitiveOperator");
                              						afterParserOrEnumRuleCall();
                              					
                            }

                            }


                            }
                            break;

                    }

                    // InternalSmartContractSpecification.g:2399:4: ( (lv_next_11_0= ruleSlotRef ) )
                    // InternalSmartContractSpecification.g:2400:5: (lv_next_11_0= ruleSlotRef )
                    {
                    // InternalSmartContractSpecification.g:2400:5: (lv_next_11_0= ruleSlotRef )
                    // InternalSmartContractSpecification.g:2401:6: lv_next_11_0= ruleSlotRef
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getThisExpressionAccess().getNextSlotRefParserRuleCall_2_2_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_next_11_0=ruleSlotRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getThisExpressionRule());
                      						}
                      						set(
                      							current,
                      							"next",
                      							lv_next_11_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.SlotRef");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleThisExpression"


    // $ANTLR start "entryRuleSlotRef"
    // InternalSmartContractSpecification.g:2423:1: entryRuleSlotRef returns [EObject current=null] : iv_ruleSlotRef= ruleSlotRef EOF ;
    public final EObject entryRuleSlotRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSlotRef = null;


        try {
            // InternalSmartContractSpecification.g:2423:48: (iv_ruleSlotRef= ruleSlotRef EOF )
            // InternalSmartContractSpecification.g:2424:2: iv_ruleSlotRef= ruleSlotRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSlotRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSlotRef=ruleSlotRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSlotRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSlotRef"


    // $ANTLR start "ruleSlotRef"
    // InternalSmartContractSpecification.g:2430:1: ruleSlotRef returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '::' ( (lv_transitive_2_0= ruleTransitiveOperator ) )? ( (lv_next_3_0= ruleSlotRef ) ) )? ) ;
    public final EObject ruleSlotRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Enumerator lv_transitive_2_0 = null;

        EObject lv_next_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2436:2: ( ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '::' ( (lv_transitive_2_0= ruleTransitiveOperator ) )? ( (lv_next_3_0= ruleSlotRef ) ) )? ) )
            // InternalSmartContractSpecification.g:2437:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '::' ( (lv_transitive_2_0= ruleTransitiveOperator ) )? ( (lv_next_3_0= ruleSlotRef ) ) )? )
            {
            // InternalSmartContractSpecification.g:2437:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '::' ( (lv_transitive_2_0= ruleTransitiveOperator ) )? ( (lv_next_3_0= ruleSlotRef ) ) )? )
            // InternalSmartContractSpecification.g:2438:3: ( (otherlv_0= RULE_ID ) ) (otherlv_1= '::' ( (lv_transitive_2_0= ruleTransitiveOperator ) )? ( (lv_next_3_0= ruleSlotRef ) ) )?
            {
            // InternalSmartContractSpecification.g:2438:3: ( (otherlv_0= RULE_ID ) )
            // InternalSmartContractSpecification.g:2439:4: (otherlv_0= RULE_ID )
            {
            // InternalSmartContractSpecification.g:2439:4: (otherlv_0= RULE_ID )
            // InternalSmartContractSpecification.g:2440:5: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getSlotRefRule());
              					}
              				
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_0, grammarAccess.getSlotRefAccess().getSlotDataSlotCrossReference_0_0());
              				
            }

            }


            }

            // InternalSmartContractSpecification.g:2451:3: (otherlv_1= '::' ( (lv_transitive_2_0= ruleTransitiveOperator ) )? ( (lv_next_3_0= ruleSlotRef ) ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==56) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalSmartContractSpecification.g:2452:4: otherlv_1= '::' ( (lv_transitive_2_0= ruleTransitiveOperator ) )? ( (lv_next_3_0= ruleSlotRef ) )
                    {
                    otherlv_1=(Token)match(input,56,FOLLOW_49); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getSlotRefAccess().getColonColonKeyword_1_0());
                      			
                    }
                    // InternalSmartContractSpecification.g:2456:4: ( (lv_transitive_2_0= ruleTransitiveOperator ) )?
                    int alt51=2;
                    int LA51_0 = input.LA(1);

                    if ( (LA51_0==89||LA51_0==91) ) {
                        alt51=1;
                    }
                    switch (alt51) {
                        case 1 :
                            // InternalSmartContractSpecification.g:2457:5: (lv_transitive_2_0= ruleTransitiveOperator )
                            {
                            // InternalSmartContractSpecification.g:2457:5: (lv_transitive_2_0= ruleTransitiveOperator )
                            // InternalSmartContractSpecification.g:2458:6: lv_transitive_2_0= ruleTransitiveOperator
                            {
                            if ( state.backtracking==0 ) {

                              						newCompositeNode(grammarAccess.getSlotRefAccess().getTransitiveTransitiveOperatorEnumRuleCall_1_1_0());
                              					
                            }
                            pushFollow(FOLLOW_3);
                            lv_transitive_2_0=ruleTransitiveOperator();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						if (current==null) {
                              							current = createModelElementForParent(grammarAccess.getSlotRefRule());
                              						}
                              						set(
                              							current,
                              							"transitive",
                              							lv_transitive_2_0,
                              							"edu.ustb.sei.mde.SmartContractSpecification.TransitiveOperator");
                              						afterParserOrEnumRuleCall();
                              					
                            }

                            }


                            }
                            break;

                    }

                    // InternalSmartContractSpecification.g:2475:4: ( (lv_next_3_0= ruleSlotRef ) )
                    // InternalSmartContractSpecification.g:2476:5: (lv_next_3_0= ruleSlotRef )
                    {
                    // InternalSmartContractSpecification.g:2476:5: (lv_next_3_0= ruleSlotRef )
                    // InternalSmartContractSpecification.g:2477:6: lv_next_3_0= ruleSlotRef
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getSlotRefAccess().getNextSlotRefParserRuleCall_1_2_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_next_3_0=ruleSlotRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getSlotRefRule());
                      						}
                      						set(
                      							current,
                      							"next",
                      							lv_next_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.SlotRef");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSlotRef"


    // $ANTLR start "entryRuleConstantExpression"
    // InternalSmartContractSpecification.g:2499:1: entryRuleConstantExpression returns [EObject current=null] : iv_ruleConstantExpression= ruleConstantExpression EOF ;
    public final EObject entryRuleConstantExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstantExpression = null;


        try {
            // InternalSmartContractSpecification.g:2499:59: (iv_ruleConstantExpression= ruleConstantExpression EOF )
            // InternalSmartContractSpecification.g:2500:2: iv_ruleConstantExpression= ruleConstantExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstantExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleConstantExpression=ruleConstantExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstantExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstantExpression"


    // $ANTLR start "ruleConstantExpression"
    // InternalSmartContractSpecification.g:2506:1: ruleConstantExpression returns [EObject current=null] : ( ( () ( (lv_value_1_0= ruleInteger ) ) ) | ( () ( (lv_value_3_0= ruleBoolean ) ) ) ) ;
    public final EObject ruleConstantExpression() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_1_0 = null;

        AntlrDatatypeRuleToken lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2512:2: ( ( ( () ( (lv_value_1_0= ruleInteger ) ) ) | ( () ( (lv_value_3_0= ruleBoolean ) ) ) ) )
            // InternalSmartContractSpecification.g:2513:2: ( ( () ( (lv_value_1_0= ruleInteger ) ) ) | ( () ( (lv_value_3_0= ruleBoolean ) ) ) )
            {
            // InternalSmartContractSpecification.g:2513:2: ( ( () ( (lv_value_1_0= ruleInteger ) ) ) | ( () ( (lv_value_3_0= ruleBoolean ) ) ) )
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==RULE_INT||LA53_0==57) ) {
                alt53=1;
            }
            else if ( ((LA53_0>=58 && LA53_0<=59)) ) {
                alt53=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;
            }
            switch (alt53) {
                case 1 :
                    // InternalSmartContractSpecification.g:2514:3: ( () ( (lv_value_1_0= ruleInteger ) ) )
                    {
                    // InternalSmartContractSpecification.g:2514:3: ( () ( (lv_value_1_0= ruleInteger ) ) )
                    // InternalSmartContractSpecification.g:2515:4: () ( (lv_value_1_0= ruleInteger ) )
                    {
                    // InternalSmartContractSpecification.g:2515:4: ()
                    // InternalSmartContractSpecification.g:2516:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getConstantExpressionAccess().getIntegerConstantAction_0_0(),
                      						current);
                      				
                    }

                    }

                    // InternalSmartContractSpecification.g:2522:4: ( (lv_value_1_0= ruleInteger ) )
                    // InternalSmartContractSpecification.g:2523:5: (lv_value_1_0= ruleInteger )
                    {
                    // InternalSmartContractSpecification.g:2523:5: (lv_value_1_0= ruleInteger )
                    // InternalSmartContractSpecification.g:2524:6: lv_value_1_0= ruleInteger
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getConstantExpressionAccess().getValueIntegerParserRuleCall_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_1_0=ruleInteger();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getConstantExpressionRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_1_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.Integer");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:2543:3: ( () ( (lv_value_3_0= ruleBoolean ) ) )
                    {
                    // InternalSmartContractSpecification.g:2543:3: ( () ( (lv_value_3_0= ruleBoolean ) ) )
                    // InternalSmartContractSpecification.g:2544:4: () ( (lv_value_3_0= ruleBoolean ) )
                    {
                    // InternalSmartContractSpecification.g:2544:4: ()
                    // InternalSmartContractSpecification.g:2545:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getConstantExpressionAccess().getBooleanconstantAction_1_0(),
                      						current);
                      				
                    }

                    }

                    // InternalSmartContractSpecification.g:2551:4: ( (lv_value_3_0= ruleBoolean ) )
                    // InternalSmartContractSpecification.g:2552:5: (lv_value_3_0= ruleBoolean )
                    {
                    // InternalSmartContractSpecification.g:2552:5: (lv_value_3_0= ruleBoolean )
                    // InternalSmartContractSpecification.g:2553:6: lv_value_3_0= ruleBoolean
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getConstantExpressionAccess().getValueBooleanParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_3_0=ruleBoolean();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getConstantExpressionRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.Boolean");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstantExpression"


    // $ANTLR start "entryRuleInteger"
    // InternalSmartContractSpecification.g:2575:1: entryRuleInteger returns [String current=null] : iv_ruleInteger= ruleInteger EOF ;
    public final String entryRuleInteger() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInteger = null;


        try {
            // InternalSmartContractSpecification.g:2575:47: (iv_ruleInteger= ruleInteger EOF )
            // InternalSmartContractSpecification.g:2576:2: iv_ruleInteger= ruleInteger EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInteger=ruleInteger();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteger.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteger"


    // $ANTLR start "ruleInteger"
    // InternalSmartContractSpecification.g:2582:1: ruleInteger returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleInteger() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2588:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalSmartContractSpecification.g:2589:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalSmartContractSpecification.g:2589:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalSmartContractSpecification.g:2590:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalSmartContractSpecification.g:2590:3: (kw= '-' )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==57) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalSmartContractSpecification.g:2591:4: kw= '-'
                    {
                    kw=(Token)match(input,57,FOLLOW_50); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getIntegerAccess().getHyphenMinusKeyword_0());
                      			
                    }

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_1);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_1, grammarAccess.getIntegerAccess().getINTTerminalRuleCall_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteger"


    // $ANTLR start "entryRuleBoolean"
    // InternalSmartContractSpecification.g:2608:1: entryRuleBoolean returns [String current=null] : iv_ruleBoolean= ruleBoolean EOF ;
    public final String entryRuleBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBoolean = null;


        try {
            // InternalSmartContractSpecification.g:2608:47: (iv_ruleBoolean= ruleBoolean EOF )
            // InternalSmartContractSpecification.g:2609:2: iv_ruleBoolean= ruleBoolean EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBoolean=ruleBoolean();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBoolean.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalSmartContractSpecification.g:2615:1: ruleBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2621:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalSmartContractSpecification.g:2622:2: (kw= 'true' | kw= 'false' )
            {
            // InternalSmartContractSpecification.g:2622:2: (kw= 'true' | kw= 'false' )
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==58) ) {
                alt55=1;
            }
            else if ( (LA55_0==59) ) {
                alt55=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }
            switch (alt55) {
                case 1 :
                    // InternalSmartContractSpecification.g:2623:3: kw= 'true'
                    {
                    kw=(Token)match(input,58,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBooleanAccess().getTrueKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:2629:3: kw= 'false'
                    {
                    kw=(Token)match(input,59,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBooleanAccess().getFalseKeyword_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean"


    // $ANTLR start "entryRuleTimePredicate"
    // InternalSmartContractSpecification.g:2638:1: entryRuleTimePredicate returns [EObject current=null] : iv_ruleTimePredicate= ruleTimePredicate EOF ;
    public final EObject entryRuleTimePredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimePredicate = null;


        try {
            // InternalSmartContractSpecification.g:2638:54: (iv_ruleTimePredicate= ruleTimePredicate EOF )
            // InternalSmartContractSpecification.g:2639:2: iv_ruleTimePredicate= ruleTimePredicate EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimePredicateRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimePredicate=ruleTimePredicate();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimePredicate; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimePredicate"


    // $ANTLR start "ruleTimePredicate"
    // InternalSmartContractSpecification.g:2645:1: ruleTimePredicate returns [EObject current=null] : (this_SimpleTimePredicate_0= ruleSimpleTimePredicate | ( ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate ) ) ;
    public final EObject ruleTimePredicate() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleTimePredicate_0 = null;

        EObject this_BoundedTimePredicate_1 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2651:2: ( (this_SimpleTimePredicate_0= ruleSimpleTimePredicate | ( ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate ) ) )
            // InternalSmartContractSpecification.g:2652:2: (this_SimpleTimePredicate_0= ruleSimpleTimePredicate | ( ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate ) )
            {
            // InternalSmartContractSpecification.g:2652:2: (this_SimpleTimePredicate_0= ruleSimpleTimePredicate | ( ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate ) )
            int alt56=2;
            alt56 = dfa56.predict(input);
            switch (alt56) {
                case 1 :
                    // InternalSmartContractSpecification.g:2653:3: this_SimpleTimePredicate_0= ruleSimpleTimePredicate
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimePredicateAccess().getSimpleTimePredicateParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SimpleTimePredicate_0=ruleSimpleTimePredicate();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SimpleTimePredicate_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:2662:3: ( ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate )
                    {
                    // InternalSmartContractSpecification.g:2662:3: ( ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate )
                    // InternalSmartContractSpecification.g:2663:4: ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getTimePredicateAccess().getBoundedTimePredicateParserRuleCall_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_BoundedTimePredicate_1=ruleBoundedTimePredicate();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_BoundedTimePredicate_1;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimePredicate"


    // $ANTLR start "entryRuleSimpleTimePredicate"
    // InternalSmartContractSpecification.g:2677:1: entryRuleSimpleTimePredicate returns [EObject current=null] : iv_ruleSimpleTimePredicate= ruleSimpleTimePredicate EOF ;
    public final EObject entryRuleSimpleTimePredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleTimePredicate = null;


        try {
            // InternalSmartContractSpecification.g:2677:60: (iv_ruleSimpleTimePredicate= ruleSimpleTimePredicate EOF )
            // InternalSmartContractSpecification.g:2678:2: iv_ruleSimpleTimePredicate= ruleSimpleTimePredicate EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleTimePredicateRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSimpleTimePredicate=ruleSimpleTimePredicate();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleTimePredicate; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleTimePredicate"


    // $ANTLR start "ruleSimpleTimePredicate"
    // InternalSmartContractSpecification.g:2684:1: ruleSimpleTimePredicate returns [EObject current=null] : ( ( (lv_operator_0_0= ruleTimePredicateOperator ) ) ( (lv_base_1_0= ruleTimeExpression ) ) ) ;
    public final EObject ruleSimpleTimePredicate() throws RecognitionException {
        EObject current = null;

        Enumerator lv_operator_0_0 = null;

        EObject lv_base_1_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2690:2: ( ( ( (lv_operator_0_0= ruleTimePredicateOperator ) ) ( (lv_base_1_0= ruleTimeExpression ) ) ) )
            // InternalSmartContractSpecification.g:2691:2: ( ( (lv_operator_0_0= ruleTimePredicateOperator ) ) ( (lv_base_1_0= ruleTimeExpression ) ) )
            {
            // InternalSmartContractSpecification.g:2691:2: ( ( (lv_operator_0_0= ruleTimePredicateOperator ) ) ( (lv_base_1_0= ruleTimeExpression ) ) )
            // InternalSmartContractSpecification.g:2692:3: ( (lv_operator_0_0= ruleTimePredicateOperator ) ) ( (lv_base_1_0= ruleTimeExpression ) )
            {
            // InternalSmartContractSpecification.g:2692:3: ( (lv_operator_0_0= ruleTimePredicateOperator ) )
            // InternalSmartContractSpecification.g:2693:4: (lv_operator_0_0= ruleTimePredicateOperator )
            {
            // InternalSmartContractSpecification.g:2693:4: (lv_operator_0_0= ruleTimePredicateOperator )
            // InternalSmartContractSpecification.g:2694:5: lv_operator_0_0= ruleTimePredicateOperator
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSimpleTimePredicateAccess().getOperatorTimePredicateOperatorEnumRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_51);
            lv_operator_0_0=ruleTimePredicateOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSimpleTimePredicateRule());
              					}
              					set(
              						current,
              						"operator",
              						lv_operator_0_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.TimePredicateOperator");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalSmartContractSpecification.g:2711:3: ( (lv_base_1_0= ruleTimeExpression ) )
            // InternalSmartContractSpecification.g:2712:4: (lv_base_1_0= ruleTimeExpression )
            {
            // InternalSmartContractSpecification.g:2712:4: (lv_base_1_0= ruleTimeExpression )
            // InternalSmartContractSpecification.g:2713:5: lv_base_1_0= ruleTimeExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSimpleTimePredicateAccess().getBaseTimeExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_base_1_0=ruleTimeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSimpleTimePredicateRule());
              					}
              					set(
              						current,
              						"base",
              						lv_base_1_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.TimeExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleTimePredicate"


    // $ANTLR start "entryRuleBoundedTimePredicate"
    // InternalSmartContractSpecification.g:2734:1: entryRuleBoundedTimePredicate returns [EObject current=null] : iv_ruleBoundedTimePredicate= ruleBoundedTimePredicate EOF ;
    public final EObject entryRuleBoundedTimePredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoundedTimePredicate = null;


        try {
            // InternalSmartContractSpecification.g:2734:61: (iv_ruleBoundedTimePredicate= ruleBoundedTimePredicate EOF )
            // InternalSmartContractSpecification.g:2735:2: iv_ruleBoundedTimePredicate= ruleBoundedTimePredicate EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBoundedTimePredicateRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBoundedTimePredicate=ruleBoundedTimePredicate();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBoundedTimePredicate; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoundedTimePredicate"


    // $ANTLR start "ruleBoundedTimePredicate"
    // InternalSmartContractSpecification.g:2741:1: ruleBoundedTimePredicate returns [EObject current=null] : ( ( (lv_closed_0_0= 'within' ) )? ( (lv_bound_1_0= ruleTimeExpression ) ) ( (lv_operator_2_0= ruleTimePredicateOperator ) ) ( (lv_base_3_0= ruleTimeExpression ) ) ) ;
    public final EObject ruleBoundedTimePredicate() throws RecognitionException {
        EObject current = null;

        Token lv_closed_0_0=null;
        EObject lv_bound_1_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_base_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2747:2: ( ( ( (lv_closed_0_0= 'within' ) )? ( (lv_bound_1_0= ruleTimeExpression ) ) ( (lv_operator_2_0= ruleTimePredicateOperator ) ) ( (lv_base_3_0= ruleTimeExpression ) ) ) )
            // InternalSmartContractSpecification.g:2748:2: ( ( (lv_closed_0_0= 'within' ) )? ( (lv_bound_1_0= ruleTimeExpression ) ) ( (lv_operator_2_0= ruleTimePredicateOperator ) ) ( (lv_base_3_0= ruleTimeExpression ) ) )
            {
            // InternalSmartContractSpecification.g:2748:2: ( ( (lv_closed_0_0= 'within' ) )? ( (lv_bound_1_0= ruleTimeExpression ) ) ( (lv_operator_2_0= ruleTimePredicateOperator ) ) ( (lv_base_3_0= ruleTimeExpression ) ) )
            // InternalSmartContractSpecification.g:2749:3: ( (lv_closed_0_0= 'within' ) )? ( (lv_bound_1_0= ruleTimeExpression ) ) ( (lv_operator_2_0= ruleTimePredicateOperator ) ) ( (lv_base_3_0= ruleTimeExpression ) )
            {
            // InternalSmartContractSpecification.g:2749:3: ( (lv_closed_0_0= 'within' ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==60) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalSmartContractSpecification.g:2750:4: (lv_closed_0_0= 'within' )
                    {
                    // InternalSmartContractSpecification.g:2750:4: (lv_closed_0_0= 'within' )
                    // InternalSmartContractSpecification.g:2751:5: lv_closed_0_0= 'within'
                    {
                    lv_closed_0_0=(Token)match(input,60,FOLLOW_51); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_closed_0_0, grammarAccess.getBoundedTimePredicateAccess().getClosedWithinKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getBoundedTimePredicateRule());
                      					}
                      					setWithLastConsumed(current, "closed", true, "within");
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalSmartContractSpecification.g:2763:3: ( (lv_bound_1_0= ruleTimeExpression ) )
            // InternalSmartContractSpecification.g:2764:4: (lv_bound_1_0= ruleTimeExpression )
            {
            // InternalSmartContractSpecification.g:2764:4: (lv_bound_1_0= ruleTimeExpression )
            // InternalSmartContractSpecification.g:2765:5: lv_bound_1_0= ruleTimeExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBoundedTimePredicateAccess().getBoundTimeExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_52);
            lv_bound_1_0=ruleTimeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBoundedTimePredicateRule());
              					}
              					set(
              						current,
              						"bound",
              						lv_bound_1_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.TimeExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalSmartContractSpecification.g:2782:3: ( (lv_operator_2_0= ruleTimePredicateOperator ) )
            // InternalSmartContractSpecification.g:2783:4: (lv_operator_2_0= ruleTimePredicateOperator )
            {
            // InternalSmartContractSpecification.g:2783:4: (lv_operator_2_0= ruleTimePredicateOperator )
            // InternalSmartContractSpecification.g:2784:5: lv_operator_2_0= ruleTimePredicateOperator
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBoundedTimePredicateAccess().getOperatorTimePredicateOperatorEnumRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_51);
            lv_operator_2_0=ruleTimePredicateOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBoundedTimePredicateRule());
              					}
              					set(
              						current,
              						"operator",
              						lv_operator_2_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.TimePredicateOperator");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalSmartContractSpecification.g:2801:3: ( (lv_base_3_0= ruleTimeExpression ) )
            // InternalSmartContractSpecification.g:2802:4: (lv_base_3_0= ruleTimeExpression )
            {
            // InternalSmartContractSpecification.g:2802:4: (lv_base_3_0= ruleTimeExpression )
            // InternalSmartContractSpecification.g:2803:5: lv_base_3_0= ruleTimeExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBoundedTimePredicateAccess().getBaseTimeExpressionParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_base_3_0=ruleTimeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBoundedTimePredicateRule());
              					}
              					set(
              						current,
              						"base",
              						lv_base_3_0,
              						"edu.ustb.sei.mde.SmartContractSpecification.TimeExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoundedTimePredicate"


    // $ANTLR start "entryRuleTimeExpression"
    // InternalSmartContractSpecification.g:2824:1: entryRuleTimeExpression returns [EObject current=null] : iv_ruleTimeExpression= ruleTimeExpression EOF ;
    public final EObject entryRuleTimeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeExpression = null;


        try {
            // InternalSmartContractSpecification.g:2824:55: (iv_ruleTimeExpression= ruleTimeExpression EOF )
            // InternalSmartContractSpecification.g:2825:2: iv_ruleTimeExpression= ruleTimeExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimeExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimeExpression=ruleTimeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimeExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeExpression"


    // $ANTLR start "ruleTimeExpression"
    // InternalSmartContractSpecification.g:2831:1: ruleTimeExpression returns [EObject current=null] : (this_TimeConstant_0= ruleTimeConstant | this_TimeQuery_1= ruleTimeQuery | this_TimeVarExpression_2= ruleTimeVarExpression ) ;
    public final EObject ruleTimeExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TimeConstant_0 = null;

        EObject this_TimeQuery_1 = null;

        EObject this_TimeVarExpression_2 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2837:2: ( (this_TimeConstant_0= ruleTimeConstant | this_TimeQuery_1= ruleTimeQuery | this_TimeVarExpression_2= ruleTimeVarExpression ) )
            // InternalSmartContractSpecification.g:2838:2: (this_TimeConstant_0= ruleTimeConstant | this_TimeQuery_1= ruleTimeQuery | this_TimeVarExpression_2= ruleTimeVarExpression )
            {
            // InternalSmartContractSpecification.g:2838:2: (this_TimeConstant_0= ruleTimeConstant | this_TimeQuery_1= ruleTimeQuery | this_TimeVarExpression_2= ruleTimeVarExpression )
            int alt58=3;
            switch ( input.LA(1) ) {
            case EOF:
            case RULE_INT:
            case 15:
            case 23:
            case 25:
            case 29:
            case 30:
            case 32:
            case 34:
            case 35:
            case 36:
            case 38:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 57:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 92:
            case 93:
                {
                alt58=1;
                }
                break;
            case RULE_ID:
            case 49:
            case 73:
            case 75:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
                {
                alt58=2;
                }
                break;
            case 72:
                {
                alt58=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 58, 0, input);

                throw nvae;
            }

            switch (alt58) {
                case 1 :
                    // InternalSmartContractSpecification.g:2839:3: this_TimeConstant_0= ruleTimeConstant
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimeExpressionAccess().getTimeConstantParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_TimeConstant_0=ruleTimeConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_TimeConstant_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:2848:3: this_TimeQuery_1= ruleTimeQuery
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimeExpressionAccess().getTimeQueryParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_TimeQuery_1=ruleTimeQuery();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_TimeQuery_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:2857:3: this_TimeVarExpression_2= ruleTimeVarExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimeExpressionAccess().getTimeVarExpressionParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_TimeVarExpression_2=ruleTimeVarExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_TimeVarExpression_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeExpression"


    // $ANTLR start "entryRuleTimeQuery"
    // InternalSmartContractSpecification.g:2869:1: entryRuleTimeQuery returns [EObject current=null] : iv_ruleTimeQuery= ruleTimeQuery EOF ;
    public final EObject entryRuleTimeQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeQuery = null;


        try {
            // InternalSmartContractSpecification.g:2869:50: (iv_ruleTimeQuery= ruleTimeQuery EOF )
            // InternalSmartContractSpecification.g:2870:2: iv_ruleTimeQuery= ruleTimeQuery EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimeQueryRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimeQuery=ruleTimeQuery();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimeQuery; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeQuery"


    // $ANTLR start "ruleTimeQuery"
    // InternalSmartContractSpecification.g:2876:1: ruleTimeQuery returns [EObject current=null] : (this_ActionEnforcedTimeQuery_0= ruleActionEnforcedTimeQuery | this_TermValidTimeQuery_1= ruleTermValidTimeQuery | this_GlobalTimeQuery_2= ruleGlobalTimeQuery ) ;
    public final EObject ruleTimeQuery() throws RecognitionException {
        EObject current = null;

        EObject this_ActionEnforcedTimeQuery_0 = null;

        EObject this_TermValidTimeQuery_1 = null;

        EObject this_GlobalTimeQuery_2 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2882:2: ( (this_ActionEnforcedTimeQuery_0= ruleActionEnforcedTimeQuery | this_TermValidTimeQuery_1= ruleTermValidTimeQuery | this_GlobalTimeQuery_2= ruleGlobalTimeQuery ) )
            // InternalSmartContractSpecification.g:2883:2: (this_ActionEnforcedTimeQuery_0= ruleActionEnforcedTimeQuery | this_TermValidTimeQuery_1= ruleTermValidTimeQuery | this_GlobalTimeQuery_2= ruleGlobalTimeQuery )
            {
            // InternalSmartContractSpecification.g:2883:2: (this_ActionEnforcedTimeQuery_0= ruleActionEnforcedTimeQuery | this_TermValidTimeQuery_1= ruleTermValidTimeQuery | this_GlobalTimeQuery_2= ruleGlobalTimeQuery )
            int alt59=3;
            switch ( input.LA(1) ) {
            case 49:
            case 73:
            case 75:
            case 94:
            case 95:
            case 96:
                {
                alt59=1;
                }
                break;
            case RULE_ID:
                {
                int LA59_2 = input.LA(2);

                if ( (LA59_2==61) ) {
                    alt59=1;
                }
                else if ( (LA59_2==62) ) {
                    alt59=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 59, 2, input);

                    throw nvae;
                }
                }
                break;
            case 97:
            case 98:
            case 99:
                {
                alt59=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 59, 0, input);

                throw nvae;
            }

            switch (alt59) {
                case 1 :
                    // InternalSmartContractSpecification.g:2884:3: this_ActionEnforcedTimeQuery_0= ruleActionEnforcedTimeQuery
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimeQueryAccess().getActionEnforcedTimeQueryParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ActionEnforcedTimeQuery_0=ruleActionEnforcedTimeQuery();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ActionEnforcedTimeQuery_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:2893:3: this_TermValidTimeQuery_1= ruleTermValidTimeQuery
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimeQueryAccess().getTermValidTimeQueryParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_TermValidTimeQuery_1=ruleTermValidTimeQuery();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_TermValidTimeQuery_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:2902:3: this_GlobalTimeQuery_2= ruleGlobalTimeQuery
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimeQueryAccess().getGlobalTimeQueryParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GlobalTimeQuery_2=ruleGlobalTimeQuery();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GlobalTimeQuery_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeQuery"


    // $ANTLR start "entryRuleActionEnforcedTimeQuery"
    // InternalSmartContractSpecification.g:2914:1: entryRuleActionEnforcedTimeQuery returns [EObject current=null] : iv_ruleActionEnforcedTimeQuery= ruleActionEnforcedTimeQuery EOF ;
    public final EObject entryRuleActionEnforcedTimeQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActionEnforcedTimeQuery = null;


        try {
            // InternalSmartContractSpecification.g:2914:64: (iv_ruleActionEnforcedTimeQuery= ruleActionEnforcedTimeQuery EOF )
            // InternalSmartContractSpecification.g:2915:2: iv_ruleActionEnforcedTimeQuery= ruleActionEnforcedTimeQuery EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActionEnforcedTimeQueryRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleActionEnforcedTimeQuery=ruleActionEnforcedTimeQuery();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleActionEnforcedTimeQuery; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActionEnforcedTimeQuery"


    // $ANTLR start "ruleActionEnforcedTimeQuery"
    // InternalSmartContractSpecification.g:2921:1: ruleActionEnforcedTimeQuery returns [EObject current=null] : ( ( (lv_multiplicity_0_0= ruleActionMultiplicity ) )? ( (otherlv_1= RULE_ID ) ) otherlv_2= 'did' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleActionEnforcedTimeQuery() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Enumerator lv_multiplicity_0_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2927:2: ( ( ( (lv_multiplicity_0_0= ruleActionMultiplicity ) )? ( (otherlv_1= RULE_ID ) ) otherlv_2= 'did' ( (otherlv_3= RULE_ID ) ) ) )
            // InternalSmartContractSpecification.g:2928:2: ( ( (lv_multiplicity_0_0= ruleActionMultiplicity ) )? ( (otherlv_1= RULE_ID ) ) otherlv_2= 'did' ( (otherlv_3= RULE_ID ) ) )
            {
            // InternalSmartContractSpecification.g:2928:2: ( ( (lv_multiplicity_0_0= ruleActionMultiplicity ) )? ( (otherlv_1= RULE_ID ) ) otherlv_2= 'did' ( (otherlv_3= RULE_ID ) ) )
            // InternalSmartContractSpecification.g:2929:3: ( (lv_multiplicity_0_0= ruleActionMultiplicity ) )? ( (otherlv_1= RULE_ID ) ) otherlv_2= 'did' ( (otherlv_3= RULE_ID ) )
            {
            // InternalSmartContractSpecification.g:2929:3: ( (lv_multiplicity_0_0= ruleActionMultiplicity ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==49||LA60_0==73||LA60_0==75||(LA60_0>=94 && LA60_0<=96)) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalSmartContractSpecification.g:2930:4: (lv_multiplicity_0_0= ruleActionMultiplicity )
                    {
                    // InternalSmartContractSpecification.g:2930:4: (lv_multiplicity_0_0= ruleActionMultiplicity )
                    // InternalSmartContractSpecification.g:2931:5: lv_multiplicity_0_0= ruleActionMultiplicity
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getActionEnforcedTimeQueryAccess().getMultiplicityActionMultiplicityEnumRuleCall_0_0());
                      				
                    }
                    pushFollow(FOLLOW_3);
                    lv_multiplicity_0_0=ruleActionMultiplicity();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getActionEnforcedTimeQueryRule());
                      					}
                      					set(
                      						current,
                      						"multiplicity",
                      						lv_multiplicity_0_0,
                      						"edu.ustb.sei.mde.SmartContractSpecification.ActionMultiplicity");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalSmartContractSpecification.g:2948:3: ( (otherlv_1= RULE_ID ) )
            // InternalSmartContractSpecification.g:2949:4: (otherlv_1= RULE_ID )
            {
            // InternalSmartContractSpecification.g:2949:4: (otherlv_1= RULE_ID )
            // InternalSmartContractSpecification.g:2950:5: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getActionEnforcedTimeQueryRule());
              					}
              				
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_53); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_1, grammarAccess.getActionEnforcedTimeQueryAccess().getPartyInternalPartyCrossReference_1_0());
              				
            }

            }


            }

            otherlv_2=(Token)match(input,61,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getActionEnforcedTimeQueryAccess().getDidKeyword_2());
              		
            }
            // InternalSmartContractSpecification.g:2965:3: ( (otherlv_3= RULE_ID ) )
            // InternalSmartContractSpecification.g:2966:4: (otherlv_3= RULE_ID )
            {
            // InternalSmartContractSpecification.g:2966:4: (otherlv_3= RULE_ID )
            // InternalSmartContractSpecification.g:2967:5: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getActionEnforcedTimeQueryRule());
              					}
              				
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_3, grammarAccess.getActionEnforcedTimeQueryAccess().getActionActionCrossReference_3_0());
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionEnforcedTimeQuery"


    // $ANTLR start "entryRuleTermValidTimeQuery"
    // InternalSmartContractSpecification.g:2982:1: entryRuleTermValidTimeQuery returns [EObject current=null] : iv_ruleTermValidTimeQuery= ruleTermValidTimeQuery EOF ;
    public final EObject entryRuleTermValidTimeQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTermValidTimeQuery = null;


        try {
            // InternalSmartContractSpecification.g:2982:59: (iv_ruleTermValidTimeQuery= ruleTermValidTimeQuery EOF )
            // InternalSmartContractSpecification.g:2983:2: iv_ruleTermValidTimeQuery= ruleTermValidTimeQuery EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTermValidTimeQueryRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTermValidTimeQuery=ruleTermValidTimeQuery();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTermValidTimeQuery; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTermValidTimeQuery"


    // $ANTLR start "ruleTermValidTimeQuery"
    // InternalSmartContractSpecification.g:2989:1: ruleTermValidTimeQuery returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= 'is' (otherlv_2= 'valid' | otherlv_3= 'fulfilled' | otherlv_4= 'executed' ) ) ;
    public final EObject ruleTermValidTimeQuery() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:2995:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= 'is' (otherlv_2= 'valid' | otherlv_3= 'fulfilled' | otherlv_4= 'executed' ) ) )
            // InternalSmartContractSpecification.g:2996:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= 'is' (otherlv_2= 'valid' | otherlv_3= 'fulfilled' | otherlv_4= 'executed' ) )
            {
            // InternalSmartContractSpecification.g:2996:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= 'is' (otherlv_2= 'valid' | otherlv_3= 'fulfilled' | otherlv_4= 'executed' ) )
            // InternalSmartContractSpecification.g:2997:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= 'is' (otherlv_2= 'valid' | otherlv_3= 'fulfilled' | otherlv_4= 'executed' )
            {
            // InternalSmartContractSpecification.g:2997:3: ( (otherlv_0= RULE_ID ) )
            // InternalSmartContractSpecification.g:2998:4: (otherlv_0= RULE_ID )
            {
            // InternalSmartContractSpecification.g:2998:4: (otherlv_0= RULE_ID )
            // InternalSmartContractSpecification.g:2999:5: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getTermValidTimeQueryRule());
              					}
              				
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_0, grammarAccess.getTermValidTimeQueryAccess().getTermTermCrossReference_0_0());
              				
            }

            }


            }

            otherlv_1=(Token)match(input,62,FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getTermValidTimeQueryAccess().getIsKeyword_1());
              		
            }
            // InternalSmartContractSpecification.g:3014:3: (otherlv_2= 'valid' | otherlv_3= 'fulfilled' | otherlv_4= 'executed' )
            int alt61=3;
            switch ( input.LA(1) ) {
            case 63:
                {
                alt61=1;
                }
                break;
            case 64:
                {
                alt61=2;
                }
                break;
            case 65:
                {
                alt61=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }

            switch (alt61) {
                case 1 :
                    // InternalSmartContractSpecification.g:3015:4: otherlv_2= 'valid'
                    {
                    otherlv_2=(Token)match(input,63,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getTermValidTimeQueryAccess().getValidKeyword_2_0());
                      			
                    }

                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3020:4: otherlv_3= 'fulfilled'
                    {
                    otherlv_3=(Token)match(input,64,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getTermValidTimeQueryAccess().getFulfilledKeyword_2_1());
                      			
                    }

                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:3025:4: otherlv_4= 'executed'
                    {
                    otherlv_4=(Token)match(input,65,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getTermValidTimeQueryAccess().getExecutedKeyword_2_2());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTermValidTimeQuery"


    // $ANTLR start "entryRuleGlobalTimeQuery"
    // InternalSmartContractSpecification.g:3034:1: entryRuleGlobalTimeQuery returns [EObject current=null] : iv_ruleGlobalTimeQuery= ruleGlobalTimeQuery EOF ;
    public final EObject entryRuleGlobalTimeQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGlobalTimeQuery = null;


        try {
            // InternalSmartContractSpecification.g:3034:56: (iv_ruleGlobalTimeQuery= ruleGlobalTimeQuery EOF )
            // InternalSmartContractSpecification.g:3035:2: iv_ruleGlobalTimeQuery= ruleGlobalTimeQuery EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGlobalTimeQueryRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGlobalTimeQuery=ruleGlobalTimeQuery();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGlobalTimeQuery; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGlobalTimeQuery"


    // $ANTLR start "ruleGlobalTimeQuery"
    // InternalSmartContractSpecification.g:3041:1: ruleGlobalTimeQuery returns [EObject current=null] : ( (lv_operator_0_0= ruleTimeQueryOperator ) ) ;
    public final EObject ruleGlobalTimeQuery() throws RecognitionException {
        EObject current = null;

        Enumerator lv_operator_0_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3047:2: ( ( (lv_operator_0_0= ruleTimeQueryOperator ) ) )
            // InternalSmartContractSpecification.g:3048:2: ( (lv_operator_0_0= ruleTimeQueryOperator ) )
            {
            // InternalSmartContractSpecification.g:3048:2: ( (lv_operator_0_0= ruleTimeQueryOperator ) )
            // InternalSmartContractSpecification.g:3049:3: (lv_operator_0_0= ruleTimeQueryOperator )
            {
            // InternalSmartContractSpecification.g:3049:3: (lv_operator_0_0= ruleTimeQueryOperator )
            // InternalSmartContractSpecification.g:3050:4: lv_operator_0_0= ruleTimeQueryOperator
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getGlobalTimeQueryAccess().getOperatorTimeQueryOperatorEnumRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_operator_0_0=ruleTimeQueryOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getGlobalTimeQueryRule());
              				}
              				set(
              					current,
              					"operator",
              					lv_operator_0_0,
              					"edu.ustb.sei.mde.SmartContractSpecification.TimeQueryOperator");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGlobalTimeQuery"


    // $ANTLR start "entryRuleTimeConstant"
    // InternalSmartContractSpecification.g:3070:1: entryRuleTimeConstant returns [EObject current=null] : iv_ruleTimeConstant= ruleTimeConstant EOF ;
    public final EObject entryRuleTimeConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeConstant = null;


        try {
            // InternalSmartContractSpecification.g:3070:53: (iv_ruleTimeConstant= ruleTimeConstant EOF )
            // InternalSmartContractSpecification.g:3071:2: iv_ruleTimeConstant= ruleTimeConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimeConstantRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimeConstant=ruleTimeConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimeConstant; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeConstant"


    // $ANTLR start "ruleTimeConstant"
    // InternalSmartContractSpecification.g:3077:1: ruleTimeConstant returns [EObject current=null] : ( () ( ( (lv_year_1_0= RULE_INT ) ) otherlv_2= 'year' )? ( ( (lv_month_3_0= RULE_INT ) ) otherlv_4= 'month' )? ( ( (lv_day_5_0= RULE_INT ) ) otherlv_6= 'day' )? ( ( (lv_hour_7_0= RULE_INT ) ) otherlv_8= 'hour' )? ( ( (lv_min_9_0= RULE_INT ) ) otherlv_10= 'min' )? ( ( (lv_sec_11_0= RULE_INT ) ) otherlv_12= 'sec' )? ) ;
    public final EObject ruleTimeConstant() throws RecognitionException {
        EObject current = null;

        Token lv_year_1_0=null;
        Token otherlv_2=null;
        Token lv_month_3_0=null;
        Token otherlv_4=null;
        Token lv_day_5_0=null;
        Token otherlv_6=null;
        Token lv_hour_7_0=null;
        Token otherlv_8=null;
        Token lv_min_9_0=null;
        Token otherlv_10=null;
        Token lv_sec_11_0=null;
        Token otherlv_12=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3083:2: ( ( () ( ( (lv_year_1_0= RULE_INT ) ) otherlv_2= 'year' )? ( ( (lv_month_3_0= RULE_INT ) ) otherlv_4= 'month' )? ( ( (lv_day_5_0= RULE_INT ) ) otherlv_6= 'day' )? ( ( (lv_hour_7_0= RULE_INT ) ) otherlv_8= 'hour' )? ( ( (lv_min_9_0= RULE_INT ) ) otherlv_10= 'min' )? ( ( (lv_sec_11_0= RULE_INT ) ) otherlv_12= 'sec' )? ) )
            // InternalSmartContractSpecification.g:3084:2: ( () ( ( (lv_year_1_0= RULE_INT ) ) otherlv_2= 'year' )? ( ( (lv_month_3_0= RULE_INT ) ) otherlv_4= 'month' )? ( ( (lv_day_5_0= RULE_INT ) ) otherlv_6= 'day' )? ( ( (lv_hour_7_0= RULE_INT ) ) otherlv_8= 'hour' )? ( ( (lv_min_9_0= RULE_INT ) ) otherlv_10= 'min' )? ( ( (lv_sec_11_0= RULE_INT ) ) otherlv_12= 'sec' )? )
            {
            // InternalSmartContractSpecification.g:3084:2: ( () ( ( (lv_year_1_0= RULE_INT ) ) otherlv_2= 'year' )? ( ( (lv_month_3_0= RULE_INT ) ) otherlv_4= 'month' )? ( ( (lv_day_5_0= RULE_INT ) ) otherlv_6= 'day' )? ( ( (lv_hour_7_0= RULE_INT ) ) otherlv_8= 'hour' )? ( ( (lv_min_9_0= RULE_INT ) ) otherlv_10= 'min' )? ( ( (lv_sec_11_0= RULE_INT ) ) otherlv_12= 'sec' )? )
            // InternalSmartContractSpecification.g:3085:3: () ( ( (lv_year_1_0= RULE_INT ) ) otherlv_2= 'year' )? ( ( (lv_month_3_0= RULE_INT ) ) otherlv_4= 'month' )? ( ( (lv_day_5_0= RULE_INT ) ) otherlv_6= 'day' )? ( ( (lv_hour_7_0= RULE_INT ) ) otherlv_8= 'hour' )? ( ( (lv_min_9_0= RULE_INT ) ) otherlv_10= 'min' )? ( ( (lv_sec_11_0= RULE_INT ) ) otherlv_12= 'sec' )?
            {
            // InternalSmartContractSpecification.g:3085:3: ()
            // InternalSmartContractSpecification.g:3086:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getTimeConstantAccess().getTimeConstantAction_0(),
              					current);
              			
            }

            }

            // InternalSmartContractSpecification.g:3092:3: ( ( (lv_year_1_0= RULE_INT ) ) otherlv_2= 'year' )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==RULE_INT) ) {
                int LA62_1 = input.LA(2);

                if ( (LA62_1==66) ) {
                    alt62=1;
                }
            }
            switch (alt62) {
                case 1 :
                    // InternalSmartContractSpecification.g:3093:4: ( (lv_year_1_0= RULE_INT ) ) otherlv_2= 'year'
                    {
                    // InternalSmartContractSpecification.g:3093:4: ( (lv_year_1_0= RULE_INT ) )
                    // InternalSmartContractSpecification.g:3094:5: (lv_year_1_0= RULE_INT )
                    {
                    // InternalSmartContractSpecification.g:3094:5: (lv_year_1_0= RULE_INT )
                    // InternalSmartContractSpecification.g:3095:6: lv_year_1_0= RULE_INT
                    {
                    lv_year_1_0=(Token)match(input,RULE_INT,FOLLOW_56); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_year_1_0, grammarAccess.getTimeConstantAccess().getYearINTTerminalRuleCall_1_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTimeConstantRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"year",
                      							lv_year_1_0,
                      							"org.eclipse.xtext.common.Terminals.INT");
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,66,FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getTimeConstantAccess().getYearKeyword_1_1());
                      			
                    }

                    }
                    break;

            }

            // InternalSmartContractSpecification.g:3116:3: ( ( (lv_month_3_0= RULE_INT ) ) otherlv_4= 'month' )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==RULE_INT) ) {
                int LA63_1 = input.LA(2);

                if ( (LA63_1==67) ) {
                    alt63=1;
                }
            }
            switch (alt63) {
                case 1 :
                    // InternalSmartContractSpecification.g:3117:4: ( (lv_month_3_0= RULE_INT ) ) otherlv_4= 'month'
                    {
                    // InternalSmartContractSpecification.g:3117:4: ( (lv_month_3_0= RULE_INT ) )
                    // InternalSmartContractSpecification.g:3118:5: (lv_month_3_0= RULE_INT )
                    {
                    // InternalSmartContractSpecification.g:3118:5: (lv_month_3_0= RULE_INT )
                    // InternalSmartContractSpecification.g:3119:6: lv_month_3_0= RULE_INT
                    {
                    lv_month_3_0=(Token)match(input,RULE_INT,FOLLOW_58); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_month_3_0, grammarAccess.getTimeConstantAccess().getMonthINTTerminalRuleCall_2_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTimeConstantRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"month",
                      							lv_month_3_0,
                      							"org.eclipse.xtext.common.Terminals.INT");
                      					
                    }

                    }


                    }

                    otherlv_4=(Token)match(input,67,FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getTimeConstantAccess().getMonthKeyword_2_1());
                      			
                    }

                    }
                    break;

            }

            // InternalSmartContractSpecification.g:3140:3: ( ( (lv_day_5_0= RULE_INT ) ) otherlv_6= 'day' )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==RULE_INT) ) {
                int LA64_1 = input.LA(2);

                if ( (LA64_1==68) ) {
                    alt64=1;
                }
            }
            switch (alt64) {
                case 1 :
                    // InternalSmartContractSpecification.g:3141:4: ( (lv_day_5_0= RULE_INT ) ) otherlv_6= 'day'
                    {
                    // InternalSmartContractSpecification.g:3141:4: ( (lv_day_5_0= RULE_INT ) )
                    // InternalSmartContractSpecification.g:3142:5: (lv_day_5_0= RULE_INT )
                    {
                    // InternalSmartContractSpecification.g:3142:5: (lv_day_5_0= RULE_INT )
                    // InternalSmartContractSpecification.g:3143:6: lv_day_5_0= RULE_INT
                    {
                    lv_day_5_0=(Token)match(input,RULE_INT,FOLLOW_59); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_day_5_0, grammarAccess.getTimeConstantAccess().getDayINTTerminalRuleCall_3_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTimeConstantRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"day",
                      							lv_day_5_0,
                      							"org.eclipse.xtext.common.Terminals.INT");
                      					
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,68,FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getTimeConstantAccess().getDayKeyword_3_1());
                      			
                    }

                    }
                    break;

            }

            // InternalSmartContractSpecification.g:3164:3: ( ( (lv_hour_7_0= RULE_INT ) ) otherlv_8= 'hour' )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==RULE_INT) ) {
                int LA65_1 = input.LA(2);

                if ( (LA65_1==69) ) {
                    alt65=1;
                }
            }
            switch (alt65) {
                case 1 :
                    // InternalSmartContractSpecification.g:3165:4: ( (lv_hour_7_0= RULE_INT ) ) otherlv_8= 'hour'
                    {
                    // InternalSmartContractSpecification.g:3165:4: ( (lv_hour_7_0= RULE_INT ) )
                    // InternalSmartContractSpecification.g:3166:5: (lv_hour_7_0= RULE_INT )
                    {
                    // InternalSmartContractSpecification.g:3166:5: (lv_hour_7_0= RULE_INT )
                    // InternalSmartContractSpecification.g:3167:6: lv_hour_7_0= RULE_INT
                    {
                    lv_hour_7_0=(Token)match(input,RULE_INT,FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_hour_7_0, grammarAccess.getTimeConstantAccess().getHourINTTerminalRuleCall_4_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTimeConstantRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"hour",
                      							lv_hour_7_0,
                      							"org.eclipse.xtext.common.Terminals.INT");
                      					
                    }

                    }


                    }

                    otherlv_8=(Token)match(input,69,FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getTimeConstantAccess().getHourKeyword_4_1());
                      			
                    }

                    }
                    break;

            }

            // InternalSmartContractSpecification.g:3188:3: ( ( (lv_min_9_0= RULE_INT ) ) otherlv_10= 'min' )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==RULE_INT) ) {
                int LA66_1 = input.LA(2);

                if ( (LA66_1==70) ) {
                    alt66=1;
                }
            }
            switch (alt66) {
                case 1 :
                    // InternalSmartContractSpecification.g:3189:4: ( (lv_min_9_0= RULE_INT ) ) otherlv_10= 'min'
                    {
                    // InternalSmartContractSpecification.g:3189:4: ( (lv_min_9_0= RULE_INT ) )
                    // InternalSmartContractSpecification.g:3190:5: (lv_min_9_0= RULE_INT )
                    {
                    // InternalSmartContractSpecification.g:3190:5: (lv_min_9_0= RULE_INT )
                    // InternalSmartContractSpecification.g:3191:6: lv_min_9_0= RULE_INT
                    {
                    lv_min_9_0=(Token)match(input,RULE_INT,FOLLOW_61); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_min_9_0, grammarAccess.getTimeConstantAccess().getMinINTTerminalRuleCall_5_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTimeConstantRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"min",
                      							lv_min_9_0,
                      							"org.eclipse.xtext.common.Terminals.INT");
                      					
                    }

                    }


                    }

                    otherlv_10=(Token)match(input,70,FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getTimeConstantAccess().getMinKeyword_5_1());
                      			
                    }

                    }
                    break;

            }

            // InternalSmartContractSpecification.g:3212:3: ( ( (lv_sec_11_0= RULE_INT ) ) otherlv_12= 'sec' )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==RULE_INT) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalSmartContractSpecification.g:3213:4: ( (lv_sec_11_0= RULE_INT ) ) otherlv_12= 'sec'
                    {
                    // InternalSmartContractSpecification.g:3213:4: ( (lv_sec_11_0= RULE_INT ) )
                    // InternalSmartContractSpecification.g:3214:5: (lv_sec_11_0= RULE_INT )
                    {
                    // InternalSmartContractSpecification.g:3214:5: (lv_sec_11_0= RULE_INT )
                    // InternalSmartContractSpecification.g:3215:6: lv_sec_11_0= RULE_INT
                    {
                    lv_sec_11_0=(Token)match(input,RULE_INT,FOLLOW_62); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_sec_11_0, grammarAccess.getTimeConstantAccess().getSecINTTerminalRuleCall_6_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTimeConstantRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"sec",
                      							lv_sec_11_0,
                      							"org.eclipse.xtext.common.Terminals.INT");
                      					
                    }

                    }


                    }

                    otherlv_12=(Token)match(input,71,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_12, grammarAccess.getTimeConstantAccess().getSecKeyword_6_1());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeConstant"


    // $ANTLR start "entryRuleTimeVarExpression"
    // InternalSmartContractSpecification.g:3240:1: entryRuleTimeVarExpression returns [EObject current=null] : iv_ruleTimeVarExpression= ruleTimeVarExpression EOF ;
    public final EObject entryRuleTimeVarExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeVarExpression = null;


        try {
            // InternalSmartContractSpecification.g:3240:58: (iv_ruleTimeVarExpression= ruleTimeVarExpression EOF )
            // InternalSmartContractSpecification.g:3241:2: iv_ruleTimeVarExpression= ruleTimeVarExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimeVarExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimeVarExpression=ruleTimeVarExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimeVarExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeVarExpression"


    // $ANTLR start "ruleTimeVarExpression"
    // InternalSmartContractSpecification.g:3247:1: ruleTimeVarExpression returns [EObject current=null] : (otherlv_0= '@' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '::' ( (lv_next_3_0= ruleSlotRef ) ) )? ) ;
    public final EObject ruleTimeVarExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_next_3_0 = null;



        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3253:2: ( (otherlv_0= '@' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '::' ( (lv_next_3_0= ruleSlotRef ) ) )? ) )
            // InternalSmartContractSpecification.g:3254:2: (otherlv_0= '@' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '::' ( (lv_next_3_0= ruleSlotRef ) ) )? )
            {
            // InternalSmartContractSpecification.g:3254:2: (otherlv_0= '@' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '::' ( (lv_next_3_0= ruleSlotRef ) ) )? )
            // InternalSmartContractSpecification.g:3255:3: otherlv_0= '@' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '::' ( (lv_next_3_0= ruleSlotRef ) ) )?
            {
            otherlv_0=(Token)match(input,72,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getTimeVarExpressionAccess().getCommercialAtKeyword_0());
              		
            }
            // InternalSmartContractSpecification.g:3259:3: ( (otherlv_1= RULE_ID ) )
            // InternalSmartContractSpecification.g:3260:4: (otherlv_1= RULE_ID )
            {
            // InternalSmartContractSpecification.g:3260:4: (otherlv_1= RULE_ID )
            // InternalSmartContractSpecification.g:3261:5: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getTimeVarExpressionRule());
              					}
              				
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_1, grammarAccess.getTimeVarExpressionAccess().getSlotDataSlotCrossReference_1_0());
              				
            }

            }


            }

            // InternalSmartContractSpecification.g:3272:3: (otherlv_2= '::' ( (lv_next_3_0= ruleSlotRef ) ) )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==56) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalSmartContractSpecification.g:3273:4: otherlv_2= '::' ( (lv_next_3_0= ruleSlotRef ) )
                    {
                    otherlv_2=(Token)match(input,56,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getTimeVarExpressionAccess().getColonColonKeyword_2_0());
                      			
                    }
                    // InternalSmartContractSpecification.g:3277:4: ( (lv_next_3_0= ruleSlotRef ) )
                    // InternalSmartContractSpecification.g:3278:5: (lv_next_3_0= ruleSlotRef )
                    {
                    // InternalSmartContractSpecification.g:3278:5: (lv_next_3_0= ruleSlotRef )
                    // InternalSmartContractSpecification.g:3279:6: lv_next_3_0= ruleSlotRef
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTimeVarExpressionAccess().getNextSlotRefParserRuleCall_2_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_next_3_0=ruleSlotRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTimeVarExpressionRule());
                      						}
                      						set(
                      							current,
                      							"next",
                      							lv_next_3_0,
                      							"edu.ustb.sei.mde.SmartContractSpecification.SlotRef");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeVarExpression"


    // $ANTLR start "ruleQuantifier"
    // InternalSmartContractSpecification.g:3301:1: ruleQuantifier returns [Enumerator current=null] : ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'forAll' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'exists' ) ) ;
    public final Enumerator ruleQuantifier() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3307:2: ( ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'forAll' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'exists' ) ) )
            // InternalSmartContractSpecification.g:3308:2: ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'forAll' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'exists' ) )
            {
            // InternalSmartContractSpecification.g:3308:2: ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'forAll' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'exists' ) )
            int alt69=4;
            switch ( input.LA(1) ) {
            case 73:
                {
                alt69=1;
                }
                break;
            case 74:
                {
                alt69=2;
                }
                break;
            case 75:
                {
                alt69=3;
                }
                break;
            case 76:
                {
                alt69=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 69, 0, input);

                throw nvae;
            }

            switch (alt69) {
                case 1 :
                    // InternalSmartContractSpecification.g:3309:3: (enumLiteral_0= 'all' )
                    {
                    // InternalSmartContractSpecification.g:3309:3: (enumLiteral_0= 'all' )
                    // InternalSmartContractSpecification.g:3310:4: enumLiteral_0= 'all'
                    {
                    enumLiteral_0=(Token)match(input,73,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getQuantifierAccess().getForAllEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getQuantifierAccess().getForAllEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3317:3: (enumLiteral_1= 'forAll' )
                    {
                    // InternalSmartContractSpecification.g:3317:3: (enumLiteral_1= 'forAll' )
                    // InternalSmartContractSpecification.g:3318:4: enumLiteral_1= 'forAll'
                    {
                    enumLiteral_1=(Token)match(input,74,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getQuantifierAccess().getForAllEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getQuantifierAccess().getForAllEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:3325:3: (enumLiteral_2= 'some' )
                    {
                    // InternalSmartContractSpecification.g:3325:3: (enumLiteral_2= 'some' )
                    // InternalSmartContractSpecification.g:3326:4: enumLiteral_2= 'some'
                    {
                    enumLiteral_2=(Token)match(input,75,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getQuantifierAccess().getExistsEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getQuantifierAccess().getExistsEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSmartContractSpecification.g:3333:3: (enumLiteral_3= 'exists' )
                    {
                    // InternalSmartContractSpecification.g:3333:3: (enumLiteral_3= 'exists' )
                    // InternalSmartContractSpecification.g:3334:4: enumLiteral_3= 'exists'
                    {
                    enumLiteral_3=(Token)match(input,76,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getQuantifierAccess().getExistsEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_3, grammarAccess.getQuantifierAccess().getExistsEnumLiteralDeclaration_3());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifier"


    // $ANTLR start "ruleRelationOperator"
    // InternalSmartContractSpecification.g:3344:1: ruleRelationOperator returns [Enumerator current=null] : ( (enumLiteral_0= '=' ) | (enumLiteral_1= 'is equal to' ) | (enumLiteral_2= '<' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '>' ) | (enumLiteral_5= '>=' ) | (enumLiteral_6= '!=' ) | (enumLiteral_7= '<>' ) | (enumLiteral_8= 'is not equal to' ) | (enumLiteral_9= 'belongs to' ) | (enumLiteral_10= 'is a subset of' ) ) ;
    public final Enumerator ruleRelationOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3350:2: ( ( (enumLiteral_0= '=' ) | (enumLiteral_1= 'is equal to' ) | (enumLiteral_2= '<' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '>' ) | (enumLiteral_5= '>=' ) | (enumLiteral_6= '!=' ) | (enumLiteral_7= '<>' ) | (enumLiteral_8= 'is not equal to' ) | (enumLiteral_9= 'belongs to' ) | (enumLiteral_10= 'is a subset of' ) ) )
            // InternalSmartContractSpecification.g:3351:2: ( (enumLiteral_0= '=' ) | (enumLiteral_1= 'is equal to' ) | (enumLiteral_2= '<' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '>' ) | (enumLiteral_5= '>=' ) | (enumLiteral_6= '!=' ) | (enumLiteral_7= '<>' ) | (enumLiteral_8= 'is not equal to' ) | (enumLiteral_9= 'belongs to' ) | (enumLiteral_10= 'is a subset of' ) )
            {
            // InternalSmartContractSpecification.g:3351:2: ( (enumLiteral_0= '=' ) | (enumLiteral_1= 'is equal to' ) | (enumLiteral_2= '<' ) | (enumLiteral_3= '<=' ) | (enumLiteral_4= '>' ) | (enumLiteral_5= '>=' ) | (enumLiteral_6= '!=' ) | (enumLiteral_7= '<>' ) | (enumLiteral_8= 'is not equal to' ) | (enumLiteral_9= 'belongs to' ) | (enumLiteral_10= 'is a subset of' ) )
            int alt70=11;
            switch ( input.LA(1) ) {
            case 77:
                {
                alt70=1;
                }
                break;
            case 78:
                {
                alt70=2;
                }
                break;
            case 79:
                {
                alt70=3;
                }
                break;
            case 80:
                {
                alt70=4;
                }
                break;
            case 81:
                {
                alt70=5;
                }
                break;
            case 82:
                {
                alt70=6;
                }
                break;
            case 83:
                {
                alt70=7;
                }
                break;
            case 84:
                {
                alt70=8;
                }
                break;
            case 85:
                {
                alt70=9;
                }
                break;
            case 86:
                {
                alt70=10;
                }
                break;
            case 87:
                {
                alt70=11;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 70, 0, input);

                throw nvae;
            }

            switch (alt70) {
                case 1 :
                    // InternalSmartContractSpecification.g:3352:3: (enumLiteral_0= '=' )
                    {
                    // InternalSmartContractSpecification.g:3352:3: (enumLiteral_0= '=' )
                    // InternalSmartContractSpecification.g:3353:4: enumLiteral_0= '='
                    {
                    enumLiteral_0=(Token)match(input,77,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getEqualEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getRelationOperatorAccess().getEqualEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3360:3: (enumLiteral_1= 'is equal to' )
                    {
                    // InternalSmartContractSpecification.g:3360:3: (enumLiteral_1= 'is equal to' )
                    // InternalSmartContractSpecification.g:3361:4: enumLiteral_1= 'is equal to'
                    {
                    enumLiteral_1=(Token)match(input,78,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getEqualEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getRelationOperatorAccess().getEqualEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:3368:3: (enumLiteral_2= '<' )
                    {
                    // InternalSmartContractSpecification.g:3368:3: (enumLiteral_2= '<' )
                    // InternalSmartContractSpecification.g:3369:4: enumLiteral_2= '<'
                    {
                    enumLiteral_2=(Token)match(input,79,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getLessEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getRelationOperatorAccess().getLessEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSmartContractSpecification.g:3376:3: (enumLiteral_3= '<=' )
                    {
                    // InternalSmartContractSpecification.g:3376:3: (enumLiteral_3= '<=' )
                    // InternalSmartContractSpecification.g:3377:4: enumLiteral_3= '<='
                    {
                    enumLiteral_3=(Token)match(input,80,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getLessOrEqualEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_3, grammarAccess.getRelationOperatorAccess().getLessOrEqualEnumLiteralDeclaration_3());
                      			
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalSmartContractSpecification.g:3384:3: (enumLiteral_4= '>' )
                    {
                    // InternalSmartContractSpecification.g:3384:3: (enumLiteral_4= '>' )
                    // InternalSmartContractSpecification.g:3385:4: enumLiteral_4= '>'
                    {
                    enumLiteral_4=(Token)match(input,81,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getGreaterEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_4, grammarAccess.getRelationOperatorAccess().getGreaterEnumLiteralDeclaration_4());
                      			
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalSmartContractSpecification.g:3392:3: (enumLiteral_5= '>=' )
                    {
                    // InternalSmartContractSpecification.g:3392:3: (enumLiteral_5= '>=' )
                    // InternalSmartContractSpecification.g:3393:4: enumLiteral_5= '>='
                    {
                    enumLiteral_5=(Token)match(input,82,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getGreaterOrEqualEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_5, grammarAccess.getRelationOperatorAccess().getGreaterOrEqualEnumLiteralDeclaration_5());
                      			
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalSmartContractSpecification.g:3400:3: (enumLiteral_6= '!=' )
                    {
                    // InternalSmartContractSpecification.g:3400:3: (enumLiteral_6= '!=' )
                    // InternalSmartContractSpecification.g:3401:4: enumLiteral_6= '!='
                    {
                    enumLiteral_6=(Token)match(input,83,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getNotEqualEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_6, grammarAccess.getRelationOperatorAccess().getNotEqualEnumLiteralDeclaration_6());
                      			
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalSmartContractSpecification.g:3408:3: (enumLiteral_7= '<>' )
                    {
                    // InternalSmartContractSpecification.g:3408:3: (enumLiteral_7= '<>' )
                    // InternalSmartContractSpecification.g:3409:4: enumLiteral_7= '<>'
                    {
                    enumLiteral_7=(Token)match(input,84,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getNotEqualEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_7, grammarAccess.getRelationOperatorAccess().getNotEqualEnumLiteralDeclaration_7());
                      			
                    }

                    }


                    }
                    break;
                case 9 :
                    // InternalSmartContractSpecification.g:3416:3: (enumLiteral_8= 'is not equal to' )
                    {
                    // InternalSmartContractSpecification.g:3416:3: (enumLiteral_8= 'is not equal to' )
                    // InternalSmartContractSpecification.g:3417:4: enumLiteral_8= 'is not equal to'
                    {
                    enumLiteral_8=(Token)match(input,85,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getNotEqualEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_8, grammarAccess.getRelationOperatorAccess().getNotEqualEnumLiteralDeclaration_8());
                      			
                    }

                    }


                    }
                    break;
                case 10 :
                    // InternalSmartContractSpecification.g:3424:3: (enumLiteral_9= 'belongs to' )
                    {
                    // InternalSmartContractSpecification.g:3424:3: (enumLiteral_9= 'belongs to' )
                    // InternalSmartContractSpecification.g:3425:4: enumLiteral_9= 'belongs to'
                    {
                    enumLiteral_9=(Token)match(input,86,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getInEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_9, grammarAccess.getRelationOperatorAccess().getInEnumLiteralDeclaration_9());
                      			
                    }

                    }


                    }
                    break;
                case 11 :
                    // InternalSmartContractSpecification.g:3432:3: (enumLiteral_10= 'is a subset of' )
                    {
                    // InternalSmartContractSpecification.g:3432:3: (enumLiteral_10= 'is a subset of' )
                    // InternalSmartContractSpecification.g:3433:4: enumLiteral_10= 'is a subset of'
                    {
                    enumLiteral_10=(Token)match(input,87,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getRelationOperatorAccess().getSubsetEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_10, grammarAccess.getRelationOperatorAccess().getSubsetEnumLiteralDeclaration_10());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationOperator"


    // $ANTLR start "ruleAdditiveOperator"
    // InternalSmartContractSpecification.g:3443:1: ruleAdditiveOperator returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) ;
    public final Enumerator ruleAdditiveOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3449:2: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) )
            // InternalSmartContractSpecification.g:3450:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            {
            // InternalSmartContractSpecification.g:3450:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( (LA71_0==88) ) {
                alt71=1;
            }
            else if ( (LA71_0==57) ) {
                alt71=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 71, 0, input);

                throw nvae;
            }
            switch (alt71) {
                case 1 :
                    // InternalSmartContractSpecification.g:3451:3: (enumLiteral_0= '+' )
                    {
                    // InternalSmartContractSpecification.g:3451:3: (enumLiteral_0= '+' )
                    // InternalSmartContractSpecification.g:3452:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,88,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getAdditiveOperatorAccess().getAddEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getAdditiveOperatorAccess().getAddEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3459:3: (enumLiteral_1= '-' )
                    {
                    // InternalSmartContractSpecification.g:3459:3: (enumLiteral_1= '-' )
                    // InternalSmartContractSpecification.g:3460:4: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,57,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getAdditiveOperatorAccess().getSubEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getAdditiveOperatorAccess().getSubEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditiveOperator"


    // $ANTLR start "ruleMultiplicativeOperator"
    // InternalSmartContractSpecification.g:3470:1: ruleMultiplicativeOperator returns [Enumerator current=null] : ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) ) ;
    public final Enumerator ruleMultiplicativeOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3476:2: ( ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) ) )
            // InternalSmartContractSpecification.g:3477:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) )
            {
            // InternalSmartContractSpecification.g:3477:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) )
            int alt72=2;
            int LA72_0 = input.LA(1);

            if ( (LA72_0==89) ) {
                alt72=1;
            }
            else if ( (LA72_0==90) ) {
                alt72=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 72, 0, input);

                throw nvae;
            }
            switch (alt72) {
                case 1 :
                    // InternalSmartContractSpecification.g:3478:3: (enumLiteral_0= '*' )
                    {
                    // InternalSmartContractSpecification.g:3478:3: (enumLiteral_0= '*' )
                    // InternalSmartContractSpecification.g:3479:4: enumLiteral_0= '*'
                    {
                    enumLiteral_0=(Token)match(input,89,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getMultiplicativeOperatorAccess().getMulEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getMultiplicativeOperatorAccess().getMulEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3486:3: (enumLiteral_1= '/' )
                    {
                    // InternalSmartContractSpecification.g:3486:3: (enumLiteral_1= '/' )
                    // InternalSmartContractSpecification.g:3487:4: enumLiteral_1= '/'
                    {
                    enumLiteral_1=(Token)match(input,90,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getMultiplicativeOperatorAccess().getDivEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getMultiplicativeOperatorAccess().getDivEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicativeOperator"


    // $ANTLR start "ruleTransitiveOperator"
    // InternalSmartContractSpecification.g:3497:1: ruleTransitiveOperator returns [Enumerator current=null] : ( (enumLiteral_0= '^' ) | (enumLiteral_1= '*' ) ) ;
    public final Enumerator ruleTransitiveOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3503:2: ( ( (enumLiteral_0= '^' ) | (enumLiteral_1= '*' ) ) )
            // InternalSmartContractSpecification.g:3504:2: ( (enumLiteral_0= '^' ) | (enumLiteral_1= '*' ) )
            {
            // InternalSmartContractSpecification.g:3504:2: ( (enumLiteral_0= '^' ) | (enumLiteral_1= '*' ) )
            int alt73=2;
            int LA73_0 = input.LA(1);

            if ( (LA73_0==91) ) {
                alt73=1;
            }
            else if ( (LA73_0==89) ) {
                alt73=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 73, 0, input);

                throw nvae;
            }
            switch (alt73) {
                case 1 :
                    // InternalSmartContractSpecification.g:3505:3: (enumLiteral_0= '^' )
                    {
                    // InternalSmartContractSpecification.g:3505:3: (enumLiteral_0= '^' )
                    // InternalSmartContractSpecification.g:3506:4: enumLiteral_0= '^'
                    {
                    enumLiteral_0=(Token)match(input,91,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getTransitiveOperatorAccess().getTransitiveEnclosureEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getTransitiveOperatorAccess().getTransitiveEnclosureEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3513:3: (enumLiteral_1= '*' )
                    {
                    // InternalSmartContractSpecification.g:3513:3: (enumLiteral_1= '*' )
                    // InternalSmartContractSpecification.g:3514:4: enumLiteral_1= '*'
                    {
                    enumLiteral_1=(Token)match(input,89,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getTransitiveOperatorAccess().getSelfTransitiveEnclosureEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getTransitiveOperatorAccess().getSelfTransitiveEnclosureEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransitiveOperator"


    // $ANTLR start "ruleTimePredicateOperator"
    // InternalSmartContractSpecification.g:3524:1: ruleTimePredicateOperator returns [Enumerator current=null] : ( (enumLiteral_0= 'before' ) | (enumLiteral_1= 'after' ) ) ;
    public final Enumerator ruleTimePredicateOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3530:2: ( ( (enumLiteral_0= 'before' ) | (enumLiteral_1= 'after' ) ) )
            // InternalSmartContractSpecification.g:3531:2: ( (enumLiteral_0= 'before' ) | (enumLiteral_1= 'after' ) )
            {
            // InternalSmartContractSpecification.g:3531:2: ( (enumLiteral_0= 'before' ) | (enumLiteral_1= 'after' ) )
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==92) ) {
                alt74=1;
            }
            else if ( (LA74_0==93) ) {
                alt74=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }
            switch (alt74) {
                case 1 :
                    // InternalSmartContractSpecification.g:3532:3: (enumLiteral_0= 'before' )
                    {
                    // InternalSmartContractSpecification.g:3532:3: (enumLiteral_0= 'before' )
                    // InternalSmartContractSpecification.g:3533:4: enumLiteral_0= 'before'
                    {
                    enumLiteral_0=(Token)match(input,92,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getTimePredicateOperatorAccess().getBeforeEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getTimePredicateOperatorAccess().getBeforeEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3540:3: (enumLiteral_1= 'after' )
                    {
                    // InternalSmartContractSpecification.g:3540:3: (enumLiteral_1= 'after' )
                    // InternalSmartContractSpecification.g:3541:4: enumLiteral_1= 'after'
                    {
                    enumLiteral_1=(Token)match(input,93,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getTimePredicateOperatorAccess().getAfterEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getTimePredicateOperatorAccess().getAfterEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimePredicateOperator"


    // $ANTLR start "ruleActionMultiplicity"
    // InternalSmartContractSpecification.g:3551:1: ruleActionMultiplicity returns [Enumerator current=null] : ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'any' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'one' ) | (enumLiteral_4= 'this' ) | (enumLiteral_5= 'the' ) ) ;
    public final Enumerator ruleActionMultiplicity() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3557:2: ( ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'any' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'one' ) | (enumLiteral_4= 'this' ) | (enumLiteral_5= 'the' ) ) )
            // InternalSmartContractSpecification.g:3558:2: ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'any' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'one' ) | (enumLiteral_4= 'this' ) | (enumLiteral_5= 'the' ) )
            {
            // InternalSmartContractSpecification.g:3558:2: ( (enumLiteral_0= 'all' ) | (enumLiteral_1= 'any' ) | (enumLiteral_2= 'some' ) | (enumLiteral_3= 'one' ) | (enumLiteral_4= 'this' ) | (enumLiteral_5= 'the' ) )
            int alt75=6;
            switch ( input.LA(1) ) {
            case 73:
                {
                alt75=1;
                }
                break;
            case 94:
                {
                alt75=2;
                }
                break;
            case 75:
                {
                alt75=3;
                }
                break;
            case 95:
                {
                alt75=4;
                }
                break;
            case 49:
                {
                alt75=5;
                }
                break;
            case 96:
                {
                alt75=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 75, 0, input);

                throw nvae;
            }

            switch (alt75) {
                case 1 :
                    // InternalSmartContractSpecification.g:3559:3: (enumLiteral_0= 'all' )
                    {
                    // InternalSmartContractSpecification.g:3559:3: (enumLiteral_0= 'all' )
                    // InternalSmartContractSpecification.g:3560:4: enumLiteral_0= 'all'
                    {
                    enumLiteral_0=(Token)match(input,73,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getActionMultiplicityAccess().getAllEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getActionMultiplicityAccess().getAllEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3567:3: (enumLiteral_1= 'any' )
                    {
                    // InternalSmartContractSpecification.g:3567:3: (enumLiteral_1= 'any' )
                    // InternalSmartContractSpecification.g:3568:4: enumLiteral_1= 'any'
                    {
                    enumLiteral_1=(Token)match(input,94,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getActionMultiplicityAccess().getAnyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getActionMultiplicityAccess().getAnyEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:3575:3: (enumLiteral_2= 'some' )
                    {
                    // InternalSmartContractSpecification.g:3575:3: (enumLiteral_2= 'some' )
                    // InternalSmartContractSpecification.g:3576:4: enumLiteral_2= 'some'
                    {
                    enumLiteral_2=(Token)match(input,75,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getActionMultiplicityAccess().getAnyEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getActionMultiplicityAccess().getAnyEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSmartContractSpecification.g:3583:3: (enumLiteral_3= 'one' )
                    {
                    // InternalSmartContractSpecification.g:3583:3: (enumLiteral_3= 'one' )
                    // InternalSmartContractSpecification.g:3584:4: enumLiteral_3= 'one'
                    {
                    enumLiteral_3=(Token)match(input,95,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getActionMultiplicityAccess().getAnyEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_3, grammarAccess.getActionMultiplicityAccess().getAnyEnumLiteralDeclaration_3());
                      			
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalSmartContractSpecification.g:3591:3: (enumLiteral_4= 'this' )
                    {
                    // InternalSmartContractSpecification.g:3591:3: (enumLiteral_4= 'this' )
                    // InternalSmartContractSpecification.g:3592:4: enumLiteral_4= 'this'
                    {
                    enumLiteral_4=(Token)match(input,49,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getActionMultiplicityAccess().getThisEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_4, grammarAccess.getActionMultiplicityAccess().getThisEnumLiteralDeclaration_4());
                      			
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalSmartContractSpecification.g:3599:3: (enumLiteral_5= 'the' )
                    {
                    // InternalSmartContractSpecification.g:3599:3: (enumLiteral_5= 'the' )
                    // InternalSmartContractSpecification.g:3600:4: enumLiteral_5= 'the'
                    {
                    enumLiteral_5=(Token)match(input,96,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getActionMultiplicityAccess().getThisEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_5, grammarAccess.getActionMultiplicityAccess().getThisEnumLiteralDeclaration_5());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionMultiplicity"


    // $ANTLR start "ruleTimeQueryOperator"
    // InternalSmartContractSpecification.g:3610:1: ruleTimeQueryOperator returns [Enumerator current=null] : ( (enumLiteral_0= 'now' ) | (enumLiteral_1= 'start' ) | (enumLiteral_2= 'end' ) ) ;
    public final Enumerator ruleTimeQueryOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalSmartContractSpecification.g:3616:2: ( ( (enumLiteral_0= 'now' ) | (enumLiteral_1= 'start' ) | (enumLiteral_2= 'end' ) ) )
            // InternalSmartContractSpecification.g:3617:2: ( (enumLiteral_0= 'now' ) | (enumLiteral_1= 'start' ) | (enumLiteral_2= 'end' ) )
            {
            // InternalSmartContractSpecification.g:3617:2: ( (enumLiteral_0= 'now' ) | (enumLiteral_1= 'start' ) | (enumLiteral_2= 'end' ) )
            int alt76=3;
            switch ( input.LA(1) ) {
            case 97:
                {
                alt76=1;
                }
                break;
            case 98:
                {
                alt76=2;
                }
                break;
            case 99:
                {
                alt76=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 76, 0, input);

                throw nvae;
            }

            switch (alt76) {
                case 1 :
                    // InternalSmartContractSpecification.g:3618:3: (enumLiteral_0= 'now' )
                    {
                    // InternalSmartContractSpecification.g:3618:3: (enumLiteral_0= 'now' )
                    // InternalSmartContractSpecification.g:3619:4: enumLiteral_0= 'now'
                    {
                    enumLiteral_0=(Token)match(input,97,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getTimeQueryOperatorAccess().getNowEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getTimeQueryOperatorAccess().getNowEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSmartContractSpecification.g:3626:3: (enumLiteral_1= 'start' )
                    {
                    // InternalSmartContractSpecification.g:3626:3: (enumLiteral_1= 'start' )
                    // InternalSmartContractSpecification.g:3627:4: enumLiteral_1= 'start'
                    {
                    enumLiteral_1=(Token)match(input,98,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getTimeQueryOperatorAccess().getContractStartEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getTimeQueryOperatorAccess().getContractStartEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSmartContractSpecification.g:3634:3: (enumLiteral_2= 'end' )
                    {
                    // InternalSmartContractSpecification.g:3634:3: (enumLiteral_2= 'end' )
                    // InternalSmartContractSpecification.g:3635:4: enumLiteral_2= 'end'
                    {
                    enumLiteral_2=(Token)match(input,99,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getTimeQueryOperatorAccess().getContractEndEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getTimeQueryOperatorAccess().getContractEndEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeQueryOperator"

    // $ANTLR start synpred1_InternalSmartContractSpecification
    public final void synpred1_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:1273:4: ( ruleImplyExpression )
        // InternalSmartContractSpecification.g:1273:5: ruleImplyExpression
        {
        pushFollow(FOLLOW_2);
        ruleImplyExpression();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalSmartContractSpecification

    // $ANTLR start synpred2_InternalSmartContractSpecification
    public final void synpred2_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:1347:5: ( ( () 'where' ) )
        // InternalSmartContractSpecification.g:1347:6: ( () 'where' )
        {
        // InternalSmartContractSpecification.g:1347:6: ( () 'where' )
        // InternalSmartContractSpecification.g:1348:6: () 'where'
        {
        // InternalSmartContractSpecification.g:1348:6: ()
        // InternalSmartContractSpecification.g:1349:6: 
        {
        }

        match(input,30,FOLLOW_2); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred2_InternalSmartContractSpecification

    // $ANTLR start synpred3_InternalSmartContractSpecification
    public final void synpred3_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:1646:5: ( ( () 'implies' ) )
        // InternalSmartContractSpecification.g:1646:6: ( () 'implies' )
        {
        // InternalSmartContractSpecification.g:1646:6: ( () 'implies' )
        // InternalSmartContractSpecification.g:1647:6: () 'implies'
        {
        // InternalSmartContractSpecification.g:1647:6: ()
        // InternalSmartContractSpecification.g:1648:6: 
        {
        }

        match(input,43,FOLLOW_2); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred3_InternalSmartContractSpecification

    // $ANTLR start synpred4_InternalSmartContractSpecification
    public final void synpred4_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:1715:5: ( ( () ( 'or' | '||' ) ) )
        // InternalSmartContractSpecification.g:1715:6: ( () ( 'or' | '||' ) )
        {
        // InternalSmartContractSpecification.g:1715:6: ( () ( 'or' | '||' ) )
        // InternalSmartContractSpecification.g:1716:6: () ( 'or' | '||' )
        {
        // InternalSmartContractSpecification.g:1716:6: ()
        // InternalSmartContractSpecification.g:1717:6: 
        {
        }

        if ( (input.LA(1)>=44 && input.LA(1)<=45) ) {
            input.consume();
            state.errorRecovery=false;state.failed=false;
        }
        else {
            if (state.backtracking>0) {state.failed=true; return ;}
            MismatchedSetException mse = new MismatchedSetException(null,input);
            throw mse;
        }


        }


        }
    }
    // $ANTLR end synpred4_InternalSmartContractSpecification

    // $ANTLR start synpred5_InternalSmartContractSpecification
    public final void synpred5_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:1795:5: ( ( () ( 'and' | '&&' ) ) )
        // InternalSmartContractSpecification.g:1795:6: ( () ( 'and' | '&&' ) )
        {
        // InternalSmartContractSpecification.g:1795:6: ( () ( 'and' | '&&' ) )
        // InternalSmartContractSpecification.g:1796:6: () ( 'and' | '&&' )
        {
        // InternalSmartContractSpecification.g:1796:6: ()
        // InternalSmartContractSpecification.g:1797:6: 
        {
        }

        if ( (input.LA(1)>=46 && input.LA(1)<=47) ) {
            input.consume();
            state.errorRecovery=false;state.failed=false;
        }
        else {
            if (state.backtracking>0) {state.failed=true; return ;}
            MismatchedSetException mse = new MismatchedSetException(null,input);
            throw mse;
        }


        }


        }
    }
    // $ANTLR end synpred5_InternalSmartContractSpecification

    // $ANTLR start synpred6_InternalSmartContractSpecification
    public final void synpred6_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:1953:5: ( ( () ( ( ruleRelationOperator ) ) ) )
        // InternalSmartContractSpecification.g:1953:6: ( () ( ( ruleRelationOperator ) ) )
        {
        // InternalSmartContractSpecification.g:1953:6: ( () ( ( ruleRelationOperator ) ) )
        // InternalSmartContractSpecification.g:1954:6: () ( ( ruleRelationOperator ) )
        {
        // InternalSmartContractSpecification.g:1954:6: ()
        // InternalSmartContractSpecification.g:1955:6: 
        {
        }

        // InternalSmartContractSpecification.g:1956:6: ( ( ruleRelationOperator ) )
        // InternalSmartContractSpecification.g:1957:7: ( ruleRelationOperator )
        {
        // InternalSmartContractSpecification.g:1957:7: ( ruleRelationOperator )
        // InternalSmartContractSpecification.g:1958:8: ruleRelationOperator
        {
        pushFollow(FOLLOW_2);
        ruleRelationOperator();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred6_InternalSmartContractSpecification

    // $ANTLR start synpred7_InternalSmartContractSpecification
    public final void synpred7_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:2066:5: ( ( () ( ( ruleAdditiveOperator ) ) ) )
        // InternalSmartContractSpecification.g:2066:6: ( () ( ( ruleAdditiveOperator ) ) )
        {
        // InternalSmartContractSpecification.g:2066:6: ( () ( ( ruleAdditiveOperator ) ) )
        // InternalSmartContractSpecification.g:2067:6: () ( ( ruleAdditiveOperator ) )
        {
        // InternalSmartContractSpecification.g:2067:6: ()
        // InternalSmartContractSpecification.g:2068:6: 
        {
        }

        // InternalSmartContractSpecification.g:2069:6: ( ( ruleAdditiveOperator ) )
        // InternalSmartContractSpecification.g:2070:7: ( ruleAdditiveOperator )
        {
        // InternalSmartContractSpecification.g:2070:7: ( ruleAdditiveOperator )
        // InternalSmartContractSpecification.g:2071:8: ruleAdditiveOperator
        {
        pushFollow(FOLLOW_2);
        ruleAdditiveOperator();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred7_InternalSmartContractSpecification

    // $ANTLR start synpred8_InternalSmartContractSpecification
    public final void synpred8_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:2154:5: ( ( () ( ( ruleMultiplicativeOperator ) ) ) )
        // InternalSmartContractSpecification.g:2154:6: ( () ( ( ruleMultiplicativeOperator ) ) )
        {
        // InternalSmartContractSpecification.g:2154:6: ( () ( ( ruleMultiplicativeOperator ) ) )
        // InternalSmartContractSpecification.g:2155:6: () ( ( ruleMultiplicativeOperator ) )
        {
        // InternalSmartContractSpecification.g:2155:6: ()
        // InternalSmartContractSpecification.g:2156:6: 
        {
        }

        // InternalSmartContractSpecification.g:2157:6: ( ( ruleMultiplicativeOperator ) )
        // InternalSmartContractSpecification.g:2158:7: ( ruleMultiplicativeOperator )
        {
        // InternalSmartContractSpecification.g:2158:7: ( ruleMultiplicativeOperator )
        // InternalSmartContractSpecification.g:2159:8: ruleMultiplicativeOperator
        {
        pushFollow(FOLLOW_2);
        ruleMultiplicativeOperator();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred8_InternalSmartContractSpecification

    // $ANTLR start synpred9_InternalSmartContractSpecification
    public final void synpred9_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:2233:4: ( ruleConstantExpression )
        // InternalSmartContractSpecification.g:2233:5: ruleConstantExpression
        {
        pushFollow(FOLLOW_2);
        ruleConstantExpression();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred9_InternalSmartContractSpecification

    // $ANTLR start synpred10_InternalSmartContractSpecification
    public final void synpred10_InternalSmartContractSpecification_fragment() throws RecognitionException {   
        // InternalSmartContractSpecification.g:2663:4: ( ruleBoundedTimePredicate )
        // InternalSmartContractSpecification.g:2663:5: ruleBoundedTimePredicate
        {
        pushFollow(FOLLOW_2);
        ruleBoundedTimePredicate();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred10_InternalSmartContractSpecification

    // Delegated rules

    public final boolean synpred6_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred6_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred7_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred10_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred10_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred9_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred9_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred1_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred4_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred5_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_InternalSmartContractSpecification() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalSmartContractSpecification_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA36 dfa36 = new DFA36(this);
    protected DFA44 dfa44 = new DFA44(this);
    protected DFA47 dfa47 = new DFA47(this);
    protected DFA56 dfa56 = new DFA56(this);
    static final String dfa_1s = "\37\uffff";
    static final String dfa_2s = "\1\4\36\uffff";
    static final String dfa_3s = "\1\143\36\uffff";
    static final String dfa_4s = "\1\uffff\1\1\35\2";
    static final String dfa_5s = "\1\0\36\uffff}>";
    static final String[] dfa_6s = {
            "\1\20\1\uffff\1\35\1\4\20\uffff\1\36\17\uffff\1\1\7\uffff\1\2\1\16\1\25\1\26\1\27\1\30\1\31\1\32\1\uffff\1\3\1\5\1\6\1\11\13\uffff\1\24\1\12\1\33\1\14\1\34\17\uffff\1\7\1\10\1\13\1\15\1\17\1\21\1\22\1\23",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA36 extends DFA {

        public DFA36(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 36;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1262:2: (this_ConditionalExpression_0= ruleConditionalExpression | ( ( ruleImplyExpression )=>this_ImplyExpression_1= ruleImplyExpression ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA36_0 = input.LA(1);

                         
                        int index36_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA36_0==40) ) {s = 1;}

                        else if ( (LA36_0==48) && (synpred1_InternalSmartContractSpecification())) {s = 2;}

                        else if ( (LA36_0==57) && (synpred1_InternalSmartContractSpecification())) {s = 3;}

                        else if ( (LA36_0==RULE_INT) && (synpred1_InternalSmartContractSpecification())) {s = 4;}

                        else if ( (LA36_0==58) && (synpred1_InternalSmartContractSpecification())) {s = 5;}

                        else if ( (LA36_0==59) && (synpred1_InternalSmartContractSpecification())) {s = 6;}

                        else if ( (LA36_0==92) && (synpred1_InternalSmartContractSpecification())) {s = 7;}

                        else if ( (LA36_0==93) && (synpred1_InternalSmartContractSpecification())) {s = 8;}

                        else if ( (LA36_0==60) && (synpred1_InternalSmartContractSpecification())) {s = 9;}

                        else if ( (LA36_0==73) && (synpred1_InternalSmartContractSpecification())) {s = 10;}

                        else if ( (LA36_0==94) && (synpred1_InternalSmartContractSpecification())) {s = 11;}

                        else if ( (LA36_0==75) && (synpred1_InternalSmartContractSpecification())) {s = 12;}

                        else if ( (LA36_0==95) && (synpred1_InternalSmartContractSpecification())) {s = 13;}

                        else if ( (LA36_0==49) && (synpred1_InternalSmartContractSpecification())) {s = 14;}

                        else if ( (LA36_0==96) && (synpred1_InternalSmartContractSpecification())) {s = 15;}

                        else if ( (LA36_0==RULE_ID) && (synpred1_InternalSmartContractSpecification())) {s = 16;}

                        else if ( (LA36_0==97) && (synpred1_InternalSmartContractSpecification())) {s = 17;}

                        else if ( (LA36_0==98) && (synpred1_InternalSmartContractSpecification())) {s = 18;}

                        else if ( (LA36_0==99) && (synpred1_InternalSmartContractSpecification())) {s = 19;}

                        else if ( (LA36_0==72) && (synpred1_InternalSmartContractSpecification())) {s = 20;}

                        else if ( (LA36_0==50) && (synpred1_InternalSmartContractSpecification())) {s = 21;}

                        else if ( (LA36_0==51) && (synpred1_InternalSmartContractSpecification())) {s = 22;}

                        else if ( (LA36_0==52) && (synpred1_InternalSmartContractSpecification())) {s = 23;}

                        else if ( (LA36_0==53) && (synpred1_InternalSmartContractSpecification())) {s = 24;}

                        else if ( (LA36_0==54) && (synpred1_InternalSmartContractSpecification())) {s = 25;}

                        else if ( (LA36_0==55) && (synpred1_InternalSmartContractSpecification())) {s = 26;}

                        else if ( (LA36_0==74) && (synpred1_InternalSmartContractSpecification())) {s = 27;}

                        else if ( (LA36_0==76) && (synpred1_InternalSmartContractSpecification())) {s = 28;}

                        else if ( (LA36_0==RULE_PLAINTEXT) && (synpred1_InternalSmartContractSpecification())) {s = 29;}

                        else if ( (LA36_0==24) && (synpred1_InternalSmartContractSpecification())) {s = 30;}

                         
                        input.seek(index36_0);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 36, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String dfa_7s = "\16\uffff";
    static final String dfa_8s = "\1\14\15\uffff";
    static final String dfa_9s = "\1\17\13\0\2\uffff";
    static final String dfa_10s = "\1\132\13\0\2\uffff";
    static final String dfa_11s = "\14\uffff\1\2\1\1";
    static final String dfa_12s = "\1\uffff\1\11\1\2\1\6\1\0\1\4\1\10\1\1\1\5\1\12\1\3\1\7\2\uffff}>";
    static final String[] dfa_13s = {
            "\1\14\7\uffff\1\14\1\uffff\1\14\3\uffff\2\14\1\uffff\1\14\1\uffff\3\14\1\uffff\1\14\2\uffff\7\14\11\uffff\1\14\23\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\3\14",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            ""
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA44 extends DFA {

        public DFA44(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 44;
            this.eot = dfa_7;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "1951:3: ( ( ( ( () ( ( ruleRelationOperator ) ) ) )=> ( () ( (lv_operator_2_0= ruleRelationOperator ) ) ) ) ( (lv_right_3_0= ruleArithmeticExpression ) ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA44_4 = input.LA(1);

                         
                        int index44_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_4);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA44_7 = input.LA(1);

                         
                        int index44_7 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_7);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA44_2 = input.LA(1);

                         
                        int index44_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_2);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA44_10 = input.LA(1);

                         
                        int index44_10 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_10);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA44_5 = input.LA(1);

                         
                        int index44_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_5);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA44_8 = input.LA(1);

                         
                        int index44_8 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_8);
                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA44_3 = input.LA(1);

                         
                        int index44_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_3);
                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA44_11 = input.LA(1);

                         
                        int index44_11 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_11);
                        if ( s>=0 ) return s;
                        break;
                    case 8 : 
                        int LA44_6 = input.LA(1);

                         
                        int index44_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_6);
                        if ( s>=0 ) return s;
                        break;
                    case 9 : 
                        int LA44_1 = input.LA(1);

                         
                        int index44_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_1);
                        if ( s>=0 ) return s;
                        break;
                    case 10 : 
                        int LA44_9 = input.LA(1);

                         
                        int index44_9 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalSmartContractSpecification()) ) {s = 13;}

                        else if ( (true) ) {s = 12;}

                         
                        input.seek(index44_9);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 44, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String dfa_14s = "\62\uffff";
    static final String dfa_15s = "\2\uffff\1\46\6\uffff\1\61\46\uffff\1\12\1\uffff";
    static final String dfa_16s = "\1\4\1\uffff\1\17\3\uffff\3\4\1\17\45\uffff\1\45\1\17\1\uffff";
    static final String dfa_17s = "\1\143\1\uffff\1\132\3\uffff\3\4\1\132\45\uffff\1\75\1\132\1\uffff";
    static final String dfa_18s = "\1\uffff\1\1\1\uffff\2\1\1\2\4\uffff\1\4\1\5\1\6\1\7\41\1\2\uffff\1\3";
    static final String dfa_19s = "\1\0\1\uffff\1\1\57\uffff}>";
    static final String[] dfa_20s = {
            "\1\11\1\uffff\1\14\1\2\20\uffff\1\15\30\uffff\1\10\6\12\1\uffff\1\1\1\3\1\4\1\5\13\uffff\1\5\1\6\1\13\1\7\1\13\17\uffff\10\5",
            "",
            "\1\45\7\uffff\1\42\1\uffff\1\56\3\uffff\1\43\1\44\1\uffff\1\47\1\uffff\1\50\1\51\1\52\1\uffff\1\53\2\uffff\1\54\1\55\1\41\1\37\1\40\1\35\1\36\11\uffff\1\21\10\uffff\6\5\5\uffff\1\22\1\23\1\24\1\25\1\26\1\27\1\30\1\31\1\32\1\33\1\34\1\20\1\16\1\17",
            "",
            "",
            "",
            "\1\57",
            "\1\57",
            "\1\60",
            "\1\61\7\uffff\1\61\1\uffff\1\61\3\uffff\2\61\1\uffff\1\61\1\uffff\3\61\1\uffff\1\61\2\uffff\7\61\10\uffff\2\61\3\uffff\2\5\16\uffff\16\61",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\13\27\uffff\1\5",
            "\1\12\7\uffff\1\12\1\uffff\1\12\3\uffff\2\12\1\uffff\1\12\1\uffff\3\12\1\uffff\1\12\2\uffff\7\12\10\uffff\2\12\3\uffff\1\5\17\uffff\16\12",
            ""
    };

    static final short[] dfa_14 = DFA.unpackEncodedString(dfa_14s);
    static final short[] dfa_15 = DFA.unpackEncodedString(dfa_15s);
    static final char[] dfa_16 = DFA.unpackEncodedStringToUnsignedChars(dfa_16s);
    static final char[] dfa_17 = DFA.unpackEncodedStringToUnsignedChars(dfa_17s);
    static final short[] dfa_18 = DFA.unpackEncodedString(dfa_18s);
    static final short[] dfa_19 = DFA.unpackEncodedString(dfa_19s);
    static final short[][] dfa_20 = unpackEncodedStringArray(dfa_20s);

    class DFA47 extends DFA {

        public DFA47(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 47;
            this.eot = dfa_14;
            this.eof = dfa_15;
            this.min = dfa_16;
            this.max = dfa_17;
            this.accept = dfa_18;
            this.special = dfa_19;
            this.transition = dfa_20;
        }
        public String getDescription() {
            return "2231:2: ( ( ( ruleConstantExpression )=>this_ConstantExpression_0= ruleConstantExpression ) | this_TimePredicate_1= ruleTimePredicate | this_SlotRef_2= ruleSlotRef | this_ThisExpression_3= ruleThisExpression | this_QuantifierExpression_4= ruleQuantifierExpression | this_DescriptiveExpression_5= ruleDescriptiveExpression | (otherlv_6= '(' this_Expression_7= ruleExpression otherlv_8= ')' ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA47_0 = input.LA(1);

                         
                        int index47_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA47_0==57) && (synpred9_InternalSmartContractSpecification())) {s = 1;}

                        else if ( (LA47_0==RULE_INT) ) {s = 2;}

                        else if ( (LA47_0==58) && (synpred9_InternalSmartContractSpecification())) {s = 3;}

                        else if ( (LA47_0==59) && (synpred9_InternalSmartContractSpecification())) {s = 4;}

                        else if ( (LA47_0==60||LA47_0==72||(LA47_0>=92 && LA47_0<=99)) ) {s = 5;}

                        else if ( (LA47_0==73) ) {s = 6;}

                        else if ( (LA47_0==75) ) {s = 7;}

                        else if ( (LA47_0==49) ) {s = 8;}

                        else if ( (LA47_0==RULE_ID) ) {s = 9;}

                        else if ( ((LA47_0>=50 && LA47_0<=55)) ) {s = 10;}

                        else if ( (LA47_0==74||LA47_0==76) ) {s = 11;}

                        else if ( (LA47_0==RULE_PLAINTEXT) ) {s = 12;}

                        else if ( (LA47_0==24) ) {s = 13;}

                         
                        input.seek(index47_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA47_2 = input.LA(1);

                         
                        int index47_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA47_2==89) && (synpred9_InternalSmartContractSpecification())) {s = 14;}

                        else if ( (LA47_2==90) && (synpred9_InternalSmartContractSpecification())) {s = 15;}

                        else if ( (LA47_2==88) && (synpred9_InternalSmartContractSpecification())) {s = 16;}

                        else if ( (LA47_2==57) && (synpred9_InternalSmartContractSpecification())) {s = 17;}

                        else if ( (LA47_2==77) && (synpred9_InternalSmartContractSpecification())) {s = 18;}

                        else if ( (LA47_2==78) && (synpred9_InternalSmartContractSpecification())) {s = 19;}

                        else if ( (LA47_2==79) && (synpred9_InternalSmartContractSpecification())) {s = 20;}

                        else if ( (LA47_2==80) && (synpred9_InternalSmartContractSpecification())) {s = 21;}

                        else if ( (LA47_2==81) && (synpred9_InternalSmartContractSpecification())) {s = 22;}

                        else if ( (LA47_2==82) && (synpred9_InternalSmartContractSpecification())) {s = 23;}

                        else if ( (LA47_2==83) && (synpred9_InternalSmartContractSpecification())) {s = 24;}

                        else if ( (LA47_2==84) && (synpred9_InternalSmartContractSpecification())) {s = 25;}

                        else if ( (LA47_2==85) && (synpred9_InternalSmartContractSpecification())) {s = 26;}

                        else if ( (LA47_2==86) && (synpred9_InternalSmartContractSpecification())) {s = 27;}

                        else if ( (LA47_2==87) && (synpred9_InternalSmartContractSpecification())) {s = 28;}

                        else if ( (LA47_2==46) && (synpred9_InternalSmartContractSpecification())) {s = 29;}

                        else if ( (LA47_2==47) && (synpred9_InternalSmartContractSpecification())) {s = 30;}

                        else if ( (LA47_2==44) && (synpred9_InternalSmartContractSpecification())) {s = 31;}

                        else if ( (LA47_2==45) && (synpred9_InternalSmartContractSpecification())) {s = 32;}

                        else if ( (LA47_2==43) && (synpred9_InternalSmartContractSpecification())) {s = 33;}

                        else if ( (LA47_2==23) && (synpred9_InternalSmartContractSpecification())) {s = 34;}

                        else if ( (LA47_2==29) && (synpred9_InternalSmartContractSpecification())) {s = 35;}

                        else if ( (LA47_2==30) && (synpred9_InternalSmartContractSpecification())) {s = 36;}

                        else if ( (LA47_2==15) && (synpred9_InternalSmartContractSpecification())) {s = 37;}

                        else if ( (LA47_2==EOF) && (synpred9_InternalSmartContractSpecification())) {s = 38;}

                        else if ( (LA47_2==32) && (synpred9_InternalSmartContractSpecification())) {s = 39;}

                        else if ( (LA47_2==34) && (synpred9_InternalSmartContractSpecification())) {s = 40;}

                        else if ( (LA47_2==35) && (synpred9_InternalSmartContractSpecification())) {s = 41;}

                        else if ( (LA47_2==36) && (synpred9_InternalSmartContractSpecification())) {s = 42;}

                        else if ( (LA47_2==38) && (synpred9_InternalSmartContractSpecification())) {s = 43;}

                        else if ( (LA47_2==41) && (synpred9_InternalSmartContractSpecification())) {s = 44;}

                        else if ( (LA47_2==42) && (synpred9_InternalSmartContractSpecification())) {s = 45;}

                        else if ( (LA47_2==25) && (synpred9_InternalSmartContractSpecification())) {s = 46;}

                        else if ( ((LA47_2>=66 && LA47_2<=71)) ) {s = 5;}

                         
                        input.seek(index47_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 47, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String dfa_21s = "\21\uffff";
    static final String dfa_22s = "\1\4\2\0\16\uffff";
    static final String dfa_23s = "\1\143\2\0\16\uffff";
    static final String dfa_24s = "\3\uffff\15\2\1\1";
    static final String dfa_25s = "\1\0\1\1\1\2\16\uffff}>";
    static final String[] dfa_26s = {
            "\1\13\2\uffff\1\4\51\uffff\1\11\12\uffff\1\3\13\uffff\1\17\1\5\1\uffff\1\7\20\uffff\1\1\1\2\1\6\1\10\1\12\1\14\1\15\1\16",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_21 = DFA.unpackEncodedString(dfa_21s);
    static final char[] dfa_22 = DFA.unpackEncodedStringToUnsignedChars(dfa_22s);
    static final char[] dfa_23 = DFA.unpackEncodedStringToUnsignedChars(dfa_23s);
    static final short[] dfa_24 = DFA.unpackEncodedString(dfa_24s);
    static final short[] dfa_25 = DFA.unpackEncodedString(dfa_25s);
    static final short[][] dfa_26 = unpackEncodedStringArray(dfa_26s);

    class DFA56 extends DFA {

        public DFA56(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 56;
            this.eot = dfa_21;
            this.eof = dfa_21;
            this.min = dfa_22;
            this.max = dfa_23;
            this.accept = dfa_24;
            this.special = dfa_25;
            this.transition = dfa_26;
        }
        public String getDescription() {
            return "2652:2: (this_SimpleTimePredicate_0= ruleSimpleTimePredicate | ( ( ruleBoundedTimePredicate )=>this_BoundedTimePredicate_1= ruleBoundedTimePredicate ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA56_0 = input.LA(1);

                         
                        int index56_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA56_0==92) ) {s = 1;}

                        else if ( (LA56_0==93) ) {s = 2;}

                        else if ( (LA56_0==60) && (synpred10_InternalSmartContractSpecification())) {s = 3;}

                        else if ( (LA56_0==RULE_INT) && (synpred10_InternalSmartContractSpecification())) {s = 4;}

                        else if ( (LA56_0==73) && (synpred10_InternalSmartContractSpecification())) {s = 5;}

                        else if ( (LA56_0==94) && (synpred10_InternalSmartContractSpecification())) {s = 6;}

                        else if ( (LA56_0==75) && (synpred10_InternalSmartContractSpecification())) {s = 7;}

                        else if ( (LA56_0==95) && (synpred10_InternalSmartContractSpecification())) {s = 8;}

                        else if ( (LA56_0==49) && (synpred10_InternalSmartContractSpecification())) {s = 9;}

                        else if ( (LA56_0==96) && (synpred10_InternalSmartContractSpecification())) {s = 10;}

                        else if ( (LA56_0==RULE_ID) && (synpred10_InternalSmartContractSpecification())) {s = 11;}

                        else if ( (LA56_0==97) && (synpred10_InternalSmartContractSpecification())) {s = 12;}

                        else if ( (LA56_0==98) && (synpred10_InternalSmartContractSpecification())) {s = 13;}

                        else if ( (LA56_0==99) && (synpred10_InternalSmartContractSpecification())) {s = 14;}

                        else if ( (LA56_0==72) && (synpred10_InternalSmartContractSpecification())) {s = 15;}

                         
                        input.seek(index56_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA56_1 = input.LA(1);

                         
                        int index56_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (true) ) {s = 16;}

                        else if ( (synpred10_InternalSmartContractSpecification()) ) {s = 15;}

                         
                        input.seek(index56_1);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA56_2 = input.LA(1);

                         
                        int index56_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (true) ) {s = 16;}

                        else if ( (synpred10_InternalSmartContractSpecification()) ) {s = 15;}

                         
                        input.seek(index56_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 56, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000500000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000004530010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000004030010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000004030000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000810000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000010L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002800000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000040022L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000070800002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x1EFF0100010000D0L,0x0000000FF0001F00L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000060800002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000D00000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000D40800002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000300000000002L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000C00000000002L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000002L,0x0000000000FFE000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0200000000000002L,0x0000000001000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000000002L,0x0000000006000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0100000000000002L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000010L,0x000000000A000000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x1002000000000090L,0x0000000FF0000B00L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000030000000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x8000000000000000L,0x0000000000000003L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});

}