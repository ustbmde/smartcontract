package edu.ustb.sei.mde.value;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;


import edu.ustb.sei.mde.smartContractSpecification.RelationalOperator;

public class RelationConverter implements IValueConverter<RelationalOperator> {
	
	final static Pair<RelationalOperator, String[]> equal = new Pair<RelationalOperator, String[]>(RelationalOperator.EQUAL,new String[] {"=","is"});
	final static Pair<RelationalOperator, String[]> less = new Pair<RelationalOperator, String[]>(RelationalOperator.LESS,new String[] {"<"});
	final static Pair<RelationalOperator, String[]> lessOrEqual = new Pair<RelationalOperator, String[]>(RelationalOperator.LESS_OR_EQUAL,new String[] {"<="});
	final static Pair<RelationalOperator, String[]> greater = new Pair<RelationalOperator, String[]>(RelationalOperator.GREATER,new String[] {">"});
	final static Pair<RelationalOperator, String[]> greaterOrEqual = new Pair<RelationalOperator, String[]>(RelationalOperator.GREATER_OR_EQUAL,new String[] {">="});
	final static Pair<RelationalOperator, String[]> notEqual = new Pair<RelationalOperator, String[]>(RelationalOperator.NOT_EQUAL,new String[] {"<>","!=","is(\\s)+not"});
	final static Pair<RelationalOperator, String[]> in = new Pair<RelationalOperator, String[]>(RelationalOperator.IN,new String[] {"is(\\s)+in","belongs(\\s)+to"});
	final static Pair<RelationalOperator, String[]> subset = new Pair<RelationalOperator, String[]>(RelationalOperator.SUBSET,new String[] {"is(\\s)+a(\\s)+subset(\\s)+of"});
	
	@SuppressWarnings("unchecked")
	final static Pair<RelationalOperator, String[]>[] pairs = new Pair[]{equal,less,lessOrEqual,greater,greaterOrEqual,notEqual,in,subset};
	@Override
	public RelationalOperator toValue(String string, INode node) throws ValueConverterException {
		for(Pair<RelationalOperator, String[]> pair : pairs) {
			for(String pat : pair.second) {
				if(string.matches(pat)) return pair.first;
			}
		}
		return RelationalOperator.EQUAL;
	}

	@Override
	public String toString(RelationalOperator value) throws ValueConverterException {
		for(Pair<RelationalOperator, String[]> pair : pairs) {
			if(value== pair.first) return pair.second[0];
		}
		return "=";
	}

}
