package edu.ustb.sei.mde.value;

public class Pair<F, S> {
	
	public final F first;
	public final S second;

	public Pair(F f, S s) {
		this.first = f;
		this.second = s;
	}

}
