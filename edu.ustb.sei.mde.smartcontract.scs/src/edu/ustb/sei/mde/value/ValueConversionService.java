package edu.ustb.sei.mde.value;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.impl.AbstractDeclarativeValueConverterService;

import edu.ustb.sei.mde.smartContractSpecification.RelationalOperator;

public class ValueConversionService extends AbstractDeclarativeValueConverterService {
	
	private IValueConverter<RelationalOperator> relation = new RelationConverter();
	@ValueConverter(rule = "RELATION")
	public IValueConverter<RelationalOperator> Relation() {
		return relation;
	}
}
