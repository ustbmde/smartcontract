/**
 */
package edu.ustb.sei.mde.smartContractSpecification;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deposit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.smartContractSpecification.Deposit#getMoney <em>Money</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.smartContractSpecification.SmartContractSpecificationPackage#getDeposit()
 * @model
 * @generated
 */
public interface Deposit extends TransferOperation {
	/**
	 * Returns the value of the '<em><b>Money</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Money</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Money</em>' containment reference.
	 * @see #setMoney(Expression)
	 * @see edu.ustb.sei.mde.smartContractSpecification.SmartContractSpecificationPackage#getDeposit_Money()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getMoney();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.smartContractSpecification.Deposit#getMoney <em>Money</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Money</em>' containment reference.
	 * @see #getMoney()
	 * @generated
	 */
	void setMoney(Expression value);

} // Deposit
